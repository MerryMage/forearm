#include <iostream>
#include "ForeARM/Interpreter/ARMInterpreter.hpp"
#include "ForeARM/Interpreter/State/MemoryInterface/DummyMemoryInterface.hpp"

int main()
{
    // Preliminary file. This will contain tests eventually.
    // This was just use to make sure library-based linking worked >_>.

    // SBFX r0, r1, #0, #11 (0xE7AA0051)
    // SBFX r0, r1, #2, #11 (0xE7AA0151)
    // SBFX r0, r1, #1, #11 (0xE7AA00D1)
    // SBFX r0, r1, #30, #2 (0xE7A10F51)
    //
    // MUL  r0, r1, r2 (0xE0000291) (r1: 0x0000FFFF, r2: 0x0000FFFF) -> r0: 0xFFFE0001
    // MULS r0, r1, r2 (0xE0100291) (r1: 0x0000FFFF, r2: 0x0000FFFF) -> r0: 0xFFFE0001
    //
    // MLAS r0, r1, r2, r3 (0xE0303291) (r1: 0x0000FFFF, r2: 0x0000FFFF, r3: 0x00000001) -> r0: 0xfffe0002
    //
    // MLS  r0, r1, r2, r3 (0xE0603291) (r1: 0x0000FFFF, r2: 0x0000FFFF, r3: 0x00000001) -> r0: 0x00020000

    ForeARM::Interpreter i;
    i.state().reg(0) = 0;
    i.state().reg(1) = 0x0000ffff;
    i.state().reg(2) = 0x0000ffff;
    i.state().reg(3) = 1;
    i.interpret(0xE0603291);

    const auto& s = i.state();
    std::cout << std::hex << "Out : " << s.reg(0) << std::endl;
    std::cout << std::hex << "CPSR: " << s.cpsr().value() << std::endl;

    std::cin.get();
    return 0;
}

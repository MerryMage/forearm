#pragma once

#include <array>
#include <cstdint>
#include <memory>
#include "ForeARM/Interpreter/State/ARMState.hpp"

namespace ForeARM
{
namespace State { class MemoryInterface; }

class DecodedInstruction;

/**
 * ARMv7 Interpreter.
 */
class Interpreter final
{
public:
    /**
     * Constructor
     *
     * This initializes the interpreter with a default state.
     * This means that everything functions as normal, however
     * the exception to this is that load/stores will not
     * function properly (stores do nothing, and loads always return zero).
     *
     * The reason for this is that the default state only has a stubbed
     * MemoryInterface instance (DummyMemoryInterface), since the user
     * has not provided a custom MemoryInterface implementation that
     * provides access to the usable memory.
     *
     * This constructor is not typically desirable because of this,
     * but is useful for unit testing when loads or stores aren't
     * a concern.
     */
    Interpreter();

    /**
     * Constructor
     *
     * @param state An ARMState instance to initialize this interpreter with.
     */
    explicit Interpreter(State::ARMState state);

    /**
     * Interprets a single instruction.
     *
     * @param opcode The opcode of an instruction to interpret.
     */
    void interpret(uint32_t opcode);

    /// Retrieves a mutable reference to the underlying ARMState instance.
    State::ARMState& state() noexcept;

    /// Retrieves a non-mutable reference to the underlying ARMState instance.
    const State::ARMState& state() const noexcept;

    /**
     * Convenience method for setting the underlying ARMState's active MemoryInterface.
     *
     * @param memory_interface The MemoryInterface instance to use.
     */
    void set_memory_interface(std::unique_ptr<State::MemoryInterface> memory_interface);

private:
    void execute_instruction(DecodedInstruction decoded_instruction);

    State::ARMState m_state;
};

} // namespace ForeARM

#include "ForeARM/Interpreter/Instructions/ARM.hpp"
#include "ForeARM/Interpreter/State/ARMState.hpp"
#include "ForeARM/Utils/Bits.hpp"

namespace ForeARM
{
namespace ARM
{

void SXTAB(State::ARMState& state, const uint32_t opcode)
{
    const uint32_t rm_value = state.reg(Bit::get_bits<0, 3>(opcode));
    const uint32_t rn_value = state.reg(Bit::get_bits<16, 19>(opcode));
    const uint32_t rd_index = Bit::get_bits<12, 15>(opcode);

    const uint32_t rotation   = Bit::get_bits<10, 11>(opcode) * 8;
    const uint32_t rotated_rm = Bit::rotate_right(rm_value, rotation);

    state.reg(rd_index) = rn_value + Bit::sign_extend<8>(rotated_rm & 0xFF);
}

void SXTAB16(State::ARMState& state, const uint32_t opcode)
{
    const uint32_t rd_index = Bit::get_bits<12, 15>(opcode);
    const uint32_t rm_value = state.reg(Bit::get_bits<0, 3>(opcode));
    const uint32_t rn_value = state.reg(Bit::get_bits<16, 19>(opcode));

    const uint32_t rotation   = Bit::get_bits<10, 11>(opcode) * 8;
    const uint32_t rotated_rm = Bit::rotate_right(rm_value, rotation);

    const uint16_t low  = static_cast<uint16_t>(Bit::sign_extend<8>(rotated_rm & 0xFF));
    const uint16_t high = static_cast<uint16_t>(Bit::sign_extend<8>((rotated_rm >> 16) & 0xFF));

    state.reg(rd_index) = (rn_value & 0xFFFF) + low | ((rn_value >> 16) + high) << 16;
}

void SXTAH(State::ARMState& state, const uint32_t opcode)
{
    const uint32_t rd_index = Bit::get_bits<12, 15>(opcode);
    const uint32_t rm_value = state.reg(Bit::get_bits<0, 3>(opcode));
    const uint32_t rn_value = state.reg(Bit::get_bits<16, 19>(opcode));

    const uint32_t rotation = Bit::get_bits<10, 11>(opcode) * 8;
    const uint32_t rotated_rm = Bit::rotate_right(rm_value, rotation);

    state.reg(rd_index) = rn_value + Bit::sign_extend<16>(rotated_rm & 0xFFFF);
}

void SXTB(State::ARMState& state, const uint32_t opcode)
{
    const uint32_t rd_index = Bit::get_bits<12, 15>(opcode);
    const uint32_t rm_value = state.reg(Bit::get_bits<0, 3>(opcode));

    const uint32_t rotation = Bit::get_bits<10, 11>(opcode) * 8;
    const uint32_t rotated_rm = Bit::rotate_right(rm_value, rotation);

    state.reg(rd_index) = Bit::sign_extend<8>(rotated_rm & 0xFF);
}

void SXTB16(State::ARMState& state, const uint32_t opcode)
{
    const uint32_t rd_index = Bit::get_bits<12, 15>(opcode);
    const uint32_t rm_value = state.reg(Bit::get_bits<0, 3>(opcode));

    const uint32_t rotation = Bit::get_bits<10, 11>(opcode) * 8;
    const uint32_t rotated_rm = Bit::rotate_right(rm_value, rotation);

    const uint16_t low  = static_cast<uint16_t>(Bit::sign_extend<8>(rotated_rm & 0xFF));
    const uint16_t high = static_cast<uint16_t>(Bit::sign_extend<8>((rotated_rm >> 16) & 0xFF));

    state.reg(rd_index) = static_cast<uint32_t>(low | (high << 16));
}

void SXTH(State::ARMState& state, const uint32_t opcode)
{
    const uint32_t rd_index = Bit::get_bits<12, 15>(opcode);
    const uint32_t rm_value = state.reg(Bit::get_bits<0, 3>(opcode));

    const uint32_t rotation = Bit::get_bits<10, 11>(opcode) * 8;
    const uint32_t rotated_rm = Bit::rotate_right(rm_value, rotation);

    state.reg(rd_index) = Bit::sign_extend<16>(rotated_rm & 0xFFFF);
}

void UXTAB(State::ARMState& state, const uint32_t opcode)
{
    const uint32_t rd_index = Bit::get_bits<12, 15>(opcode);
    const uint32_t rm_value = state.reg(Bit::get_bits<0, 3>(opcode));
    const uint32_t rn_value = state.reg(Bit::get_bits<16, 19>(opcode));

    const uint32_t rotation = Bit::get_bits<10, 11>(opcode) * 8;
    const uint32_t rotated_rm = Bit::rotate_right(rm_value, rotation);

    state.reg(rd_index) = rn_value + (rotated_rm & 0xFF);
}

void UXTAB16(State::ARMState& state, const uint32_t opcode)
{
    const uint32_t rd_index = Bit::get_bits<12, 15>(opcode);
    const uint32_t rm_value = state.reg(Bit::get_bits<0, 3>(opcode));
    const uint32_t rn_value = state.reg(Bit::get_bits<16, 19>(opcode));

    const uint32_t rotation = Bit::get_bits<10, 11>(opcode) * 8;
    const uint32_t rotated_rm = Bit::rotate_right(rm_value, rotation);

    const uint32_t low  = (rn_value & 0xFFFF) + (rotated_rm & 0xFF);
    const uint32_t high = ((rn_value >> 16) & 0xFFFF) + ((rotated_rm >> 16) & 0xFF);

    state.reg(rd_index) = low | (high << 16);
}

void UXTAH(State::ARMState& state, const uint32_t opcode)
{
    const uint32_t rd_index = Bit::get_bits<12, 15>(opcode);
    const uint32_t rm_value = state.reg(Bit::get_bits<0, 3>(opcode));
    const uint32_t rn_value = state.reg(Bit::get_bits<16, 19>(opcode));

    const uint32_t rotation = Bit::get_bits<10, 11>(opcode) * 8;
    const uint32_t rotated_rm = Bit::rotate_right(rm_value, rotation);

    state.reg(rd_index) = rn_value + (rotated_rm & 0xFFFF);
}

void UXTB(State::ARMState& state, const uint32_t opcode)
{
    const uint32_t rd_index = Bit::get_bits<12, 15>(opcode);
    const uint32_t rm_value = state.reg(Bit::get_bits<0, 3>(opcode));

    const uint32_t rotation = Bit::get_bits<10, 11>(opcode) * 8;
    const uint32_t rotated_rm = Bit::rotate_right(rm_value, rotation);

    state.reg(rd_index) = (rotated_rm & 0xFF);
}

void UXTB16(State::ARMState& state, const uint32_t opcode)
{
    const uint32_t rd_index = Bit::get_bits<12, 15>(opcode);
    const uint32_t rm_value = state.reg(Bit::get_bits<0, 3>(opcode));

    const uint32_t rotation = Bit::get_bits<10, 11>(opcode) * 8;
    const uint32_t rotated_rm = Bit::rotate_right(rm_value, rotation);

    state.reg(rd_index) = (rotated_rm & 0xFF) | ((rotated_rm >> 16) & 0xFF) << 16;
}

void UXTH(State::ARMState& state, const uint32_t opcode)
{
    const uint32_t rd_index = Bit::get_bits<12, 15>(opcode);
    const uint32_t rm_value = state.reg(Bit::get_bits<0, 3>(opcode));

    const uint32_t rotation = Bit::get_bits<10, 11>(opcode) * 8;
    const uint32_t rotated_rm = Bit::rotate_right(rm_value, rotation);

    state.reg(rd_index) = (rotated_rm & 0xFFFF);
}

} // namespace ARM
} // namespace ForeARM

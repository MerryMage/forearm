#include "ForeARM/Interpreter/Instructions/ARM.hpp"
#include "ForeARM/Interpreter/State/ARMState.hpp"
#include "ForeARM/Utils/Bits.hpp"

namespace ForeARM
{
namespace ARM
{

void CLREX(State::ARMState&, const uint32_t)
{
}

void LDREX(State::ARMState&, const uint32_t)
{
}

void LDREXB(State::ARMState&, const uint32_t)
{
}

void LDREXD(State::ARMState&, const uint32_t)
{
}

void LDREXH(State::ARMState&, const uint32_t)
{
}

void STREX(State::ARMState&, const uint32_t)
{
}

void STREXB(State::ARMState&, const uint32_t)
{
}

void STREXD(State::ARMState&, const uint32_t)
{
}

void STREXH(State::ARMState&, const uint32_t)
{
}

void SWP(State::ARMState&, const uint32_t)
{
}

} // namespace ARM
} // namespace ForeARM

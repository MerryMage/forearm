#include "ForeARM/Interpreter/Instructions/ARM.hpp"
#include "ForeARM/Interpreter/State/ARMState.hpp"
#include "ForeARM/Utils/Bits.hpp"

namespace
{

uint32_t unsigned_sum_absolute_difference(const uint32_t left, const uint32_t right) noexcept
{
    if (left > right)
        return left - right;

    return right - left;
}

} // Anonymous namespace

namespace ForeARM
{
namespace ARM
{

void USAD8(State::ARMState& state, const uint32_t opcode)
{
    const uint32_t rd_index = Bit::get_bits<16, 19>(opcode);
    const uint32_t rm_value = state.reg(Bit::get_bits<8, 11>(opcode));
    const uint32_t rn_value = state.reg(Bit::get_bits<0, 3>(opcode));

    const uint32_t diff1 = unsigned_sum_absolute_difference(rn_value & 0xFF, rm_value & 0xFF);
    const uint32_t diff2 = unsigned_sum_absolute_difference((rn_value >>  8) & 0xFF, (rm_value >>  8) & 0xFF);
    const uint32_t diff3 = unsigned_sum_absolute_difference((rn_value >> 16) & 0xFF, (rm_value >> 16) & 0xFF);
    const uint32_t diff4 = unsigned_sum_absolute_difference((rn_value >> 24) & 0xFF, (rm_value >> 24) & 0xFF);

    state.reg(rd_index) = (diff1 & 0xFF) + (diff2 & 0xFF) + (diff3 & 0xFF) + (diff4 & 0xFF);
}

void USADA8(State::ARMState& state, const uint32_t opcode)
{
    const uint32_t rd_index = Bit::get_bits<16, 19>(opcode);
    const uint32_t ra_value = state.reg(Bit::get_bits<12, 15>(opcode));
    const uint32_t rm_value = state.reg(Bit::get_bits<8, 11>(opcode));
    const uint32_t rn_value = state.reg(Bit::get_bits<0, 3>(opcode));

    const uint32_t diff1 = unsigned_sum_absolute_difference(rn_value & 0xFF, rm_value & 0xFF);
    const uint32_t diff2 = unsigned_sum_absolute_difference((rn_value >>  8) & 0xFF, (rm_value >>  8) & 0xFF);
    const uint32_t diff3 = unsigned_sum_absolute_difference((rn_value >> 16) & 0xFF, (rm_value >> 16) & 0xFF);
    const uint32_t diff4 = unsigned_sum_absolute_difference((rn_value >> 24) & 0xFF, (rm_value >> 24) & 0xFF);

    state.reg(rd_index) = ra_value + (diff1 & 0xFF) + (diff2 & 0xFF) + (diff3 & 0xFF) + (diff4 & 0xFF);
}

} // namespace ARM
} // namespace ForeARM

#include "ForeARM/Interpreter/Instructions/ARM.hpp"
#include "ForeARM/Interpreter/State/ARMState.hpp"
#include "ForeARM/Utils/Bits.hpp"

namespace ForeARM
{
namespace ARM
{

void DMB(State::ARMState&, const uint32_t)
{
}

void DSB(State::ARMState&, const uint32_t)
{
}

void ISB(State::ARMState&, const uint32_t)
{
}

} // namespace ARM
} // namespace ForeARM

#include "ForeARM/Interpreter/Instructions/ARM.hpp"
#include "ForeARM/Interpreter/Instructions/Helper/Saturation.hpp"
#include "ForeARM/Interpreter/State/ARMState.hpp"
#include "ForeARM/Utils/Bits.hpp"

namespace ForeARM
{
namespace ARM
{

void QADD8(State::ARMState& state, const uint32_t opcode)
{
    const uint32_t rd_index = Bit::get_bits<12, 15>(opcode);
    const uint64_t rm_value = state.reg(Bit::get_bits<0, 3>(opcode));
    const uint64_t rn_value = state.reg(Bit::get_bits<16, 19>(opcode));

    const uint64_t sum1 = Bit::sign_extend<8>(rn_value)       + Bit::sign_extend<8>(rm_value);
    const uint64_t sum2 = Bit::sign_extend<8>(rn_value >>  8) + Bit::sign_extend<8>(rm_value >>  8);
    const uint64_t sum3 = Bit::sign_extend<8>(rn_value >> 16) + Bit::sign_extend<8>(rm_value >> 16);
    const uint64_t sum4 = Bit::sign_extend<8>(rn_value >> 24) + Bit::sign_extend<8>(rm_value >> 24);

    const uint32_t result1 = Saturation::signed_saturation(sum1, 8) & 0xFF;
    const uint32_t result2 = Saturation::signed_saturation(sum2, 8) & 0xFF;
    const uint32_t result3 = Saturation::signed_saturation(sum3, 8) & 0xFF;
    const uint32_t result4 = Saturation::signed_saturation(sum4, 8) & 0xFF;

    state.reg(rd_index) = (result1 | result2 << 8 | result3 << 16 | result4 << 24);
}

void QADD16(State::ARMState& state, const uint32_t opcode)
{
    const uint32_t rd_index = Bit::get_bits<12, 15>(opcode);
    const uint64_t rm_value = state.reg(Bit::get_bits<0, 3>(opcode));
    const uint64_t rn_value = state.reg(Bit::get_bits<16, 19>(opcode));

    const uint64_t sum1 = Bit::sign_extend<16>(rn_value)       + Bit::sign_extend<16>(rm_value);
    const uint64_t sum2 = Bit::sign_extend<16>(rn_value >> 16) + Bit::sign_extend<16>(rm_value >> 16);

    const uint32_t result1 = Saturation::signed_saturation(sum1, 16) & 0xFFFF;
    const uint32_t result2 = Saturation::signed_saturation(sum2, 16) & 0xFFFF;

    state.reg(rd_index) = (result1 | result2 << 16);
}

void QASX(State::ARMState& state, const uint32_t opcode)
{
    const uint32_t rd_index = Bit::get_bits<12, 15>(opcode);
    const uint64_t rm_value = state.reg(Bit::get_bits<0, 3>(opcode));
    const uint64_t rn_value = state.reg(Bit::get_bits<16, 19>(opcode));

    const uint64_t diff = Bit::sign_extend<16>(rn_value)       - Bit::sign_extend<16>(rm_value >> 16);
    const uint64_t sum  = Bit::sign_extend<16>(rn_value >> 16) + Bit::sign_extend<16>(rm_value);

    const uint32_t result1 = Saturation::signed_saturation(diff, 16) & 0xFFFF;
    const uint32_t result2 = Saturation::signed_saturation(sum, 16) & 0xFFFF;

    state.reg(rd_index) = (result1 | result2 << 16);
}

void QSAX(State::ARMState& state, const uint32_t opcode)
{
    const uint32_t rd_index = Bit::get_bits<12, 15>(opcode);
    const uint64_t rm_value = state.reg(Bit::get_bits<0, 3>(opcode));
    const uint64_t rn_value = state.reg(Bit::get_bits<16, 19>(opcode));

    const uint64_t sum  = Bit::sign_extend<16>(rn_value)       + Bit::sign_extend<16>(rm_value >> 16);
    const uint64_t diff = Bit::sign_extend<16>(rn_value >> 16) - Bit::sign_extend<16>(rm_value);

    const uint32_t result1 = Saturation::signed_saturation(sum, 16) & 0xFFFF;
    const uint32_t result2 = Saturation::signed_saturation(diff, 16) & 0xFFFF;

    state.reg(rd_index) = (result1 | result2 << 16);
}

void QSUB8(State::ARMState& state, const uint32_t opcode)
{
    const uint32_t rd_index = Bit::get_bits<12, 15>(opcode);
    const uint64_t rm_value = state.reg(Bit::get_bits<0, 3>(opcode));
    const uint64_t rn_value = state.reg(Bit::get_bits<16, 19>(opcode));

    const uint64_t diff1 = Bit::sign_extend<8>(rn_value)       - Bit::sign_extend<8>(rm_value);
    const uint64_t diff2 = Bit::sign_extend<8>(rn_value >>  8) - Bit::sign_extend<8>(rm_value >>  8);
    const uint64_t diff3 = Bit::sign_extend<8>(rn_value >> 16) - Bit::sign_extend<8>(rm_value >> 16);
    const uint64_t diff4 = Bit::sign_extend<8>(rn_value >> 24) - Bit::sign_extend<8>(rm_value >> 24);

    const uint32_t result1 = Saturation::signed_saturation(diff1, 8) & 0xFF;
    const uint32_t result2 = Saturation::signed_saturation(diff2, 8) & 0xFF;
    const uint32_t result3 = Saturation::signed_saturation(diff3, 8) & 0xFF;
    const uint32_t result4 = Saturation::signed_saturation(diff4, 8) & 0xFF;

    state.reg(rd_index) = (result1 | result2 << 8 | result3 << 16 | result4 << 24);
}

void QSUB16(State::ARMState& state, const uint32_t opcode)
{
    const uint32_t rd_index = Bit::get_bits<12, 15>(opcode);
    const uint64_t rm_value = state.reg(Bit::get_bits<0, 3>(opcode));
    const uint64_t rn_value = state.reg(Bit::get_bits<16, 19>(opcode));

    const uint64_t diff1 = Bit::sign_extend<16>(rn_value)       - Bit::sign_extend<16>(rm_value);
    const uint64_t diff2 = Bit::sign_extend<16>(rn_value >> 16) - Bit::sign_extend<16>(rm_value >> 16);

    const uint32_t result1 = Saturation::signed_saturation(diff1, 16) & 0xFFFF;
    const uint32_t result2 = Saturation::signed_saturation(diff2, 16) & 0xFFFF;

    state.reg(rd_index) = (result1 | result2 << 16);
}

void UQADD8(State::ARMState& state, const uint32_t opcode)
{
    const uint32_t rd_index = Bit::get_bits<12, 15>(opcode);
    const uint64_t rm_value = state.reg(Bit::get_bits<0, 3>(opcode));
    const uint64_t rn_value = state.reg(Bit::get_bits<16, 19>(opcode));

    const uint64_t sum1 = (rn_value & 0xFF) + (rm_value & 0xFF);
    const uint64_t sum2 = ((rn_value >>  8) & 0xFF) + ((rm_value >>  8) & 0xFF);
    const uint64_t sum3 = ((rn_value >> 16) & 0xFF) + ((rm_value >> 16) & 0xFF);
    const uint64_t sum4 = ((rn_value >> 24) & 0xFF) + ((rm_value >> 24) & 0xFF);

    state.reg(rd_index) = Saturation::unsigned_saturation(sum1, 8)       |
                          Saturation::unsigned_saturation(sum2, 8) <<  8 |
                          Saturation::unsigned_saturation(sum3, 8) << 16 |
                          Saturation::unsigned_saturation(sum4, 8) << 24;
}

void UQADD16(State::ARMState& state, const uint32_t opcode)
{
    const uint32_t rd_index = Bit::get_bits<12, 15>(opcode);
    const uint64_t rm_value = state.reg(Bit::get_bits<0, 3>(opcode));
    const uint64_t rn_value = state.reg(Bit::get_bits<16, 19>(opcode));

    const uint64_t sum1 = (rn_value & 0xFFFF) + (rm_value & 0xFFFF);
    const uint64_t sum2 = (rn_value >> 16)    + (rm_value >> 16);

    state.reg(rd_index) = Saturation::unsigned_saturation(sum1, 16) |
                          Saturation::unsigned_saturation(sum2, 16) << 16;
}

void UQASX(State::ARMState& state, const uint32_t opcode)
{
    const uint32_t rd_index = Bit::get_bits<12, 15>(opcode);
    const uint64_t rm_value = state.reg(Bit::get_bits<0, 3>(opcode));
    const uint64_t rn_value = state.reg(Bit::get_bits<16, 19>(opcode));

    const uint64_t diff = (rn_value & 0xFFFF) - (rm_value >> 16);
    const uint64_t sum  = (rn_value >> 16)    + (rm_value & 0xFFFF);

    state.reg(rd_index) = Saturation::unsigned_saturation(diff, 16) |
                          Saturation::unsigned_saturation(sum, 16) << 16;
}

void UQSAX(State::ARMState& state, const uint32_t opcode)
{
    const uint32_t rd_index = Bit::get_bits<12, 15>(opcode);
    const uint64_t rm_value = state.reg(Bit::get_bits<0, 3>(opcode));
    const uint64_t rn_value = state.reg(Bit::get_bits<16, 19>(opcode));

    const uint64_t sum  = (rn_value & 0xFFFF) + (rm_value >> 16);
    const uint64_t diff = (rn_value >> 16)    - (rm_value & 0xFFFF);

    state.reg(rd_index) = Saturation::unsigned_saturation(sum, 16) |
                          Saturation::unsigned_saturation(diff, 16) << 16;
}

void UQSUB8(State::ARMState& state, const uint32_t opcode)
{
    const uint32_t rd_index = Bit::get_bits<12, 15>(opcode);
    const uint64_t rm_value = state.reg(Bit::get_bits<0, 3>(opcode));
    const uint64_t rn_value = state.reg(Bit::get_bits<16, 19>(opcode));

    const uint64_t diff1 = (rn_value & 0xFF) - (rm_value & 0xFF);
    const uint64_t diff2 = ((rn_value >>  8) & 0xFF) - ((rm_value >>  8) & 0xFF);
    const uint64_t diff3 = ((rn_value >> 16) & 0xFF) - ((rm_value >> 16) & 0xFF);
    const uint64_t diff4 = ((rn_value >> 24) & 0xFF) - ((rm_value >> 24) & 0xFF);

    state.reg(rd_index) = Saturation::unsigned_saturation(diff1, 8)       |
                          Saturation::unsigned_saturation(diff2, 8) <<  8 |
                          Saturation::unsigned_saturation(diff3, 8) << 16 |
                          Saturation::unsigned_saturation(diff4, 8) << 24;
}

void UQSUB16(State::ARMState& state, const uint32_t opcode)
{
    const uint32_t rd_index = Bit::get_bits<12, 15>(opcode);
    const uint64_t rm_value = state.reg(Bit::get_bits<0, 3>(opcode));
    const uint64_t rn_value = state.reg(Bit::get_bits<16, 19>(opcode));

    const uint64_t diff1 = (rn_value & 0xFFFF) - (rm_value & 0xFFFF);
    const uint64_t diff2 = (rn_value >> 16)    - (rm_value >> 16);

    state.reg(rd_index) = Saturation::unsigned_saturation(diff1, 16) |
                          Saturation::unsigned_saturation(diff2, 16) << 16;
}

} // namespace ARM
} // namespace ForeARM

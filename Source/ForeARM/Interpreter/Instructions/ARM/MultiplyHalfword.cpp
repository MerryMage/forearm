#include "ForeARM/Interpreter/Instructions/ARM.hpp"
#include "ForeARM/Interpreter/State/ARMState.hpp"
#include "ForeARM/Utils/Bits.hpp"

namespace ForeARM
{
namespace ARM
{

void SMLALXY(State::ARMState& state, const uint32_t opcode)
{
    const uint32_t rdlo_index = Bit::get_bits<12, 15>(opcode);
    const uint32_t rdhi_index = Bit::get_bits<16, 19>(opcode);
    const uint32_t rm_value = state.reg(Bit::get_bits<8, 11>(opcode));
    const uint32_t rn_value = state.reg(Bit::get_bits<0, 3>(opcode));
    const bool m_high = Bit::is_set<6>(opcode);
    const bool n_high = Bit::is_set<5>(opcode);

    const uint64_t operand1 = n_high ? (rn_value >> 16)
                                     : (rn_value & 0xFFFF);

    const uint64_t operand2 = m_high ? (rm_value >> 16)
                                     : (rm_value & 0xFFFF);

    const uint64_t accumulate = static_cast<uint64_t>(state.reg(rdhi_index)) << 32 | state.reg(rdlo_index);
    const uint64_t product = Bit::sign_extend<16>(operand1) * Bit::sign_extend<16>(operand2);
    const uint64_t result = product + accumulate;

    state.reg(rdhi_index) = static_cast<uint32_t>(result >> 32);
    state.reg(rdlo_index) = static_cast<uint32_t>(result);
}

void SMLAXY(State::ARMState& state, const uint32_t opcode)
{
    const uint32_t ra_index = Bit::get_bits<12, 15>(opcode);
    const uint32_t rd_index = Bit::get_bits<16, 19>(opcode);
    const bool m_high = Bit::is_set<6>(opcode);
    const bool n_high = Bit::is_set<5>(opcode);

    const uint64_t ra_value = Bit::sign_extend<32, uint64_t>(state.reg(ra_index));
    const uint64_t rm_value = state.reg(Bit::get_bits<0, 3>(opcode));
    const uint64_t rn_value = state.reg(Bit::get_bits<8, 11>(opcode));

    const uint64_t left  = n_high ? rn_value >> 16 : rn_value & 0xFFFF;
    const uint64_t right = m_high ? rm_value >> 16 : rm_value & 0xFFFF;
    const uint64_t result = (Bit::sign_extend<16>(left) * Bit::sign_extend<16>(right)) + ra_value;

    state.reg(rd_index) = static_cast<uint32_t>(result);

    // Signed overflow occurred
    if (result != Bit::sign_extend<32>(result & 0xFFFFFFFF))
        state.cpsr().q(1);
}

void SMULXY(State::ARMState& state, const uint32_t opcode)
{
    const uint32_t rd_index = Bit::get_bits<16, 19>(opcode);
    const bool m_high = Bit::is_set<6>(opcode);
    const bool n_high = Bit::is_set<5>(opcode);

    const uint32_t rm_value = state.reg(Bit::get_bits<0, 3>(opcode));
    const uint32_t rn_value = state.reg(Bit::get_bits<8, 11>(opcode));

    const uint32_t left  = n_high ? rn_value >> 16 : rn_value & 0xFFFF;
    const uint32_t right = m_high ? rm_value >> 16 : rm_value & 0xFFFF;
    const uint32_t result = Bit::sign_extend<16>(left) * Bit::sign_extend<16>(right);

    state.reg(rd_index) = result;
}

} // namespace ARM
} // namespace ForeARM

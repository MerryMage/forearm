#include "ForeARM/Interpreter/Instructions/ARM.hpp"
#include "ForeARM/Interpreter/Instructions/Helper/Arithmetic.hpp"
#include "ForeARM/Interpreter/State/ARMState.hpp"
#include "ForeARM/Utils/Bits.hpp"

namespace ForeARM
{
namespace ARM
{

void CPS(State::ARMState& state, const uint32_t opcode)
{
    if (State::current_mode_is_user(state))
        return;

    const bool change_mode  = Bit::is_set<17>(opcode);
    const bool affect_f     = Bit::is_set<6>(opcode);
    const bool affect_i     = Bit::is_set<7>(opcode);
    const bool affect_a     = Bit::is_set<8>(opcode);
    const uint32_t mode     = Bit::get_bits<0, 4>(opcode);
    const uint32_t imod     = Bit::get_bits<18, 19>(opcode);
    const bool enable       = imod == 0b10;
    const bool disable      = imod == 0b11;

    uint32_t cpsr_value = state.cpsr().value();

    if (enable)
    {
        if (affect_a)
            cpsr_value = Bit::set_bit<8>(cpsr_value);

        if (affect_i)
            cpsr_value = Bit::set_bit<7>(cpsr_value);

        if (affect_f)
            cpsr_value = Bit::set_bit<6>(cpsr_value);
    }
    else if (disable)
    {
        if (affect_a)
            cpsr_value = Bit::clear_bit<8>(cpsr_value);

        if (affect_i)
            cpsr_value = Bit::clear_bit<7>(cpsr_value);

        if (affect_f)
            cpsr_value = Bit::clear_bit<6>(cpsr_value);
    }

    if (change_mode)
        cpsr_value = Bit::clear_and_set_bits<0, 4>(cpsr_value, mode);

    // Write to all bits applicable bits
    State::write_cpsr_from_instruction(state, cpsr_value, 0b1111, false);
}

void ERET(State::ARMState&, const uint32_t)
{
}

void MRS(State::ARMState& state, const uint32_t opcode)
{
    const bool read_spsr    = Bit::is_set<22>(opcode);
    const uint32_t rd_index = Bit::get_bits<12, 15>(opcode);

    if (read_spsr)
    {
        if (!State::current_mode_is_user_or_system(state))
            state.reg(rd_index) = state.spsr().value();
    }
    else
    {
        const uint32_t cpsr_value = state.cpsr().value() & 0xF8FF03DF;

        // Mask away bits that aren't part of the APSR in user mode.
        if (State::current_mode_is_user(state))
            state.reg(rd_index) = cpsr_value & 0xF8FF0000;
        else
            state.reg(rd_index) = cpsr_value;
    }
}

void MRS_banked(State::ARMState&, const uint32_t)
{
}

void MSR_imm(State::ARMState& state, const uint32_t opcode)
{
    const bool write_spsr = Bit::is_set<22>(opcode);
    const uint32_t imm32  = Arithmetic::expand_immediate(Bit::get_bits<0, 11>(opcode));
    const uint32_t mask   = Bit::get_bits<16, 19>(opcode);

    if (write_spsr)
        State::write_spsr_from_instruction(state, imm32, mask);
    else
        State::write_cpsr_from_instruction(state, imm32, mask, false);
}

void MSR_reg(State::ARMState& state, const uint32_t opcode)
{
    const bool write_spsr   = Bit::is_set<22>(opcode);
    const uint32_t rn_value = state.reg(Bit::get_bits<0, 3>(opcode));
    const uint32_t mask     = Bit::get_bits<16, 19>(opcode);

    if (write_spsr)
        State::write_spsr_from_instruction(state, rn_value, mask);
    else
        State::write_cpsr_from_instruction(state, rn_value, mask, false);
}

void MSR_banked(State::ARMState&, const uint32_t)
{
}

void RFE(State::ARMState&, const uint32_t)
{
}

void SETEND(State::ARMState& state, const uint32_t opcode)
{
    state.cpsr().e(Bit::get_bit<9>(opcode));
}

void SRS(State::ARMState&, const uint32_t)
{
}

} // namespace ARM
} // namespace ForeARM

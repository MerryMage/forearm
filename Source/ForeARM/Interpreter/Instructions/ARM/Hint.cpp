#include "ForeARM/Interpreter/Instructions/ARM.hpp"
#include "ForeARM/Interpreter/State/ARMState.hpp"

namespace ForeARM
{
namespace ARM
{

void DBG(State::ARMState&, const uint32_t)
{
}

void PLD(State::ARMState&, const uint32_t)
{
}

void PLI(State::ARMState&, const uint32_t)
{
}

void SEV(State::ARMState&, const uint32_t)
{
}

void WFE(State::ARMState&, const uint32_t)
{
}

void WFI(State::ARMState&, const uint32_t)
{
}

void YIELD(State::ARMState&, const uint32_t)
{
}

} // namespace ARM
} // namespace ForeARM

#include "ForeARM/Interpreter/Instructions/ARM.hpp"
#include "ForeARM/Interpreter/State/ARMState.hpp"
#include "ForeARM/Utils/Bits.hpp"

namespace ForeARM
{
namespace ARM
{

void SDIV(State::ARMState& state, const uint32_t opcode)
{
    const uint32_t rd_index = Bit::get_bits<16, 19>(opcode);
    const int64_t rm_value = Bit::sign_extend<32, int64_t>(state.reg(Bit::get_bits<8, 11>(opcode)));
    const int64_t rn_value = Bit::sign_extend<32, int64_t>(state.reg(Bit::get_bits<0, 3>(opcode)));

    if (rm_value == 0)
    {
        // TODO: If ARMv7-R, this should result in an undefined instruction exception.
        //       However, on the ARMv7-A profile, returning zero is correct.
        //       Should check SCTLR.DZ bit (bit 19).
        state.reg(rd_index) = 0;
    }
    else
    {
        state.reg(rd_index) = static_cast<uint32_t>(rn_value / rm_value);
    }
}

void UDIV(State::ARMState& state, const uint32_t opcode)
{
    const uint32_t rd_index = Bit::get_bits<16, 19>(opcode);
    const uint32_t rm_value = state.reg(Bit::get_bits<8, 11>(opcode));
    const uint32_t rn_value = state.reg(Bit::get_bits<0, 3>(opcode));

    if (rm_value == 0)
    {
        // TODO: If ARMv7-R, this should result in an undefined instruction exception.
        //       However, on the ARMv7-A profile, returning zero is correct.
        //       Should check SCTLR.DZ bit (bit 19).
        state.reg(rd_index) = 0;
    }
    else
    {
        state.reg(rd_index) = rn_value / rm_value;
    }
}

} // namespace ARM
} // namespace ForeARM

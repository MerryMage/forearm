#include "ForeARM/Interpreter/Instructions/ARM.hpp"
#include "ForeARM/Interpreter/Instructions/Helper/Branch.hpp"
#include "ForeARM/Interpreter/State/ARMState.hpp"
#include "ForeARM/Utils/Bits.hpp"

namespace ForeARM
{
namespace ARM
{

void B(State::ARMState& state, const uint32_t opcode)
{
    const uint32_t pc = State::get_program_counter(state);
    const uint32_t imm32 = Bit::sign_extend<26>((opcode & 0xFFFFFF) << 2);

    Branch::branch_write_pc(state, pc + imm32);
}

// Merges BL and BLX immediate variants.
void BLX_imm(State::ARMState& state, const uint32_t opcode)
{
    const bool is_blx = Bit::get_bits<28, 31>(opcode) == 15;
    const uint32_t pc = State::get_program_counter(state);

    const uint32_t imm32 = [opcode, is_blx]() {
        uint32_t value = (opcode & 0xFFFFFF) << 2;

        if (is_blx)
            value |= Bit::get_bit<24>(opcode) << 1;

        return Bit::sign_extend<26>(value);
    }();

    if (state.current_instruction_set() == State::InstructionSet::ARM)
        state.reg(14) = pc - 4;
    else
        state.reg(14) = pc | 1;

    const State::InstructionSet target_instruction_set = is_blx
                               ? State::InstructionSet::Thumb
                               : State::InstructionSet::ARM;

    uint32_t target_address = 0;

    if (target_instruction_set == State::InstructionSet::ARM)
        target_address = State::get_word_aligned_program_counter(state) + imm32;
    else
        target_address = pc + imm32;

    state.select_instruction_set(State::InstructionSet::ARM);
    Branch::branch_write_pc(state, target_address);
}

void BLX_reg(State::ARMState& state, const uint32_t opcode)
{
    const uint32_t target = state.reg(Bit::get_bits<0, 3>(opcode));
    const uint32_t pc = State::get_program_counter(state);

    if (state.current_instruction_set() == State::InstructionSet::ARM)
    {
        state.reg(14) = pc - 4;
    }
    else
    {
        state.reg(14) = (pc - 2) | 1;
    }

    Branch::bx_write_pc(state, target);
}

void BX(State::ARMState& state, const uint32_t opcode)
{
    const uint32_t rm_value = State::get_register_value_and_offset_if_pc(state, opcode & 0xF);

    Branch::bx_write_pc(state, rm_value);
}

void BXJ(State::ARMState& state, const uint32_t opcode)
{
    // Since Jazelle implementation details aren't fully released
    // we simply emulate the fail-case where Rm specifies a
    // register with an address to take if the transition to
    // Jazelle state fails.
    //
    // Sorry if you wanted to emulate a Java ME-based game (lol).
    BX(state, opcode);
}

} // namespace ARM
} // namespace ForeARM

#include "ForeARM/Interpreter/Instructions/ARM.hpp"
#include "ForeARM/Interpreter/State/ARMState.hpp"
#include "ForeARM/Utils/Bits.hpp"

namespace ForeARM
{
namespace ARM
{

void BKPT(State::ARMState&, const uint32_t)
{
}

void HVC(State::ARMState&, const uint32_t)
{
}

void SMC(State::ARMState&, const uint32_t)
{
}

void SVC(State::ARMState&, const uint32_t)
{
}

void UDF(State::ARMState&, const uint32_t)
{
}

} // namespace ARM
} // namespace ForeARM

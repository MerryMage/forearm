#include "ForeARM/Interpreter/Instructions/ARM.hpp"
#include "ForeARM/Interpreter/Instructions/Helper/Arithmetic.hpp"
#include "ForeARM/Interpreter/Instructions/Helper/Branch.hpp"
#include "ForeARM/Interpreter/Instructions/Helper/Shift.hpp"
#include "ForeARM/Interpreter/State/ARMState.hpp"
#include "ForeARM/Utils/Bits.hpp"

namespace ForeARM
{
namespace ARM
{

void ADC_imm(State::ARMState& state, const uint32_t opcode)
{
    const uint32_t rd_index = Bit::get_bits<12, 15>(opcode);
    const uint32_t rn_index = Bit::get_bits<16, 19>(opcode);
    const uint32_t imm32    = Arithmetic::expand_immediate(Bit::get_bits<0, 11>(opcode));

    const uint32_t rn_value = State::get_register_value_and_offset_if_pc(state, rn_index);
    const auto result = Arithmetic::add_with_carry(rn_value, imm32, state.cpsr().c());

    if (rd_index == 15)
    {
        Branch::bx_write_pc(state, result.value);
    }
    else
    {
        state.reg(rd_index) = result.value;

        if (Bit::is_set<20>(opcode))
        {
            state.cpsr().n(result.value & 0x80000000);
            state.cpsr().z(result.value == 0);
            state.cpsr().c(result.carry_out);
            state.cpsr().v(result.overflow);
        }
    }
}

void ADC_reg(State::ARMState& state, const uint32_t opcode)
{
    const uint32_t rd_index   = Bit::get_bits<12, 15>(opcode);
    const uint32_t rm_index   = Bit::get_bits<0, 3>(opcode);
    const uint32_t rn_index   = Bit::get_bits<16, 19>(opcode);
    const uint32_t imm5       = Bit::get_bits<7, 11>(opcode);
    const uint32_t shift_type = Bit::get_bits<5, 6>(opcode);

    const uint32_t rm_value = State::get_register_value_and_offset_if_pc(state, rm_index);
    const uint32_t rn_value = State::get_register_value_and_offset_if_pc(state, rn_index);

    const auto imm_shift = Shift::decode_imm_shift(shift_type, imm5);
    const auto shifted_value = Shift::shift_without_carry(rm_value, imm_shift.type, imm_shift.amount, state.cpsr().c());
    const auto result = Arithmetic::add_with_carry(shifted_value, rn_value, state.cpsr().c());

    if (rd_index == 15)
    {
        Branch::bx_write_pc(state, result.value);
    }
    else
    {
        state.reg(rd_index) = result.value;

        if (Bit::is_set<20>(opcode))
        {
            state.cpsr().n(result.value & 0x80000000);
            state.cpsr().z(result.value == 0);
            state.cpsr().c(result.carry_out);
            state.cpsr().v(result.overflow);
        }
    }
}

void ADC_rsr(State::ARMState& state, const uint32_t opcode)
{
    const uint32_t rd_index = Bit::get_bits<12, 15>(opcode);
    const uint32_t rm_value = state.reg(Bit::get_bits<0, 3>(opcode));
    const uint32_t rn_value = state.reg(Bit::get_bits<16, 19>(opcode));
    const uint32_t rs_value = state.reg(Bit::get_bits<8, 11>(opcode));
    const Shift::ShiftType shift_type = Shift::decode_reg_shift(Bit::get_bits<5, 6>(opcode));

    const auto shifted_value = Shift::shift_without_carry(rm_value, shift_type, rs_value & 0xFF, state.cpsr().c());
    const auto result = Arithmetic::add_with_carry(rn_value, shifted_value, state.cpsr().c());

    state.reg(rd_index) = result.value;

    if (Bit::is_set<20>(opcode))
    {
        state.cpsr().n(result.value & 0x80000000);
        state.cpsr().z(result.value == 0);
        state.cpsr().c(result.carry_out);
        state.cpsr().v(result.overflow);
    }
}

void ADD_imm(State::ARMState& state, const uint32_t opcode)
{
    const uint32_t rn_index = Bit::get_bits<16, 19>(opcode);
    const uint32_t rd_index = Bit::get_bits<12, 15>(opcode);
    const uint32_t imm32    = Arithmetic::expand_immediate(opcode & 0xFFF);
    const uint32_t rn_value = State::get_register_value_and_offset_if_pc(state, rn_index);

    const auto result = Arithmetic::add_with_carry(rn_value, imm32, 0);

    if (rd_index == 15)
    {
        Branch::bx_write_pc(state, result.value);
    }
    else
    {
        state.reg(rd_index) = result.value;

        if (Bit::is_set<20>(opcode))
        {
            state.cpsr().n(result.value & 0x80000000);
            state.cpsr().z(result.value == 0);
            state.cpsr().c(result.carry_out);
            state.cpsr().v(result.overflow);
        }
    }
}

void ADD_reg(State::ARMState& state, const uint32_t opcode)
{
    const uint32_t rd_index = Bit::get_bits<12, 15>(opcode);
    const uint32_t rm_index = Bit::get_bits<0, 3>(opcode);
    const uint32_t rn_index = Bit::get_bits<16, 19>(opcode);

    const uint32_t rm_value = State::get_register_value_and_offset_if_pc(state, rm_index);
    const uint32_t rn_value = State::get_register_value_and_offset_if_pc(state, rn_index);

    const uint32_t imm5 = Bit::get_bits<7, 11>(opcode);
    const uint32_t type = Bit::get_bits<5, 6>(opcode);

    const auto imm_shift = Shift::decode_imm_shift(type, imm5);
    const auto shifted_value = Shift::shift_without_carry(rm_value, imm_shift.type, imm_shift.amount, state.cpsr().c());
    const auto result = Arithmetic::add_with_carry(rn_value, shifted_value, 0);

    if (rd_index == 15)
    {
        Branch::bx_write_pc(state, result.value);
    }
    else
    {
        state.reg(rd_index) = result.value;

        if (Bit::is_set<20>(opcode))
        {
            state.cpsr().n(result.value & 0x80000000);
            state.cpsr().z(result.value == 0);
            state.cpsr().c(result.carry_out);
            state.cpsr().v(result.overflow);
        }
    }
}

void ADD_rsr(State::ARMState& state, const uint32_t opcode)
{
    const uint32_t rd_index = Bit::get_bits<12, 15>(opcode);
    const uint32_t rm_value = state.reg(Bit::get_bits<0, 3>(opcode));
    const uint32_t rn_value = state.reg(Bit::get_bits<16, 19>(opcode));
    const uint32_t rs_value = state.reg(Bit::get_bits<8, 11>(opcode));

    const auto shift_type = Shift::decode_reg_shift(Bit::get_bits<5, 6>(opcode));

    const auto shifted_value = Shift::shift_without_carry(rm_value, shift_type, rs_value & 0xFF, state.cpsr().c());
    const auto result = Arithmetic::add_with_carry(rn_value, shifted_value, 0);

    state.reg(rd_index) = result.value;

    if (Bit::is_set<20>(opcode))
    {
        state.cpsr().n(result.value & 0x80000000);
        state.cpsr().z(result.value == 0);
        state.cpsr().c(result.carry_out);
        state.cpsr().v(result.overflow);
    }
}

void AND_imm(State::ARMState& state, const uint32_t opcode)
{
    const uint32_t rd_index = Bit::get_bits<12, 15>(opcode);
    const uint32_t rn_index = Bit::get_bits<16, 19>(opcode);
    const uint32_t rn_value = State::get_register_value_and_offset_if_pc(state, rn_index);

    const auto imm32 = Arithmetic::expand_immediate_with_carry_out(opcode & 0xFFF, state.cpsr().c());
    const uint32_t result = rn_value & imm32.value;

    if (rd_index == 15)
    {
        Branch::bx_write_pc(state, result);
    }
    else
    {
        state.reg(rd_index) = result;

        if (Bit::is_set<20>(opcode))
        {
            state.cpsr().n(result & 0x80000000);
            state.cpsr().z(result == 0);
            state.cpsr().c(imm32.carry_out);
        }
    }
}

void AND_reg(State::ARMState& state, const uint32_t opcode)
{
    const uint32_t rd_index = Bit::get_bits<12, 15>(opcode);
    const uint32_t rm_index = Bit::get_bits<0, 3>(opcode);
    const uint32_t rn_index = Bit::get_bits<16, 19>(opcode);

    const uint32_t rm_value = State::get_register_value_and_offset_if_pc(state, rm_index);
    const uint32_t rn_value = State::get_register_value_and_offset_if_pc(state, rn_index);

    const uint32_t imm5 = Bit::get_bits<7, 11>(opcode);
    const uint32_t type = Bit::get_bits<5, 6>(opcode);

    const auto imm_shift = Shift::decode_imm_shift(type, imm5);
    const auto shifted_value = Shift::shift_with_carry(rm_value, imm_shift.type, imm_shift.amount, state.cpsr().c());
    const auto result = rn_value & shifted_value.value;

    if (rd_index == 15)
    {
        Branch::bx_write_pc(state, result);
    }
    else
    {
        state.reg(rd_index) = result;

        if (Bit::is_set<20>(opcode))
        {
            state.cpsr().n(result & 0x80000000);
            state.cpsr().z(result == 0);
            state.cpsr().c(shifted_value.carry_out);
        }
    }
}

void AND_rsr(State::ARMState& state, const uint32_t opcode)
{
    const uint32_t rd_index = Bit::get_bits<12, 15>(opcode);
    const uint32_t rm_value = state.reg(Bit::get_bits<0, 3>(opcode));
    const uint32_t rn_value = state.reg(Bit::get_bits<16, 19>(opcode));
    const uint32_t rs_value = state.reg(Bit::get_bits<8, 11>(opcode));

    const auto shift_type = Shift::decode_reg_shift(Bit::get_bits<5, 6>(opcode));

    const auto shifted_value = Shift::shift_with_carry(rm_value, shift_type, rs_value & 0xFF, state.cpsr().c());
    const auto result = rn_value & shifted_value.value;

    state.reg(rd_index) = result;

    if (Bit::is_set<20>(opcode))
    {
        state.cpsr().n(result & 0x80000000);
        state.cpsr().z(result == 0);
        state.cpsr().c(shifted_value.carry_out);
    }
}

void BIC_imm(State::ARMState& state, const uint32_t opcode)
{
    const uint32_t rd_index = Bit::get_bits<12, 15>(opcode);
    const uint32_t rn_index = Bit::get_bits<16, 19>(opcode);
    const uint32_t rn_value = State::get_register_value_and_offset_if_pc(state, rn_index);

    const auto imm32 = Arithmetic::expand_immediate_with_carry_out(opcode & 0xFFF, state.cpsr().c());
    const uint32_t result = rn_value & ~imm32.value;

    if (rd_index == 15)
    {
        Branch::bx_write_pc(state, result);
    }
    else
    {
        state.reg(rd_index) = result;

        if (Bit::is_set<20>(opcode))
        {
            state.cpsr().n(result & 0x80000000);
            state.cpsr().z(result == 0);
            state.cpsr().c(imm32.carry_out);
        }
    }
}

void BIC_reg(State::ARMState& state, const uint32_t opcode)
{
    const uint32_t rd_index = Bit::get_bits<12, 15>(opcode);
    const uint32_t rm_index = Bit::get_bits<0, 3>(opcode);
    const uint32_t rn_index = Bit::get_bits<16, 19>(opcode);

    const uint32_t rm_value = State::get_register_value_and_offset_if_pc(state, rm_index);
    const uint32_t rn_value = State::get_register_value_and_offset_if_pc(state, rn_index);

    const uint32_t imm5 = Bit::get_bits<7, 11>(opcode);
    const uint32_t type = Bit::get_bits<5, 6>(opcode);

    const auto imm_shift = Shift::decode_imm_shift(type, imm5);
    const auto shifted_value = Shift::shift_with_carry(rm_value, imm_shift.type, imm_shift.amount, state.cpsr().c());
    const auto result = rn_value & ~shifted_value.value;

    if (rd_index == 15)
    {
        Branch::bx_write_pc(state, result);
    }
    else
    {
        state.reg(rd_index) = result;

        if (Bit::is_set<20>(opcode))
        {
            state.cpsr().n(result & 0x80000000);
            state.cpsr().z(result == 0);
            state.cpsr().c(shifted_value.carry_out);
        }
    }
}

void BIC_rsr(State::ARMState& state, const uint32_t opcode)
{
    const uint32_t rd_index = Bit::get_bits<12, 15>(opcode);
    const uint32_t rm_value = state.reg(Bit::get_bits<0, 3>(opcode));
    const uint32_t rn_value = state.reg(Bit::get_bits<16, 19>(opcode));
    const uint32_t rs_value = state.reg(Bit::get_bits<8, 11>(opcode));

    const auto shift_type = Shift::decode_reg_shift(Bit::get_bits<5, 6>(opcode));

    const auto shifted_value = Shift::shift_with_carry(rm_value, shift_type, rs_value & 0xFF, state.cpsr().c());
    const auto result = rn_value & ~shifted_value.value;

    state.reg(rd_index) = result;

    if (Bit::is_set<20>(opcode))
    {
        state.cpsr().n(result & 0x80000000);
        state.cpsr().z(result == 0);
        state.cpsr().c(shifted_value.carry_out);
    }
}

void CMN_imm(State::ARMState& state, const uint32_t opcode)
{
    const uint32_t rn_index = Bit::get_bits<16, 19>(opcode);
    const uint32_t rn_value = State::get_register_value_and_offset_if_pc(state, rn_index);
    const uint32_t imm32    = Arithmetic::expand_immediate(opcode & 0xFFF);
    const auto result       = Arithmetic::add_with_carry(rn_value, imm32, 0);

    state.cpsr().n(result.value & 0x80000000);
    state.cpsr().z(result.value == 0);
    state.cpsr().c(result.carry_out);
    state.cpsr().v(result.overflow);
}

void CMN_reg(State::ARMState& state, const uint32_t opcode)
{
    const uint32_t rm_index = Bit::get_bits<0, 3>(opcode);
    const uint32_t rn_index = Bit::get_bits<16, 19>(opcode);
    const uint32_t rm_value = State::get_register_value_and_offset_if_pc(state, rm_index);
    const uint32_t rn_value = State::get_register_value_and_offset_if_pc(state, rn_index);

    const uint32_t imm5 = Bit::get_bits<7, 11>(opcode);
    const uint32_t type = Bit::get_bits<5, 6>(opcode);

    const auto imm_shift = Shift::decode_imm_shift(type, imm5);
    const auto shifted_value = Shift::shift_without_carry(rm_value, imm_shift.type, imm_shift.amount, state.cpsr().c());
    const auto result = Arithmetic::add_with_carry(rn_value, shifted_value, 0);

    state.cpsr().n(result.value & 0x80000000);
    state.cpsr().z(result.value == 0);
    state.cpsr().c(result.carry_out);
    state.cpsr().v(result.overflow);
}

void CMN_rsr(State::ARMState& state, const uint32_t opcode)
{
    const uint32_t rm_value = state.reg(Bit::get_bits<0, 3>(opcode));
    const uint32_t rn_value = state.reg(Bit::get_bits<16, 19>(opcode));
    const uint32_t rs_value = state.reg(Bit::get_bits<8, 11>(opcode));

    const auto shift_type = Shift::decode_reg_shift(Bit::get_bits<5, 6>(opcode));
    const auto shifted_value = Shift::shift_with_carry(rm_value, shift_type, rs_value & 0xFF, state.cpsr().c());
    const auto result = Arithmetic::add_with_carry(rn_value, shifted_value.value, 0);

    state.cpsr().n(result.value & 0x80000000);
    state.cpsr().z(result.value == 0);
    state.cpsr().c(result.carry_out);
    state.cpsr().v(result.overflow);
}

void CMP_imm(State::ARMState& state, const uint32_t opcode)
{
    const uint32_t rn_index = Bit::get_bits<16, 19>(opcode);
    const uint32_t rn_value = State::get_register_value_and_offset_if_pc(state, rn_index);
    const uint32_t imm32    = Arithmetic::expand_immediate(opcode & 0xFFF);
    const auto result       = Arithmetic::add_with_carry(rn_value, ~imm32, 1);

    state.cpsr().n(result.value & 0x80000000);
    state.cpsr().z(result.value == 0);
    state.cpsr().c(result.carry_out);
    state.cpsr().v(result.overflow);
}

void CMP_reg(State::ARMState& state, const uint32_t opcode)
{
    const uint32_t rm_index = Bit::get_bits<0, 3>(opcode);
    const uint32_t rn_index = Bit::get_bits<16, 19>(opcode);
    const uint32_t rm_value = State::get_register_value_and_offset_if_pc(state, rm_index);
    const uint32_t rn_value = State::get_register_value_and_offset_if_pc(state, rn_index);

    const uint32_t imm5 = Bit::get_bits<7, 11>(opcode);
    const uint32_t type = Bit::get_bits<5, 6>(opcode);

    const auto imm_shift = Shift::decode_imm_shift(type, imm5);
    const auto shifted_value = Shift::shift_without_carry(rm_value, imm_shift.type, imm_shift.amount, state.cpsr().c());
    const auto result = Arithmetic::add_with_carry(rn_value, ~shifted_value, 1);

    state.cpsr().n(result.value & 0x80000000);
    state.cpsr().z(result.value == 0);
    state.cpsr().c(result.carry_out);
    state.cpsr().v(result.overflow);
}

void CMP_rsr(State::ARMState& state, const uint32_t opcode)
{
    const uint32_t rm_value = state.reg(Bit::get_bits<0, 3>(opcode));
    const uint32_t rn_value = state.reg(Bit::get_bits<16, 19>(opcode));
    const uint32_t rs_value = state.reg(Bit::get_bits<8, 11>(opcode));

    const auto shift_type = Shift::decode_reg_shift(Bit::get_bits<5, 6>(opcode));
    const auto shifted_value = Shift::shift_with_carry(rm_value, shift_type, rs_value & 0xFF, state.cpsr().c());
    const auto result = Arithmetic::add_with_carry(rn_value, ~shifted_value.value, 1);

    state.cpsr().n(result.value & 0x80000000);
    state.cpsr().z(result.value == 0);
    state.cpsr().c(result.carry_out);
    state.cpsr().v(result.overflow);
}

void EOR_imm(State::ARMState& state, const uint32_t opcode)
{
    const uint32_t rd_index = Bit::get_bits<12, 15>(opcode);
    const uint32_t rn_index = Bit::get_bits<16, 19>(opcode);
    const uint32_t rn_value = State::get_register_value_and_offset_if_pc(state, rn_index);

    const auto imm32 = Arithmetic::expand_immediate_with_carry_out(opcode & 0xFFF, state.cpsr().c());
    const uint32_t result = rn_value ^ imm32.value;

    if (rd_index == 15)
    {
        Branch::bx_write_pc(state, result);
    }
    else
    {
        state.reg(rd_index) = result;

        if (Bit::is_set<20>(opcode))
        {
            state.cpsr().n(result & 0x80000000);
            state.cpsr().z(result == 0);
            state.cpsr().c(imm32.carry_out);
        }
    }
}

void EOR_reg(State::ARMState& state, const uint32_t opcode)
{
    const uint32_t rd_index = Bit::get_bits<12, 15>(opcode);
    const uint32_t rm_index = Bit::get_bits<0, 3>(opcode);
    const uint32_t rn_index = Bit::get_bits<16, 19>(opcode);

    const uint32_t rm_value = State::get_register_value_and_offset_if_pc(state, rm_index);
    const uint32_t rn_value = State::get_register_value_and_offset_if_pc(state, rn_index);

    const uint32_t imm5 = Bit::get_bits<7, 11>(opcode);
    const uint32_t type = Bit::get_bits<5, 6>(opcode);

    const auto imm_shift = Shift::decode_imm_shift(type, imm5);
    const auto shifted_value = Shift::shift_with_carry(rm_value, imm_shift.type, imm_shift.amount, state.cpsr().c());
    const auto result = rn_value ^ shifted_value.value;

    if (rd_index == 15)
    {
        Branch::bx_write_pc(state, result);
    }
    else
    {
        state.reg(rd_index) = result;

        if (Bit::is_set<20>(opcode))
        {
            state.cpsr().n(result & 0x80000000);
            state.cpsr().z(result == 0);
            state.cpsr().c(shifted_value.carry_out);
        }
    }
}

void EOR_rsr(State::ARMState& state, const uint32_t opcode)
{
    const uint32_t rd_index = Bit::get_bits<12, 15>(opcode);
    const uint32_t rm_value = state.reg(Bit::get_bits<0, 3>(opcode));
    const uint32_t rn_value = state.reg(Bit::get_bits<16, 19>(opcode));
    const uint32_t rs_value = state.reg(Bit::get_bits<8, 11>(opcode));

    const auto shift_type = Shift::decode_reg_shift(Bit::get_bits<5, 6>(opcode));

    const auto shifted_value = Shift::shift_with_carry(rm_value, shift_type, rs_value & 0xFF, state.cpsr().c());
    const auto result = rn_value & shifted_value.value;

    state.reg(rd_index) = result;

    if (Bit::is_set<20>(opcode))
    {
        state.cpsr().n(result & 0x80000000);
        state.cpsr().z(result == 0);
        state.cpsr().c(shifted_value.carry_out);
    }
}

void MOV_imm(State::ARMState& state, const uint32_t opcode)
{
    const uint32_t rd_index = Bit::get_bits<12, 15>(opcode);
    const auto imm32 = Arithmetic::expand_immediate_with_carry_out(opcode & 0xFFF, state.cpsr().c());
    const uint32_t result = imm32.value;

    if (rd_index == 15)
    {
        Branch::bx_write_pc(state, result);
    }
    else
    {
        state.reg(rd_index) = result;

        if (Bit::is_set<20>(opcode))
        {
            state.cpsr().n(result & 0x80000000);
            state.cpsr().z(result == 0);
            state.cpsr().c(imm32.carry_out);
        }
    }
}

void MOV_reg(State::ARMState& state, const uint32_t opcode)
{
    const uint32_t rd_index = Bit::get_bits<12, 15>(opcode);
    const uint32_t rm_value = State::get_register_value_and_offset_if_pc(state, opcode & 0xF);

    if (rd_index == 15)
    {
        Branch::bx_write_pc(state, rm_value);
    }
    else
    {
        state.reg(rd_index) = rm_value;

        if (Bit::is_set<20>(opcode))
        {
            state.cpsr().n(rm_value & 0x80000000);
            state.cpsr().z(rm_value == 0);
        }
    }
}

void MOVT(State::ARMState& state, const uint32_t opcode)
{
    const uint32_t rd_index = Bit::get_bits<12, 15>(opcode);
    const uint32_t imm16    = ((opcode >> 4) & 0xF000) | (opcode & 0xFFF);

    state.reg(rd_index) = (state.reg(rd_index) & ~0xFFFF0000) | (imm16 << 16);
}

void MOVW(State::ARMState& state, const uint32_t opcode)
{
    const uint32_t rd_index = Bit::get_bits<12, 15>(opcode);
    const uint32_t imm16    = ((opcode >> 4) & 0xF000) | (opcode & 0xFFF);

    state.reg(rd_index) = imm16;
}

void MVN_imm(State::ARMState& state, const uint32_t opcode)
{
    const uint32_t rd_index = Bit::get_bits<12, 15>(opcode);
    const auto imm32 = Arithmetic::expand_immediate_with_carry_out(opcode & 0xFFF, state.cpsr().c());
    const uint32_t result = ~imm32.value;

    if (rd_index == 15)
    {
        Branch::bx_write_pc(state, result);
    }
    else
    {
        state.reg(rd_index) = result;

        if (Bit::is_set<20>(opcode))
        {
            state.cpsr().n(result & 0x80000000);
            state.cpsr().z(result == 0);
            state.cpsr().c(imm32.carry_out);
        }
    }
}

void MVN_reg(State::ARMState& state, const uint32_t opcode)
{
    const uint32_t rd_index = Bit::get_bits<12, 15>(opcode);
    const uint32_t rm_index = Bit::get_bits<0, 3>(opcode);
    const uint32_t rm_value = State::get_register_value_and_offset_if_pc(state, rm_index);

    const uint32_t imm5 = Bit::get_bits<7, 11>(opcode);
    const uint32_t type = Bit::get_bits<5, 6>(opcode);

    const auto imm_shift = Shift::decode_imm_shift(type, imm5);
    const auto shifted_value = Shift::shift_with_carry(rm_value, imm_shift.type, imm_shift.amount, state.cpsr().c());
    const auto result = ~shifted_value.value;

    if (rd_index == 15)
    {
        Branch::bx_write_pc(state, result);
    }
    else
    {
        state.reg(rd_index) = result;

        if (Bit::is_set<20>(opcode))
        {
            state.cpsr().n(result & 0x80000000);
            state.cpsr().z(result == 0);
            state.cpsr().c(shifted_value.carry_out);
        }
    }
}

void MVN_rsr(State::ARMState& state, const uint32_t opcode)
{
    const uint32_t rd_index = Bit::get_bits<12, 15>(opcode);
    const uint32_t rm_value = state.reg(Bit::get_bits<0, 3>(opcode));
    const uint32_t rs_value = state.reg(Bit::get_bits<8, 11>(opcode));

    const auto shift_type = Shift::decode_reg_shift(Bit::get_bits<5, 6>(opcode));
    const auto shifted_value = Shift::shift_with_carry(rm_value, shift_type, rs_value & 0xFF, state.cpsr().c());
    const auto result = ~shifted_value.value;

    state.reg(rd_index) = result;

    if (Bit::is_set<20>(opcode))
    {
        state.cpsr().n(result & 0x80000000);
        state.cpsr().z(result == 0);
        state.cpsr().c(shifted_value.carry_out);
    }
}

void ORR_imm(State::ARMState& state, const uint32_t opcode)
{
    const uint32_t rd_index = Bit::get_bits<12, 15>(opcode);
    const uint32_t rn_index = Bit::get_bits<16, 19>(opcode);
    const uint32_t rn_value = State::get_register_value_and_offset_if_pc(state, rn_index);

    const auto imm32 = Arithmetic::expand_immediate_with_carry_out(opcode & 0xFFF, state.cpsr().c());
    const uint32_t result = rn_value | imm32.value;

    if (rd_index == 15)
    {
        Branch::bx_write_pc(state, result);
    }
    else
    {
        state.reg(rd_index) = result;

        if (Bit::is_set<20>(opcode))
        {
            state.cpsr().n(result & 0x80000000);
            state.cpsr().z(result == 0);
            state.cpsr().c(imm32.carry_out);
        }
    }
}

void ORR_reg(State::ARMState& state, const uint32_t opcode)
{
    const uint32_t rd_index = Bit::get_bits<12, 15>(opcode);
    const uint32_t rm_index = Bit::get_bits<0, 3>(opcode);
    const uint32_t rn_index = Bit::get_bits<16, 19>(opcode);

    const uint32_t rm_value = State::get_register_value_and_offset_if_pc(state, rm_index);
    const uint32_t rn_value = State::get_register_value_and_offset_if_pc(state, rn_index);

    const uint32_t imm5 = Bit::get_bits<7, 11>(opcode);
    const uint32_t type = Bit::get_bits<5, 6>(opcode);

    const auto imm_shift = Shift::decode_imm_shift(type, imm5);
    const auto shifted_value = Shift::shift_with_carry(rm_value, imm_shift.type, imm_shift.amount, state.cpsr().c());
    const auto result = rn_value | shifted_value.value;

    if (rd_index == 15)
    {
        Branch::bx_write_pc(state, result);
    }
    else
    {
        state.reg(rd_index) = result;

        if (Bit::is_set<20>(opcode))
        {
            state.cpsr().n(result & 0x80000000);
            state.cpsr().z(result == 0);
            state.cpsr().c(shifted_value.carry_out);
        }
    }
}

void ORR_rsr(State::ARMState& state, const uint32_t opcode)
{
    const uint32_t rd_index = Bit::get_bits<12, 15>(opcode);
    const uint32_t rm_value = state.reg(Bit::get_bits<0, 3>(opcode));
    const uint32_t rn_value = state.reg(Bit::get_bits<16, 19>(opcode));
    const uint32_t rs_value = state.reg(Bit::get_bits<8, 11>(opcode));

    const auto shift_type = Shift::decode_reg_shift(Bit::get_bits<5, 6>(opcode));
    const auto shifted_value = Shift::shift_with_carry(rm_value, shift_type, rs_value & 0xFF, state.cpsr().c());
    const auto result = rn_value | shifted_value.value;

    state.reg(rd_index) = result;

    if (Bit::is_set<20>(opcode))
    {
        state.cpsr().n(result & 0x80000000);
        state.cpsr().z(result == 0);
        state.cpsr().c(shifted_value.carry_out);
    }
}

void RSB_imm(State::ARMState& state, const uint32_t opcode)
{
    const uint32_t rd_index = Bit::get_bits<12, 15>(opcode);
    const uint32_t rn_index = Bit::get_bits<16, 19>(opcode);
    const uint32_t rn_value = State::get_register_value_and_offset_if_pc(state, rn_index);

    const auto imm32 = Arithmetic::expand_immediate(opcode & 0xFFF);
    const auto result = Arithmetic::add_with_carry(~rn_value, imm32, 1);

    if (rd_index == 15)
    {
        Branch::bx_write_pc(state, result.value);
    }
    else
    {
        state.reg(rd_index) = result.value;

        if (Bit::is_set<20>(opcode))
        {
            state.cpsr().n(result.value & 0x80000000);
            state.cpsr().z(result.value == 0);
            state.cpsr().c(result.carry_out);
            state.cpsr().v(result.overflow);
        }
    }
}

void RSB_reg(State::ARMState& state, const uint32_t opcode)
{
    const uint32_t rd_index = Bit::get_bits<12, 15>(opcode);
    const uint32_t rm_index = Bit::get_bits<0, 3>(opcode);
    const uint32_t rn_index = Bit::get_bits<16, 19>(opcode);

    const uint32_t rm_value = State::get_register_value_and_offset_if_pc(state, rm_index);
    const uint32_t rn_value = State::get_register_value_and_offset_if_pc(state, rn_index);

    const uint32_t imm5 = Bit::get_bits<7, 11>(opcode);
    const uint32_t type = Bit::get_bits<5, 6>(opcode);

    const auto imm_shift = Shift::decode_imm_shift(type, imm5);
    const auto shifted_value = Shift::shift_with_carry(rm_value, imm_shift.type, imm_shift.amount, state.cpsr().c());
    const auto result = Arithmetic::add_with_carry(~rn_value, shifted_value.value, 1);

    if (rd_index == 15)
    {
        Branch::bx_write_pc(state, result.value);
    }
    else
    {
        state.reg(rd_index) = result.value;
        
        if (Bit::is_set<20>(opcode))
        {
            state.cpsr().n(result.value & 0x80000000);
            state.cpsr().z(result.value == 0);
            state.cpsr().c(result.carry_out);
            state.cpsr().v(result.overflow);
        }
    }
}

void RSB_rsr(State::ARMState& state, const uint32_t opcode)
{
    const uint32_t rd_index = Bit::get_bits<12, 15>(opcode);
    const uint32_t rm_value = state.reg(Bit::get_bits<0, 3>(opcode));
    const uint32_t rn_value = state.reg(Bit::get_bits<16, 19>(opcode));
    const uint32_t rs_value = state.reg(Bit::get_bits<8, 11>(opcode));

    const auto shift_type = Shift::decode_reg_shift(Bit::get_bits<5, 6>(opcode));
    const auto shifted_value = Shift::shift_with_carry(rm_value, shift_type, rs_value & 0xFF, state.cpsr().c());
    const auto result = Arithmetic::add_with_carry(~rn_value, shifted_value.value, 1);

    state.reg(rd_index) = result.value;

    if (Bit::is_set<20>(opcode))
    {
        state.cpsr().n(result.value & 0x80000000);
        state.cpsr().z(result.value == 0);
        state.cpsr().c(result.carry_out);
        state.cpsr().v(result.overflow);
    }
}

void RSC_imm(State::ARMState& state, const uint32_t opcode)
{
    const uint32_t rd_index = Bit::get_bits<12, 15>(opcode);
    const uint32_t rn_index = Bit::get_bits<16, 19>(opcode);
    const uint32_t rn_value = State::get_register_value_and_offset_if_pc(state, rn_index);

    const auto imm32 = Arithmetic::expand_immediate(opcode & 0xFFF);
    const auto result = Arithmetic::add_with_carry(~rn_value, imm32, state.cpsr().c());

    if (rd_index == 15)
    {
        Branch::bx_write_pc(state, result.value);
    }
    else
    {
        state.reg(rd_index) = result.value;

        if (Bit::is_set<20>(opcode))
        {
            state.cpsr().n(result.value & 0x80000000);
            state.cpsr().z(result.value == 0);
            state.cpsr().c(result.carry_out);
            state.cpsr().v(result.overflow);
        }
    }
}

void RSC_reg(State::ARMState& state, const uint32_t opcode)
{
    const uint32_t rd_index = Bit::get_bits<12, 15>(opcode);
    const uint32_t rm_index = Bit::get_bits<0, 3>(opcode);
    const uint32_t rn_index = Bit::get_bits<16, 19>(opcode);

    const uint32_t rm_value = State::get_register_value_and_offset_if_pc(state, rm_index);
    const uint32_t rn_value = State::get_register_value_and_offset_if_pc(state, rn_index);

    const uint32_t imm5 = Bit::get_bits<7, 11>(opcode);
    const uint32_t type = Bit::get_bits<5, 6>(opcode);

    const auto imm_shift = Shift::decode_imm_shift(type, imm5);
    const auto shifted_value = Shift::shift_with_carry(rm_value, imm_shift.type, imm_shift.amount, state.cpsr().c());
    const auto result = Arithmetic::add_with_carry(~rn_value, shifted_value.value, state.cpsr().c());

    if (rd_index == 15)
    {
        Branch::bx_write_pc(state, result.value);
    }
    else
    {
        state.reg(rd_index) = result.value;

        if (Bit::is_set<20>(opcode))
        {
            state.cpsr().n(result.value & 0x80000000);
            state.cpsr().z(result.value == 0);
            state.cpsr().c(result.carry_out);
            state.cpsr().v(result.overflow);
        }
    }
}

void RSC_rsr(State::ARMState& state, const uint32_t opcode)
{
    const uint32_t rd_index = Bit::get_bits<12, 15>(opcode);
    const uint32_t rm_value = state.reg(Bit::get_bits<0, 3>(opcode));
    const uint32_t rn_value = state.reg(Bit::get_bits<16, 19>(opcode));
    const uint32_t rs_value = state.reg(Bit::get_bits<8, 11>(opcode));

    const auto shift_type = Shift::decode_reg_shift(Bit::get_bits<5, 6>(opcode));
    const auto shifted_value = Shift::shift_with_carry(rm_value, shift_type, rs_value & 0xFF, state.cpsr().c());
    const auto result = Arithmetic::add_with_carry(~rn_value, shifted_value.value, state.cpsr().c());

    state.reg(rd_index) = result.value;

    if (Bit::is_set<20>(opcode))
    {
        state.cpsr().n(result.value & 0x80000000);
        state.cpsr().z(result.value == 0);
        state.cpsr().c(result.carry_out);
        state.cpsr().v(result.overflow);
    }
}

void SBC_imm(State::ARMState& state, const uint32_t opcode)
{
    const uint32_t rd_index = Bit::get_bits<12, 15>(opcode);
    const uint32_t rn_index = Bit::get_bits<16, 19>(opcode);
    const uint32_t rn_value = State::get_register_value_and_offset_if_pc(state, rn_index);

    const auto imm32 = Arithmetic::expand_immediate(opcode & 0xFFF);
    const auto result = Arithmetic::add_with_carry(rn_value, ~imm32, state.cpsr().c());

    if (rd_index == 15)
    {
        Branch::bx_write_pc(state, result.value);
    }
    else
    {
        state.reg(rd_index) = result.value;

        if (Bit::is_set<20>(opcode))
        {
            state.cpsr().n(result.value & 0x80000000);
            state.cpsr().z(result.value == 0);
            state.cpsr().c(result.carry_out);
            state.cpsr().v(result.overflow);
        }
    }
}

void SBC_reg(State::ARMState& state, const uint32_t opcode)
{
    const uint32_t rd_index = Bit::get_bits<12, 15>(opcode);
    const uint32_t rm_index = Bit::get_bits<0, 3>(opcode);
    const uint32_t rn_index = Bit::get_bits<16, 19>(opcode);

    const uint32_t rm_value = State::get_register_value_and_offset_if_pc(state, rm_index);
    const uint32_t rn_value = State::get_register_value_and_offset_if_pc(state, rn_index);

    const uint32_t imm5 = Bit::get_bits<7, 11>(opcode);
    const uint32_t type = Bit::get_bits<5, 6>(opcode);

    const auto imm_shift = Shift::decode_imm_shift(type, imm5);
    const auto shifted_value = Shift::shift_with_carry(rm_value, imm_shift.type, imm_shift.amount, state.cpsr().c());
    const auto result = Arithmetic::add_with_carry(rn_value, ~shifted_value.value, state.cpsr().c());

    if (rd_index == 15)
    {
        Branch::bx_write_pc(state, result.value);
    }
    else
    {
        state.reg(rd_index) = result.value;

        if (Bit::is_set<20>(opcode))
        {
            state.cpsr().n(result.value & 0x80000000);
            state.cpsr().z(result.value == 0);
            state.cpsr().c(result.carry_out);
            state.cpsr().v(result.overflow);
        }
    }
}

void SBC_rsr(State::ARMState& state, const uint32_t opcode)
{
    const uint32_t rd_index = Bit::get_bits<12, 15>(opcode);
    const uint32_t rm_value = state.reg(Bit::get_bits<0, 3>(opcode));
    const uint32_t rn_value = state.reg(Bit::get_bits<16, 19>(opcode));
    const uint32_t rs_value = state.reg(Bit::get_bits<8, 11>(opcode));

    const auto shift_type = Shift::decode_reg_shift(Bit::get_bits<5, 6>(opcode));
    const auto shifted_value = Shift::shift_with_carry(rm_value, shift_type, rs_value & 0xFF, state.cpsr().c());
    const auto result = Arithmetic::add_with_carry(rn_value, ~shifted_value.value, state.cpsr().c());

    state.reg(rd_index) = result.value;

    if (Bit::is_set<20>(opcode))
    {
        state.cpsr().n(result.value & 0x80000000);
        state.cpsr().z(result.value == 0);
        state.cpsr().c(result.carry_out);
        state.cpsr().v(result.overflow);
    }
}

void SUB_imm(State::ARMState& state, const uint32_t opcode)
{
    const uint32_t rd_index = Bit::get_bits<12, 15>(opcode);
    const uint32_t rn_index = Bit::get_bits<16, 19>(opcode);
    const uint32_t rn_value = State::get_register_value_and_offset_if_pc(state, rn_index);

    const auto imm32 = Arithmetic::expand_immediate(opcode & 0xFFF);
    const auto result = Arithmetic::add_with_carry(rn_value, ~imm32, 1);

    if (rd_index == 15)
    {
        Branch::bx_write_pc(state, result.value);
    }
    else
    {
        state.reg(rd_index) = result.value;

        if (Bit::is_set<20>(opcode))
        {
            state.cpsr().n(result.value & 0x80000000);
            state.cpsr().z(result.value == 0);
            state.cpsr().c(result.carry_out);
            state.cpsr().v(result.overflow);
        }
    }
}

void SUB_reg(State::ARMState& state, const uint32_t opcode)
{
    const uint32_t rd_index = Bit::get_bits<12, 15>(opcode);
    const uint32_t rm_index = Bit::get_bits<0, 3>(opcode);
    const uint32_t rn_index = Bit::get_bits<16, 19>(opcode);

    const uint32_t rm_value = State::get_register_value_and_offset_if_pc(state, rm_index);
    const uint32_t rn_value = State::get_register_value_and_offset_if_pc(state, rn_index);

    const uint32_t imm5 = Bit::get_bits<7, 11>(opcode);
    const uint32_t type = Bit::get_bits<5, 6>(opcode);

    const auto imm_shift = Shift::decode_imm_shift(type, imm5);
    const auto shifted_value = Shift::shift_with_carry(rm_value, imm_shift.type, imm_shift.amount, state.cpsr().c());
    const auto result = Arithmetic::add_with_carry(rn_value, ~shifted_value.value, 1);

    if (rd_index == 15)
    {
        Branch::bx_write_pc(state, result.value);
    }
    else
    {
        state.reg(rd_index) = result.value;

        if (Bit::is_set<20>(opcode))
        {
            state.cpsr().n(result.value & 0x80000000);
            state.cpsr().z(result.value == 0);
            state.cpsr().c(result.carry_out);
            state.cpsr().v(result.overflow);
        }
    }
}

void SUB_rsr(State::ARMState& state, const uint32_t opcode)
{
    const uint32_t rd_index = Bit::get_bits<12, 15>(opcode);
    const uint32_t rm_value = state.reg(Bit::get_bits<0, 3>(opcode));
    const uint32_t rn_value = state.reg(Bit::get_bits<16, 19>(opcode));
    const uint32_t rs_value = state.reg(Bit::get_bits<8, 11>(opcode));

    const auto shift_type = Shift::decode_reg_shift(Bit::get_bits<5, 6>(opcode));
    const auto shifted_value = Shift::shift_with_carry(rm_value, shift_type, rs_value & 0xFF, state.cpsr().c());
    const auto result = Arithmetic::add_with_carry(rn_value, ~shifted_value.value, 1);

    state.reg(rd_index) = result.value;

    if (Bit::is_set<20>(opcode))
    {
        state.cpsr().n(result.value & 0x80000000);
        state.cpsr().z(result.value == 0);
        state.cpsr().c(result.carry_out);
        state.cpsr().v(result.overflow);
    }
}

void TEQ_imm(State::ARMState& state, const uint32_t opcode)
{
    const uint32_t rn_index = Bit::get_bits<16, 19>(opcode);
    const uint32_t rn_value = State::get_register_value_and_offset_if_pc(state, rn_index);

    const auto imm32 = Arithmetic::expand_immediate_with_carry_out(opcode & 0xFFF, state.cpsr().c());
    const auto result = rn_value ^ imm32.value;

    state.cpsr().n(result & 0x80000000);
    state.cpsr().z(result == 0);
    state.cpsr().c(imm32.carry_out);
}

void TEQ_reg(State::ARMState& state, const uint32_t opcode)
{
    const uint32_t rm_index = Bit::get_bits<0, 3>(opcode);
    const uint32_t rn_index = Bit::get_bits<16, 19>(opcode);
    const uint32_t rm_value = State::get_register_value_and_offset_if_pc(state, rm_index);
    const uint32_t rn_value = State::get_register_value_and_offset_if_pc(state, rn_index);

    const uint32_t imm5 = Bit::get_bits<7, 11>(opcode);
    const uint32_t type = Bit::get_bits<5, 6>(opcode);

    const auto imm_shift = Shift::decode_imm_shift(type, imm5);
    const auto shifted_value = Shift::shift_with_carry(rm_value, imm_shift.type, imm_shift.amount, state.cpsr().c());
    const auto result = rn_value ^ shifted_value.value;

    state.cpsr().n(result & 0x80000000);
    state.cpsr().z(result == 0);
    state.cpsr().c(shifted_value.carry_out);
}

void TEQ_rsr(State::ARMState& state, const uint32_t opcode)
{
    const uint32_t rm_value = state.reg(Bit::get_bits<0, 3>(opcode));
    const uint32_t rn_value = state.reg(Bit::get_bits<16, 19>(opcode));
    const uint32_t rs_value = state.reg(Bit::get_bits<8, 11>(opcode));

    const auto shift_type = Shift::decode_reg_shift(Bit::get_bits<5, 6>(opcode));
    const auto shifted_value = Shift::shift_with_carry(rm_value, shift_type, rs_value & 0xFF, state.cpsr().c());
    const auto result = rn_value ^ shifted_value.value;

    state.cpsr().n(result & 0x80000000);
    state.cpsr().z(result == 0);
    state.cpsr().c(shifted_value.carry_out);
}

void TST_imm(State::ARMState& state, const uint32_t opcode)
{
    const uint32_t rn_index = Bit::get_bits<16, 19>(opcode);
    const uint32_t rn_value = State::get_register_value_and_offset_if_pc(state, rn_index);

    const auto imm32 = Arithmetic::expand_immediate_with_carry_out(opcode & 0xFFF, state.cpsr().c());
    const auto result = rn_value & imm32.value;

    state.cpsr().n(result & 0x80000000);
    state.cpsr().z(result == 0);
    state.cpsr().c(imm32.carry_out);
}

void TST_reg(State::ARMState& state, const uint32_t opcode)
{
    const uint32_t rm_index = Bit::get_bits<0, 3>(opcode);
    const uint32_t rn_index = Bit::get_bits<16, 19>(opcode);
    const uint32_t rm_value = State::get_register_value_and_offset_if_pc(state, rm_index);
    const uint32_t rn_value = State::get_register_value_and_offset_if_pc(state, rn_index);

    const uint32_t imm5 = Bit::get_bits<7, 11>(opcode);
    const uint32_t type = Bit::get_bits<5, 6>(opcode);

    const auto imm_shift = Shift::decode_imm_shift(type, imm5);
    const auto shifted_value = Shift::shift_with_carry(rm_value, imm_shift.type, imm_shift.amount, state.cpsr().c());
    const auto result = rn_value & shifted_value.value;

    state.cpsr().n(result & 0x80000000);
    state.cpsr().z(result == 0);
    state.cpsr().c(shifted_value.carry_out);
}

void TST_rsr(State::ARMState& state, const uint32_t opcode)
{
    const uint32_t rm_value = state.reg(Bit::get_bits<0, 3>(opcode));
    const uint32_t rn_value = state.reg(Bit::get_bits<16, 19>(opcode));
    const uint32_t rs_value = state.reg(Bit::get_bits<8, 11>(opcode));

    const auto shift_type = Shift::decode_reg_shift(Bit::get_bits<5, 6>(opcode));
    const auto shifted_value = Shift::shift_with_carry(rm_value, shift_type, rs_value & 0xFF, state.cpsr().c());
    const auto result = rn_value & shifted_value.value;

    state.cpsr().n(result & 0x80000000);
    state.cpsr().z(result == 0);
    state.cpsr().c(shifted_value.carry_out);
}

} // namespace ARM
} // namespace ForeARM

#include "ForeARM/Interpreter/Instructions/ARM.hpp"
#include "ForeARM/Interpreter/Instructions/Helper/Shift.hpp"
#include "ForeARM/Interpreter/State/ARMState.hpp"
#include "ForeARM/Utils/Bits.hpp"

namespace ForeARM
{
namespace ARM
{

void PKH(State::ARMState& state, const uint32_t opcode)
{
    const uint32_t rd_index = Bit::get_bits<12, 15>(opcode);
    const uint32_t rm_value = state.reg(Bit::get_bits<0, 3>(opcode));
    const uint32_t rn_value = state.reg(Bit::get_bits<16, 19>(opcode));

    const uint32_t tb_bit = Bit::get_bit<6>(opcode);
    const uint32_t shift_type = tb_bit << 1;
    const uint32_t imm5 = Bit::get_bits<7, 11>(opcode);

    const auto shift = Shift::decode_imm_shift(shift_type, imm5);
    const auto result = Shift::shift_without_carry(rm_value, shift.type, shift.amount, state.cpsr().c());

    if (tb_bit)
        state.reg(rd_index) = (rn_value & 0xFFFF0000) | (result & 0xFFFF);
    else
        state.reg(rd_index) = (result & 0xFFFF0000) | (rn_value & 0xFFFF);
}

} // namespace ARM
} // namespace ForeARM

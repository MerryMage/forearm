#include "ForeARM/Interpreter/Instructions/ARM.hpp"
#include "ForeARM/Interpreter/State/ARMState.hpp"
#include "ForeARM/Utils/Bits.hpp"
#include "ForeARM/Utils/Endian.hpp"

namespace ForeARM
{
namespace ARM
{

void RBIT(State::ARMState& state, const uint32_t opcode)
{
    const uint32_t rd_index = Bit::get_bits<12, 15>(opcode);
    const uint32_t rm_value = state.reg(Bit::get_bits<0, 3>(opcode));

    uint32_t result = 0;
    for (unsigned int i = 0; i < 32; ++i)
        result |= Bit::get_bit(rm_value, i) << (31 - i);

    state.reg(rd_index) = result;
}

void REV(State::ARMState& state, const uint32_t opcode)
{
    const uint32_t rd_index = Bit::get_bits<12, 15>(opcode);
    const uint32_t rm_index = Bit::get_bits<0, 3>(opcode);

    state.reg(rd_index) = Endian::swap32(state.reg(rm_index));
}

void REV16(State::ARMState& state, const uint32_t opcode)
{
    const uint32_t rd_index = Bit::get_bits<12, 15>(opcode);
    const uint32_t rm_value = state.reg(Bit::get_bits<0, 3>(opcode));

    const uint32_t rm_low  = Endian::swap16(static_cast<uint16_t>(Bit::get_bits<0, 15>(rm_value)));
    const uint32_t rm_high = Endian::swap16(static_cast<uint16_t>(Bit::get_bits<16, 31>(rm_value)));

    state.reg(rd_index) = (rm_high << 16) | rm_low;
}

void REVSH(State::ARMState& state, const uint32_t opcode)
{
    const uint32_t rd_index = Bit::get_bits<12, 15>(opcode);
    const uint32_t rm_value = state.reg(Bit::get_bits<0, 3>(opcode));

    const uint32_t rm_low  = Bit::get_bits<0, 7>(rm_value);
    const uint32_t rm_high = Bit::get_bits<8, 15>(rm_value);

    state.reg(rd_index) = rm_high | (Bit::sign_extend<8>(rm_low) << 8);
}

} // namespace ARM
} // namespace ForeARM

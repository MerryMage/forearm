#include "ForeARM/Interpreter/Instructions/ARM.hpp"
#include "ForeARM/Interpreter/State/ARMState.hpp"
#include "ForeARM/Utils/Bits.hpp"

namespace ForeARM
{
namespace ARM
{

void CLZ(State::ARMState& state, const uint32_t opcode)
{
    const uint32_t rm = Bit::get_bits<0, 3>(opcode);
    const uint32_t rd = Bit::get_bits<12, 15>(opcode);

    state.reg(rd) = Bit::count_leading_zeroes32(state.reg(rm));
}

void NOP(State::ARMState&, const uint32_t)
{
}

void SEL(State::ARMState& state, const uint32_t opcode)
{
    const uint32_t rm = Bit::get_bits<0, 3>(opcode);
    const uint32_t rn = Bit::get_bits<16, 19>(opcode);
    const uint32_t rd = Bit::get_bits<12, 15>(opcode);
    const uint32_t ge = state.cpsr().ge();

    const auto select_bits = [ge, rm, rn, &state](const uint32_t selection, const uint32_t mask) {
        if (ge & selection)
            return state.reg(rn) & mask;

        return state.reg(rm) & mask;
    };

    state.reg(rd) = select_bits(0b0001, 0x000000FF) |
                    select_bits(0b0010, 0x0000FF00) |
                    select_bits(0b0100, 0x00FF0000) |
                    select_bits(0b1000, 0xFF000000);
}

} // namespace ARM
} // namespace ForeARM

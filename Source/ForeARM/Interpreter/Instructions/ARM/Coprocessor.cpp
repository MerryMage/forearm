#include "ForeARM/Interpreter/Instructions/ARM.hpp"
#include "ForeARM/Interpreter/Instructions/Helper/LoadStore.hpp"
#include "ForeARM/Interpreter/State/ARMState.hpp"
#include "ForeARM/Utils/Bits.hpp"

namespace ForeARM
{
namespace ARM
{

void CDP(State::ARMState& state, const uint32_t opcode)
{
    const uint32_t coproc = Bit::get_bits<8, 11>(opcode);

    // TODO: Trigger an undefined instruction exception
    //       if a coprocessor isn't present. This is here
    //       as a reminder to do this when I implement
    //       exception vector support.

    state.coprocessor_manager().perform_operation(coproc, opcode);
}

void LDC(State::ARMState& state, const uint32_t opcode)
{
    const uint32_t coproc = Bit::get_bits<8, 11>(opcode);

    // TODO: Trigger an undefined instruction exception
    //       if a coprocessor isn't present. This is here
    //       as a reminder to do this when I implement
    //       exception vector support.
    if (!state.coprocessor_manager().can_handle_instruction(coproc, opcode))
        return;

    const uint32_t offset   = Bit::get_bits<0, 7>(opcode) << 2;
    const uint32_t rn_index = Bit::get_bits<16, 19>(opcode);
    const bool writeback    = Bit::is_set<21>(opcode);

    const uint32_t rn_value = rn_index == 15
                            ? State::get_word_aligned_program_counter(state)
                            : state.reg(rn_index);

    uint32_t offset_address, target_address;
    std::tie(offset_address, target_address) = LoadStore::get_addresses(opcode, rn_value, offset);

    do
    {
        const uint32_t value = state.memory_interface().read32(target_address, state);
        state.coprocessor_manager().write32(coproc, opcode, value);
        target_address += 4;
    }
    while (!state.coprocessor_manager().done_loading(coproc, opcode));

    if (writeback)
        state.reg(rn_index) = offset_address;
}

void MCR(State::ARMState& state, const uint32_t opcode)
{
    const uint32_t coproc   = Bit::get_bits<8, 11>(opcode);
    const uint32_t rt_index = Bit::get_bits<12, 15>(opcode);

    // TODO: Trigger an undefined instruction exception
    //       if a coprocessor isn't present. This is here
    //       as a reminder to do this when I implement
    //       exception vector support.
    if (!state.coprocessor_manager().can_handle_instruction(coproc, opcode))
        return;

    state.coprocessor_manager().write32(coproc, opcode, state.reg(rt_index));
}

void MCRR(State::ARMState& state, const uint32_t opcode)
{
    const uint32_t coproc    = Bit::get_bits<8, 11>(opcode);
    const uint32_t rt_index  = Bit::get_bits<12, 15>(opcode);
    const uint32_t rt2_index = Bit::get_bits<16, 19>(opcode);

    const uint64_t rt_value  = state.reg(rt_index);
    const uint64_t rt2_value = state.reg(rt2_index);
    const uint64_t value = rt_value | (rt2_value << 32);

    // TODO: Trigger an undefined instruction exception
    //       if a coprocessor isn't present. This is here
    //       as a reminder to do this when I implement
    //       exception vector support.
    if (!state.coprocessor_manager().can_handle_instruction(coproc, opcode))
        return;

    state.coprocessor_manager().write64(coproc, opcode, value);
}

void MRC(State::ARMState& state, const uint32_t opcode)
{
    const uint32_t coproc = Bit::get_bits<8, 11>(opcode);

    // TODO: Trigger an undefined instruction exception
    //       if a coprocessor isn't present. This is here
    //       as a reminder to do this when I implement
    //       exception vector support.
    if (!state.coprocessor_manager().can_handle_instruction(coproc, opcode))
        return;

    const uint32_t value = state.coprocessor_manager().read32(coproc, opcode);
    const uint32_t rt_index = Bit::get_bits<12, 15>(opcode);

    // Reads coprocessor register value bits 28-31 into APSR_nzcv.
    if (rt_index == 15)
    {
        state.cpsr() = (state.cpsr().value() & ~0xF0000000) | (value & 0xF0000000);
    }
    else
    {
        state.reg(rt_index) = value;
    }
}

void MRRC(State::ARMState& state, const uint32_t opcode)
{
    const uint32_t coproc = Bit::get_bits<8, 11>(opcode);

    // TODO: Trigger an undefined instruction exception
    //       if a coprocessor isn't present. This is here
    //       as a reminder to do this when I implement
    //       exception vector support.
    if (!state.coprocessor_manager().can_handle_instruction(coproc, opcode))
        return;

    const uint32_t rt_index  = Bit::get_bits<12, 15>(opcode);
    const uint32_t rt2_index = Bit::get_bits<16, 19>(opcode);
    const uint64_t value = state.coprocessor_manager().read64(coproc, opcode);

    state.reg(rt_index)  = static_cast<uint32_t>(value);
    state.reg(rt2_index) = static_cast<uint32_t>(value >> 32);
}

void STC(State::ARMState& state, const uint32_t opcode)
{
    const uint32_t coproc = Bit::get_bits<8, 11>(opcode);

    // TODO: Trigger an undefined instruction exception
    //       if a coprocessor isn't present. This is here
    //       as a reminder to do this when I implement
    //       exception vector support.
    if (!state.coprocessor_manager().can_handle_instruction(coproc, opcode))
        return;

    const uint32_t offset   = Bit::get_bits<0, 7>(opcode) << 2;
    const uint32_t rn_index = Bit::get_bits<16, 19>(opcode);
    const bool writeback    = Bit::is_set<21>(opcode);
    const uint32_t rn_value = State::get_register_value_and_offset_if_pc(state, rn_index);

    uint32_t offset_address, target_address;
    std::tie(offset_address, target_address) = LoadStore::get_addresses(opcode, rn_value, offset);

    do
    {
        const uint32_t value = state.coprocessor_manager().read32(coproc, opcode);
        state.memory_interface().store32(target_address, value, state);
        target_address += 4;
    }
    while (!state.coprocessor_manager().done_storing(coproc, opcode));

    if (writeback)
        state.reg(rn_index) = offset_address;
}

} // namespace ARM
} // namespace ForeARM

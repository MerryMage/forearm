#include "ForeARM/Interpreter/Instructions/ARM.hpp"
#include "ForeARM/Interpreter/State/ARMState.hpp"
#include "ForeARM/Utils/Bits.hpp"

namespace
{

enum class ExtraBehavior
{
    Accumulate,
    Subtract,
};

template <ExtraBehavior behavior>
void signed_multiply_and(ForeARM::State::ARMState& state, const uint32_t opcode)
{
    const uint32_t rd_index = Bit::get_bits<16, 19>(opcode);
    const uint32_t ra_index = Bit::get_bits<12, 15>(opcode);
    const uint32_t rm_index = Bit::get_bits<8, 11>(opcode);
    const uint32_t rn_index = Bit::get_bits<0, 3>(opcode);

    const uint64_t ra_value = state.reg(ra_index);
    const uint64_t rm_value = Bit::sign_extend<32, uint64_t>(state.reg(rm_index));
    const uint64_t rn_value = Bit::sign_extend<32, uint64_t>(state.reg(rn_index));
    uint64_t result = (ra_value << 32);

    if (behavior == ExtraBehavior::Accumulate)
        result += (rn_value * rm_value);
    else
        result -= (rn_value * rm_value);

    // Round
    if (Bit::is_set<5>(opcode))
        result += 0x80000000;

    state.reg(rd_index) = static_cast<uint32_t>(result >> 32);
}

} // Anonymous namespace

namespace ForeARM
{
namespace ARM
{

void SMMLA(State::ARMState& state, const uint32_t opcode)
{
    signed_multiply_and<ExtraBehavior::Accumulate>(state, opcode);
}

void SMMLS(State::ARMState& state, const uint32_t opcode)
{
    signed_multiply_and<ExtraBehavior::Subtract>(state, opcode);
}

void SMMUL(State::ARMState& state, const uint32_t opcode)
{
    const uint32_t rd_index = Bit::get_bits<16, 19>(opcode);
    const uint32_t rm_index = Bit::get_bits<8, 11>(opcode);
    const uint32_t rn_index = Bit::get_bits<0, 3>(opcode);

    const uint64_t rm_value = Bit::sign_extend<32, uint64_t>(state.reg(rm_index));
    const uint64_t rn_value = Bit::sign_extend<32, uint64_t>(state.reg(rn_index));
    uint64_t result = rn_value * rm_value;

    // Round
    if (Bit::is_set<5>(opcode))
        result += 0x80000000;

    state.reg(rd_index) = static_cast<uint32_t>(result >> 32);
}

} // namespace ARM
} // namespace ForeARM

#include "ForeARM/Interpreter/Instructions/ARM.hpp"
#include "ForeARM/Interpreter/Instructions/Helper/Branch.hpp"
#include "ForeARM/Interpreter/Instructions/Helper/Shift.hpp"
#include "ForeARM/Interpreter/State/ARMState.hpp"
#include "ForeARM/Utils/Bits.hpp"

namespace
{
using namespace ForeARM::Branch;
using namespace ForeARM::Shift;
using ForeARM::State::ARMState;

void set_cpsr_if_s_bit_set(ARMState& state, const uint32_t opcode, const ShiftWithCarryResult& result)
{
    if (Bit::is_not_set<20>(opcode))
        return;

    state.cpsr().n(result.value & 0x80000000);
    state.cpsr().z(result.value == 0);
    state.cpsr().c(result.carry_out);
}

void shift_by_immediate(ARMState& state, const uint32_t opcode, const ShiftType shift_type)
{
    const uint32_t rd_index = Bit::get_bits<12, 15>(opcode);
    const uint32_t rm_index = Bit::get_bits<0, 3>(opcode);
    const uint32_t imm5     = Bit::get_bits<7, 11>(opcode);

    const uint32_t shift_value = decode_imm_shift(static_cast<uint32_t>(shift_type), imm5).amount;
    const uint32_t rm_value = ForeARM::State::get_register_value_and_offset_if_pc(state, rm_index);

    const auto result = shift_with_carry(rm_value, shift_type, shift_value, state.cpsr().c());

    if (rd_index == 15)
    {
        bx_write_pc(state, result.value);
    }
    else
    {
        state.reg(rd_index) = result.value;
        set_cpsr_if_s_bit_set(state, opcode, result);
    }
}

void shift_by_register(ARMState& state, const uint32_t opcode, const ShiftType shift_type)
{
    const uint32_t rd_index = Bit::get_bits<12, 15>(opcode);
    const uint32_t rm_value = state.reg(Bit::get_bits<8, 11>(opcode));
    const uint32_t rn_value = state.reg(Bit::get_bits<0, 3>(opcode));

    const auto result = shift_with_carry(rn_value, shift_type, rm_value & 0xFF, state.cpsr().c());
    state.reg(rd_index) = result.value;
    set_cpsr_if_s_bit_set(state, opcode, result);
}

} // Anonymous namespace

namespace ForeARM
{
namespace ARM
{

void ASR_imm(State::ARMState& state, const uint32_t opcode)
{
    shift_by_immediate(state, opcode, Shift::ShiftType::ASR);
}

void ASR_reg(State::ARMState& state, const uint32_t opcode)
{
    shift_by_register(state, opcode, Shift::ShiftType::ASR);
}

void LSL_imm(State::ARMState& state, const uint32_t opcode)
{
    shift_by_immediate(state, opcode, Shift::ShiftType::LSL);
}

void LSL_reg(State::ARMState& state, const uint32_t opcode)
{
    shift_by_register(state, opcode, Shift::ShiftType::LSL);
}

void LSR_imm(State::ARMState& state, const uint32_t opcode)
{
    shift_by_immediate(state, opcode, Shift::ShiftType::LSR);
}

void LSR_reg(State::ARMState& state, const uint32_t opcode)
{
    shift_by_register(state, opcode, Shift::ShiftType::LSR);
}

void ROR_imm(State::ARMState& state, const uint32_t opcode)
{
    shift_by_immediate(state, opcode, Shift::ShiftType::ROR);
}

void ROR_reg(State::ARMState& state, const uint32_t opcode)
{
    shift_by_register(state, opcode, Shift::ShiftType::ROR);
}

void RRX(State::ARMState& state, const uint32_t opcode)
{
    const uint32_t rd_index = Bit::get_bits<12, 15>(opcode);
    const uint32_t rm_index = Bit::get_bits<0, 3>(opcode);
    const uint32_t rm_value = State::get_register_value_and_offset_if_pc(state, rm_index);

    const auto result = Shift::shift_with_carry(rm_value, Shift::ShiftType::RRX, 1, state.cpsr().c());

    if (rd_index == 15)
    {
        Branch::bx_write_pc(state, result.value);
    }
    else
    {
        state.reg(rd_index) = result.value;
        set_cpsr_if_s_bit_set(state, opcode, result);
    }
}

} // namespace ARM
} // namespace ForeARM

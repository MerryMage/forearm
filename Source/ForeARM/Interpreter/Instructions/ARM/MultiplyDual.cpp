#include "ForeARM/Interpreter/Instructions/ARM.hpp"
#include "ForeARM/Interpreter/State/ARMState.hpp"
#include "ForeARM/Utils/Bits.hpp"

namespace ForeARM
{
namespace ARM
{

static uint64_t get_operand(const uint64_t value, const bool swap) noexcept
{
    return swap ? (value >> 16) | (value & 0xFFFF) << 16
                : value;
}

void SMLAD(State::ARMState& state, const uint32_t opcode)
{
    const uint32_t rd_index = Bit::get_bits<16, 19>(opcode);
    const uint64_t ra_value = state.reg(Bit::get_bits<12, 15>(opcode));
    const uint64_t rm_value = state.reg(Bit::get_bits<8, 11>(opcode));
    const uint64_t rn_value = state.reg(Bit::get_bits<0, 3>(opcode));
    const bool swap = Bit::is_set<5>(opcode);

    const uint64_t operand = get_operand(rm_value, swap);

    const uint64_t product1 = Bit::sign_extend<16>(rn_value) * Bit::sign_extend<16>(operand);
    const uint64_t product2 = Bit::sign_extend<16>(rn_value >> 16) * Bit::sign_extend<16>(operand >> 16);
    const uint64_t result   = product1 + product2 + Bit::sign_extend<32>(ra_value);

    state.reg(rd_index) = static_cast<uint32_t>(result);

    if (result != Bit::sign_extend<32>(result))
        state.cpsr().q(1);
}

void SMLALD(State::ARMState& state, const uint32_t opcode)
{
    const uint32_t rdlo_index = Bit::get_bits<12, 15>(opcode);
    const uint32_t rdhi_index = Bit::get_bits<16, 19>(opcode);
    const uint64_t rm_value = state.reg(Bit::get_bits<0, 3>(opcode));
    const uint64_t rn_value = state.reg(Bit::get_bits<8, 11>(opcode));
    const bool swap = Bit::is_set<5>(opcode);

    const uint64_t operand = get_operand(rm_value, swap);

    const uint64_t product1 = Bit::sign_extend<16>(rn_value) * Bit::sign_extend<16>(operand);
    const uint64_t product2 = Bit::sign_extend<16>(rn_value >> 16) * Bit::sign_extend<16>(operand >> 16);
    const uint64_t accumulate = static_cast<uint64_t>(state.reg(rdhi_index)) << 32 | state.reg(rdlo_index);
    const uint64_t result = product1 + product2 + accumulate;

    state.reg(rdhi_index) = static_cast<uint32_t>(result >> 32);
    state.reg(rdlo_index) = static_cast<uint32_t>(result);
}

void SMLSD(State::ARMState& state, const uint32_t opcode)
{
    const uint32_t rd_index = Bit::get_bits<16, 19>(opcode);
    const uint64_t ra_value = state.reg(Bit::get_bits<12, 15>(opcode));
    const uint64_t rm_value = state.reg(Bit::get_bits<8, 11>(opcode));
    const uint64_t rn_value = state.reg(Bit::get_bits<0, 3>(opcode));
    const bool swap = Bit::is_set<5>(opcode);

    const uint64_t operand = get_operand(rm_value, swap);

    const uint64_t product1 = Bit::sign_extend<16>(rn_value) * Bit::sign_extend<16>(operand);
    const uint64_t product2 = Bit::sign_extend<16>(rn_value >> 16) * Bit::sign_extend<16>(operand >> 16);
    const uint64_t result   = (product1 - product2) + Bit::sign_extend<32>(ra_value);

    state.reg(rd_index) = static_cast<uint32_t>(result);

    if (result != Bit::sign_extend<32>(result))
        state.cpsr().q(1);
}

void SMLSLD(State::ARMState& state, const uint32_t opcode)
{
    const uint32_t rdlo_index = Bit::get_bits<12, 15>(opcode);
    const uint32_t rdhi_index = Bit::get_bits<16, 19>(opcode);
    const uint64_t rm_value = state.reg(Bit::get_bits<8, 11>(opcode));
    const uint64_t rn_value = state.reg(Bit::get_bits<0, 3>(opcode));
    const bool swap = Bit::is_set<5>(opcode);

    const uint64_t operand = get_operand(rm_value, swap);

    const uint64_t product1 = Bit::sign_extend<16>(rn_value) * Bit::sign_extend<16>(operand);
    const uint64_t product2 = Bit::sign_extend<16>(rn_value >> 16) * Bit::sign_extend<16>(operand >> 16);
    const uint64_t accumulate = static_cast<uint64_t>(state.reg(rdhi_index)) << 32 | state.reg(rdlo_index);
    const uint64_t result = (product1 - product2) + accumulate;

    state.reg(rdhi_index) = static_cast<uint32_t>(result >> 32);
    state.reg(rdlo_index) = static_cast<uint32_t>(result);
}

void SMUAD(State::ARMState& state, const uint32_t opcode)
{
    const uint32_t rd_index = Bit::get_bits<16, 19>(opcode);
    const uint64_t rm_value = state.reg(Bit::get_bits<8, 11>(opcode));
    const uint64_t rn_value = state.reg(Bit::get_bits<0, 3>(opcode));
    const bool swap = Bit::is_set<5>(opcode);

    const uint64_t operand = get_operand(rm_value, swap);

    const uint64_t product1 = Bit::sign_extend<16>(rn_value) * Bit::sign_extend<16>(operand);
    const uint64_t product2 = Bit::sign_extend<16>(rn_value >> 16) * Bit::sign_extend<16>(operand >> 16);
    const uint64_t result   = product1 + product2;

    state.reg(rd_index) = static_cast<uint32_t>(result);

    if (result != Bit::sign_extend<32>(result))
        state.cpsr().q(1);
}

void SMUSD(State::ARMState& state, const uint32_t opcode)
{
    const uint32_t rd_index = Bit::get_bits<16, 19>(opcode);
    const uint64_t rm_value = state.reg(Bit::get_bits<8, 11>(opcode));
    const uint64_t rn_value = state.reg(Bit::get_bits<0, 3>(opcode));
    const bool swap = Bit::is_set<5>(opcode);

    const uint64_t operand = get_operand(rm_value, swap);

    const uint64_t product1 = Bit::sign_extend<16>(rn_value) * Bit::sign_extend<16>(operand);
    const uint64_t product2 = Bit::sign_extend<16>(rn_value >> 16) * Bit::sign_extend<16>(operand >> 16);
    const uint64_t result   = product1 - product2;

    state.reg(rd_index) = static_cast<uint32_t>(result);
}

} // namespace ARM
} // namespace ForeARM

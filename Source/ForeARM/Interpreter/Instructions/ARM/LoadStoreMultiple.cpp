#include <bitset>
#include "ForeARM/Interpreter/Instructions/ARM.hpp"
#include "ForeARM/Interpreter/Instructions/Helper/Branch.hpp"
#include "ForeARM/Interpreter/State/ARMState.hpp"
#include "ForeARM/Utils/Bits.hpp"

namespace
{
using ForeARM::State::ARMState;

// The first 16 bits of an ARM-mode LDM or STM instruction indicate a list of bits
// that signify which registers to load data into.
uint32_t num_register_list_bits_set(const uint32_t value) noexcept
{
    return static_cast<uint32_t>(std::bitset<16>(value).count());
}

// Whether or not an opcode signifies write-back should be performed.
bool do_writeback(const uint32_t value) noexcept
{
    return Bit::is_set<21>(value);
}

void read_memory_to_registers(ARMState& state, const uint32_t opcode, uint32_t start_address)
{
    for (size_t i = 0; i <= 14; i++)
    {
        if (Bit::is_not_set(opcode, i))
            continue;
        
        state.reg(i) = state.memory_interface().read32(start_address, state);
        start_address += 4;
    }

    if (Bit::is_set<15>(opcode))
        ForeARM::Branch::bx_write_pc(state, state.memory_interface().read32(start_address, state));
}

void store_registers_to_memory(ARMState& state, const uint32_t opcode, uint32_t start_address)
{
    for (size_t i = 0; i <= 14; i++)
    {
        if (Bit::is_not_set(opcode, i))
            continue;

        state.memory_interface().store32(start_address, state.reg(i), state);
        start_address += 4;
    }
    
    if (Bit::is_set<15>(opcode))
        state.memory_interface().store32(start_address, ForeARM::State::get_program_counter(state), state);
}

} // Anonymous namespace

namespace ForeARM
{
namespace ARM
{

// AKA LDMFD
void LDMIA(State::ARMState& state, const uint32_t opcode)
{
    const uint32_t rn_index = Bit::get_bits<16, 19>(opcode);
    const uint32_t address  = state.reg(rn_index);

    read_memory_to_registers(state, opcode, address);

    if (do_writeback(opcode) && Bit::is_not_set(opcode, rn_index))
    {
        const uint32_t count = num_register_list_bits_set(opcode);
        state.reg(rn_index) += 4 * count;
    }
}

// AKA LDMFA
void LDMDA(State::ARMState& state, const uint32_t opcode)
{
    const uint32_t count    = num_register_list_bits_set(opcode);
    const uint32_t rn_index = Bit::get_bits<16, 19>(opcode);
    const uint32_t address  = state.reg(rn_index) - (4 * count) + 4;

    read_memory_to_registers(state, opcode, address);

    if (do_writeback(opcode) && Bit::is_not_set(opcode, rn_index))
        state.reg(rn_index) -= 4 * count;
}

// AKA LDMEA
void LDMDB(State::ARMState& state, const uint32_t opcode)
{
    const uint32_t count    = num_register_list_bits_set(opcode);
    const uint32_t rn_index = Bit::get_bits<16, 19>(opcode);
    const uint32_t address  = state.reg(rn_index) - (4 * count);

    read_memory_to_registers(state, opcode, address);

    if (do_writeback(opcode) && Bit::is_not_set(opcode, rn_index))
        state.reg(rn_index) -= 4 * count;
}

// AKA LDMED
void LDMIB(State::ARMState& state, const uint32_t opcode)
{
    const uint32_t rn_index = Bit::get_bits<16, 19>(opcode);
    const uint32_t address  = state.reg(rn_index) + 4;

    read_memory_to_registers(state, opcode, address);

    if (do_writeback(opcode) && Bit::is_not_set(opcode, rn_index))
    {
        const uint32_t count = num_register_list_bits_set(opcode);
        state.reg(rn_index) += 4 * count;
    }
}

// AKA STMEA
void STMIA(State::ARMState& state, const uint32_t opcode)
{
    const uint32_t rn_index = Bit::get_bits<16, 19>(opcode);
    const uint32_t address  = state.reg(rn_index);

    store_registers_to_memory(state, opcode, address);

    if (do_writeback(opcode))
    {
        const uint32_t count = num_register_list_bits_set(opcode);
        state.reg(rn_index) += 4 * count;
    }
}

// AKA STMED
void STMDA(State::ARMState& state, const uint32_t opcode)
{
    const uint32_t count    = num_register_list_bits_set(opcode);
    const uint32_t rn_index = Bit::get_bits<16, 19>(opcode);
    const uint32_t address  = state.reg(rn_index) - (4 * count) + 4;

    store_registers_to_memory(state, opcode, address);

    if (do_writeback(opcode))
        state.reg(rn_index) -= 4 * count;
}

// AKA STMFD
void STMDB(State::ARMState& state, const uint32_t opcode)
{
    const uint32_t count    = num_register_list_bits_set(opcode);
    const uint32_t rn_index = Bit::get_bits<16, 19>(opcode);
    const uint32_t address  = state.reg(rn_index) - (4 * count);

    store_registers_to_memory(state, opcode, address);

    if (do_writeback(opcode))
        state.reg(rn_index) -= 4 * count;
}

// AKA STMFA
void STMIB(State::ARMState& state, const uint32_t opcode)
{
    const uint32_t rn_index = Bit::get_bits<16, 19>(opcode);
    const uint32_t address  = state.reg(rn_index) + 4;

    store_registers_to_memory(state, opcode, address);

    if (do_writeback(opcode))
    {
        const uint32_t count = num_register_list_bits_set(opcode);
        state.reg(rn_index) += 4 * count;
    }
}

void LDM_stub(State::ARMState&, const uint32_t)
{
}

void STM_stub(State::ARMState&, const uint32_t)
{
}

} // namespace ARM
} // namespace ForeARM

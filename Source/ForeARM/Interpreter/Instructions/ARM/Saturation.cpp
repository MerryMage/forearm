#include "ForeARM/Interpreter/Instructions/ARM.hpp"
#include "ForeARM/Interpreter/Instructions/Helper/Shift.hpp"
#include "ForeARM/Interpreter/Instructions/Helper/Saturation.hpp"
#include "ForeARM/Interpreter/State/ARMState.hpp"
#include "ForeARM/Utils/Bits.hpp"

namespace ForeARM
{
namespace ARM
{

void SSAT(State::ARMState& state, const uint32_t opcode)
{
    const uint32_t rd_index   = Bit::get_bits<12, 15>(opcode);
    const uint32_t rn_value   = state.reg(opcode & 0xF);
    const uint32_t sat_imm    = Bit::get_bits<16, 20>(opcode) + 1;
    const uint32_t imm5       = Bit::get_bits<7, 11>(opcode);
    const uint32_t shift_type = Bit::get_bit<6>(opcode);

    const auto shift = Shift::decode_imm_shift(shift_type << 1, imm5);
    const uint64_t operand = Shift::shift_without_carry(rn_value, shift.type, shift.amount, state.cpsr().c());

    const auto result = Saturation::signed_saturation_q(Bit::sign_extend<32>(operand), sat_imm);

    state.reg(rd_index) = result.value;

    if (result.saturation_occurred)
        state.cpsr().q(1);
}

void SSAT16(State::ARMState& state, const uint32_t opcode)
{
    const uint32_t rd_index = Bit::get_bits<12, 15>(opcode);
    const uint32_t rn_value = state.reg(opcode & 0xF);
    const uint32_t sat_imm  = Bit::get_bits<16, 19>(opcode) + 1;

    const auto result1 = Saturation::signed_saturation_q(Bit::sign_extend<16, uint64_t>(rn_value & 0xFFFF), sat_imm);
    const auto result2 = Saturation::signed_saturation_q(Bit::sign_extend<16, uint64_t>(rn_value >> 16), sat_imm);

    state.reg(rd_index) = (result1.value & 0xFFFF) | (result2.value & 0xFFFF) << 16;

    if (result1.saturation_occurred || result2.saturation_occurred)
        state.cpsr().q(1);
}

void USAT(State::ARMState& state, const uint32_t opcode)
{
    const uint32_t rd_index   = Bit::get_bits<12, 15>(opcode);
    const uint32_t rn_value   = state.reg(opcode & 0xF);
    const uint32_t sat_imm    = Bit::get_bits<16, 20>(opcode);
    const uint32_t imm5       = Bit::get_bits<7, 11>(opcode);
    const uint32_t shift_type = Bit::get_bit<6>(opcode);

    const auto shift = Shift::decode_imm_shift(shift_type << 1, imm5);
    const uint64_t operand = Shift::shift_without_carry(rn_value, shift.type, shift.amount, state.cpsr().c());

    const auto result = Saturation::unsigned_saturation_q(Bit::sign_extend<32>(operand), sat_imm);

    state.reg(rd_index) = result.value;

    if (result.saturation_occurred)
        state.cpsr().q(1);
}

void USAT16(State::ARMState& state, const uint32_t opcode)
{
    const uint32_t rd_index = Bit::get_bits<12, 15>(opcode);
    const uint32_t rn_value = state.reg(opcode & 0xF);
    const uint32_t sat_imm  = Bit::get_bits<16, 19>(opcode);

    const auto result1 = Saturation::unsigned_saturation_q(Bit::sign_extend<16, uint64_t>(rn_value & 0xFFFF), sat_imm);
    const auto result2 = Saturation::unsigned_saturation_q(Bit::sign_extend<16, uint64_t>(rn_value >> 16), sat_imm);

    state.reg(rd_index) = (result1.value & 0xFFFF) | (result2.value & 0xFFFF) << 16;

    if (result1.saturation_occurred || result2.saturation_occurred)
        state.cpsr().q(1);
}

} // namespace ARM
} // namespace ForeARM

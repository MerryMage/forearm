#include "ForeARM/Interpreter/Instructions/ARM.hpp"
#include "ForeARM/Interpreter/State/ARMState.hpp"
#include "ForeARM/Utils/Bits.hpp"

namespace ForeARM
{
namespace ARM
{

void SADD8(State::ARMState& state, const uint32_t opcode)
{
    const uint32_t rd_index = Bit::get_bits<12, 15>(opcode);
    const uint32_t rm_value = state.reg(Bit::get_bits<0, 3>(opcode));
    const uint32_t rn_value = state.reg(Bit::get_bits<16, 19>(opcode));

    const uint32_t sum1 = Bit::sign_extend<8>(rn_value) + Bit::sign_extend<8>(rm_value);
    const uint32_t sum2 = Bit::sign_extend<8>(rn_value >>  8) + Bit::sign_extend<8>(rm_value >>  8);
    const uint32_t sum3 = Bit::sign_extend<8>(rn_value >> 16) + Bit::sign_extend<8>(rm_value >> 16);
    const uint32_t sum4 = Bit::sign_extend<8>(rn_value >> 24) + Bit::sign_extend<8>(rm_value >> 24);

    state.reg(rd_index) = (sum1 & 0xFF) | ((sum2 & 0xFF) << 8) | ((sum3 & 0xFF) << 16) | ((sum4 & 0xFF) << 24);

    uint32_t ge = 0;
    if (Bit::is_not_set<8>(sum1)) ge |= 0b0001;
    if (Bit::is_not_set<8>(sum2)) ge |= 0b0010;
    if (Bit::is_not_set<8>(sum3)) ge |= 0b0100;
    if (Bit::is_not_set<8>(sum4)) ge |= 0b1000;

    state.cpsr().ge(ge);
}

void SADD16(State::ARMState& state, const uint32_t opcode)
{
    const uint32_t rd_index = Bit::get_bits<12, 15>(opcode);
    const uint32_t rm_value = state.reg(Bit::get_bits<0, 3>(opcode));
    const uint32_t rn_value = state.reg(Bit::get_bits<16, 19>(opcode));

    const uint32_t sum1 = Bit::sign_extend<16>(rn_value) + Bit::sign_extend<16>(rm_value);
    const uint32_t sum2 = Bit::sign_extend<16>(rn_value >> 16) + Bit::sign_extend<16>(rm_value >> 16);

    state.reg(rd_index) = (sum1 & 0xFFFF) | ((sum2 & 0xFFFF) << 16);

    uint32_t ge = 0;
    if (Bit::is_not_set<16>(sum1)) ge |= 0b0011;
    if (Bit::is_not_set<16>(sum2)) ge |= 0b1100;

    state.cpsr().ge(ge);
}

void SASX(State::ARMState& state, const uint32_t opcode)
{
    const uint32_t rd_index = Bit::get_bits<12, 15>(opcode);
    const uint32_t rm_value = state.reg(Bit::get_bits<0, 3>(opcode));
    const uint32_t rn_value = state.reg(Bit::get_bits<16, 19>(opcode));

    const uint32_t diff = Bit::sign_extend<16>(rn_value) - Bit::sign_extend<16>(rm_value >> 16);
    const uint32_t sum  = Bit::sign_extend<16>(rn_value >> 16) + Bit::sign_extend<16>(rm_value);

    state.reg(rd_index) = (diff & 0xFFFF) | ((sum & 0xFFFF) << 16);

    uint32_t ge = 0;
    if (Bit::is_not_set<16>(diff))
        ge |= 0b0011;
    if (Bit::is_not_set<16>(sum))
        ge |= 0b1100;

    state.cpsr().ge(ge);
}

void SSAX(State::ARMState& state, const uint32_t opcode)
{
    const uint32_t rd_index = Bit::get_bits<12, 15>(opcode);
    const uint32_t rm_value = state.reg(Bit::get_bits<0, 3>(opcode));
    const uint32_t rn_value = state.reg(Bit::get_bits<16, 19>(opcode));

    const uint32_t sum  = Bit::sign_extend<16>(rn_value) + Bit::sign_extend<16>(rm_value >> 16);
    const uint32_t diff = Bit::sign_extend<16>(rn_value >> 16) - Bit::sign_extend<16>(rm_value);

    state.reg(rd_index) = (sum & 0xFFFF) | ((diff & 0xFFFF) << 16);

    uint32_t ge = 0;
    if (Bit::is_not_set<16>(sum))
        ge |= 0b0011;
    if (Bit::is_not_set<16>(diff))
        ge |= 0b1100;

    state.cpsr().ge(ge);
}

void SSUB8(State::ARMState& state, const uint32_t opcode)
{
    const uint32_t rd_index = Bit::get_bits<12, 15>(opcode);
    const uint32_t rm_value = state.reg(Bit::get_bits<0, 3>(opcode));
    const uint32_t rn_value = state.reg(Bit::get_bits<16, 19>(opcode));

    const uint32_t diff1 = Bit::sign_extend<8>(rn_value) - Bit::sign_extend<8>(rm_value);
    const uint32_t diff2 = Bit::sign_extend<8>(rn_value >>  8) - Bit::sign_extend<8>(rm_value >>  8);
    const uint32_t diff3 = Bit::sign_extend<8>(rn_value >> 16) - Bit::sign_extend<8>(rm_value >> 16);
    const uint32_t diff4 = Bit::sign_extend<8>(rn_value >> 24) - Bit::sign_extend<8>(rm_value >> 24);

    state.reg(rd_index) = (diff1 & 0xFF) | ((diff2 & 0xFF) << 8) | ((diff3 & 0xFF) << 16) | ((diff4 & 0xFF) << 24);

    uint32_t ge = 0;
    if (Bit::is_not_set<8>(diff1)) ge |= 0b0001;
    if (Bit::is_not_set<8>(diff2)) ge |= 0b0010;
    if (Bit::is_not_set<8>(diff3)) ge |= 0b0100;
    if (Bit::is_not_set<8>(diff4)) ge |= 0b1000;

    state.cpsr().ge(ge);
}

void SSUB16(State::ARMState& state, const uint32_t opcode)
{
    const uint32_t rd_index = Bit::get_bits<12, 15>(opcode);
    const uint32_t rm_value = state.reg(Bit::get_bits<0, 3>(opcode));
    const uint32_t rn_value = state.reg(Bit::get_bits<16, 19>(opcode));

    const uint32_t diff1 = Bit::sign_extend<16>(rn_value) - Bit::sign_extend<16>(rm_value);
    const uint32_t diff2 = Bit::sign_extend<16>(rn_value >> 16) - Bit::sign_extend<16>(rm_value >> 16);

    state.reg(rd_index) = (diff1 & 0xFFFF) | ((diff2 & 0xFFFF) << 16);

    uint32_t ge = 0;
    if (Bit::is_not_set<16>(diff1)) ge |= 0b0011;
    if (Bit::is_not_set<16>(diff2)) ge |= 0b1100;

    state.cpsr().ge(ge);
}

void UADD8(State::ARMState& state, const uint32_t opcode)
{
    const uint32_t rd_index = Bit::get_bits<12, 15>(opcode);
    const uint32_t rm_value = state.reg(Bit::get_bits<0, 3>(opcode));
    const uint32_t rn_value = state.reg(Bit::get_bits<16, 19>(opcode));

    const uint32_t sum1 = (rn_value & 0xFF) + (rm_value & 0xFF);
    const uint32_t sum2 = ((rn_value >>  8) & 0xFF) + ((rm_value >>  8) & 0xFF);
    const uint32_t sum3 = ((rn_value >> 16) & 0xFF) + ((rm_value >> 16) & 0xFF);
    const uint32_t sum4 = ((rn_value >> 24) & 0xFF) + ((rm_value >> 24) & 0xFF);

    state.reg(rd_index) = (sum1 & 0xFF) | ((sum2 & 0xFF) << 8) | ((sum3 & 0xFF) << 16) | ((sum4 & 0xFF) << 24);

    uint32_t ge = 0;
    if (sum1 >= 0x100) ge |= 0b0001;
    if (sum2 >= 0x100) ge |= 0b0010;
    if (sum3 >= 0x100) ge |= 0b0100;
    if (sum4 >= 0x100) ge |= 0b1000;

    state.cpsr().ge(ge);
}

void UADD16(State::ARMState& state, const uint32_t opcode)
{
    const uint32_t rd_index = Bit::get_bits<12, 15>(opcode);
    const uint32_t rm_value = state.reg(Bit::get_bits<0, 3>(opcode));
    const uint32_t rn_value = state.reg(Bit::get_bits<16, 19>(opcode));

    const uint32_t sum1 = (rn_value & 0xFFFF) + (rm_value & 0xFFFF);
    const uint32_t sum2 = (rn_value >> 16)    + (rm_value >> 16);

    state.reg(rd_index) = (sum1 & 0xFFFF) | ((sum2 & 0xFFFF) << 16);

    uint32_t ge = 0;
    if (sum1 >= 0x10000) ge |= 0b0011;
    if (sum2 >= 0x10000) ge |= 0b1100;

    state.cpsr().ge(ge);
}

void UASX(State::ARMState& state, const uint32_t opcode)
{
    const uint32_t rd_index = Bit::get_bits<12, 15>(opcode);
    const uint32_t rm_value = state.reg(Bit::get_bits<0, 3>(opcode));
    const uint32_t rn_value = state.reg(Bit::get_bits<16, 19>(opcode));

    const uint32_t difference = (rn_value & 0xFFFF) - (rm_value >> 16);
    const uint32_t sum        = (rn_value >> 16)    + (rm_value & 0xFFFF);

    state.reg(rd_index) = (difference & 0xFFFF) | ((sum & 0xFFFF) << 16);

    uint32_t ge = 0;
    // Check for negativity
    if (Bit::is_not_set<16>(difference))
        ge |= 0b0011;
    if (sum >= 0x10000)
        ge |= 0b1100;

    state.cpsr().ge(ge);
}

void USAX(State::ARMState& state, const uint32_t opcode)
{
    const uint32_t rd_index = Bit::get_bits<12, 15>(opcode);
    const uint32_t rm_value = state.reg(Bit::get_bits<0, 3>(opcode));
    const uint32_t rn_value = state.reg(Bit::get_bits<16, 19>(opcode));

    const uint32_t sum        = (rn_value & 0xFFFF) + (rm_value >> 16);
    const uint32_t difference = (rn_value >> 16)    - (rm_value & 0xFFFF);

    state.reg(rd_index) = (sum & 0xFFFF) | ((difference & 0xFFFF) << 16);

    uint32_t ge = 0;
    // Check for negativity
    if (sum >= 0x10000)
        ge |= 0b0011;
    if (Bit::is_not_set<16>(difference))
        ge |= 0b1100;

    state.cpsr().ge(ge);
}

void USUB8(State::ARMState& state, const uint32_t opcode)
{
    const uint32_t rd_index = Bit::get_bits<12, 15>(opcode);
    const uint32_t rm_value = state.reg(Bit::get_bits<0, 3>(opcode));
    const uint32_t rn_value = state.reg(Bit::get_bits<16, 19>(opcode));

    const uint32_t diff1 = (rn_value & 0xFF) - (rm_value & 0xFF);
    const uint32_t diff2 = ((rn_value >>  8) & 0xFF) - ((rm_value >>  8) & 0xFF);
    const uint32_t diff3 = ((rn_value >> 16) & 0xFF) - ((rm_value >> 16) & 0xFF);
    const uint32_t diff4 = ((rn_value >> 24) & 0xFF) - ((rm_value >> 24) & 0xFF);

    state.reg(rd_index) = (diff1 & 0xFF) | ((diff2 & 0xFF) << 8) | ((diff3 & 0xFF) << 16) | ((diff4 & 0xFF) << 24);

    uint32_t ge = 0;
    if (Bit::is_not_set<8>(diff1)) ge |= 0b0001;
    if (Bit::is_not_set<8>(diff2)) ge |= 0b0010;
    if (Bit::is_not_set<8>(diff3)) ge |= 0b0100;
    if (Bit::is_not_set<8>(diff4)) ge |= 0b1000;

    state.cpsr().ge(ge);
}

void USUB16(State::ARMState& state, const uint32_t opcode)
{
    const uint32_t rd_index = Bit::get_bits<12, 15>(opcode);
    const uint32_t rm_value = state.reg(Bit::get_bits<0, 3>(opcode));
    const uint32_t rn_value = state.reg(Bit::get_bits<16, 19>(opcode));

    const uint32_t diff1 = (rn_value & 0xFFFF) - (rm_value & 0xFFFF);
    const uint32_t diff2 = (rn_value >> 16)    - (rm_value >> 16);

    state.reg(rd_index) = (diff1 & 0xFFFF) | ((diff2 & 0xFFFF) << 16);

    uint32_t ge = 0;
    if (Bit::is_not_set<16>(diff1)) ge |= 0b0011;
    if (Bit::is_not_set<16>(diff2)) ge |= 0b1100;

    state.cpsr().ge(ge);
}

} // namespace ARM
} // namespace ForeARM

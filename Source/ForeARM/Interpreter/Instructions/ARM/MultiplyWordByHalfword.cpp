#include "ForeARM/Interpreter/Instructions/ARM.hpp"
#include "ForeARM/Interpreter/State/ARMState.hpp"
#include "ForeARM/Utils/Bits.hpp"

namespace ForeARM
{
namespace ARM
{

void SMLAWY(State::ARMState& state, const uint32_t opcode)
{
    const bool high = Bit::is_set<6>(opcode);
    const uint32_t rd_index = Bit::get_bits<16, 19>(opcode);
    const uint64_t ra_value = state.reg(Bit::get_bits<12, 15>(opcode));
    const uint64_t rm_value = state.reg(Bit::get_bits<8, 11>(opcode));
    const uint64_t rn_value = state.reg(Bit::get_bits<0, 3>(opcode));

    const uint64_t operand = high ? (rm_value >> 16)
                                  : (rm_value & 0xFFFF);

    const uint64_t product = Bit::sign_extend<32>(rn_value) * Bit::sign_extend<16>(operand);
    const uint64_t accumulate = Bit::sign_extend<32>(ra_value) << 16;
    const uint64_t result = (product + accumulate) >> 16;

    state.reg(rd_index) = static_cast<uint32_t>(result);

    // Signed overflow
    if (result != (Bit::sign_extend<32>(result) & 0xFFFFFFFFFFFF))
        state.cpsr().q(1);
}

void SMULWY(State::ARMState& state, const uint32_t opcode)
{
    const bool high = Bit::is_set<6>(opcode);
    const uint32_t rd_index = Bit::get_bits<16, 19>(opcode);
    const uint32_t rn_index = Bit::get_bits<0, 3>(opcode);
    const uint32_t rm_value = state.reg(Bit::get_bits<8, 11>(opcode));

    const uint64_t operand  = high ? (rm_value >> 16) : (rm_value & 0xFFFF);
    const uint64_t rn_value = state.reg(rn_index);
    const uint64_t result   = Bit::sign_extend<16>(operand) * Bit::sign_extend<32>(rn_value);

    state.reg(rd_index) = static_cast<uint32_t>(result >> 16);
}

} // namespace ARM
} // namespace ForeARM

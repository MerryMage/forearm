#include "ForeARM/Interpreter/Instructions/ARM.hpp"
#include "ForeARM/Interpreter/Instructions/Helper/Saturation.hpp"
#include "ForeARM/Interpreter/State/ARMState.hpp"
#include "ForeARM/Utils/Bits.hpp"

namespace ForeARM
{
namespace ARM
{

void QADD(State::ARMState& state, const uint32_t opcode)
{
    const uint32_t rd_index = Bit::get_bits<12, 15>(opcode);
    const uint64_t rn_value = state.reg(Bit::get_bits<16, 19>(opcode));
    const uint64_t rm_value = state.reg(Bit::get_bits<0, 3>(opcode));

    const uint64_t sum = Bit::sign_extend<32>(rn_value) + Bit::sign_extend<32>(rm_value);
    const auto result  = Saturation::signed_saturation_q(sum, 32);

    state.reg(rd_index) = result.value;

    if (result.saturation_occurred)
        state.cpsr().q(1);
}

void QSUB(State::ARMState& state, const uint32_t opcode)
{
    const uint32_t rd_index = Bit::get_bits<12, 15>(opcode);
    const uint64_t rn_value = state.reg(Bit::get_bits<16, 19>(opcode));
    const uint64_t rm_value = state.reg(Bit::get_bits<0, 3>(opcode));

    const uint64_t diff = Bit::sign_extend<32>(rm_value) - Bit::sign_extend<32>(rn_value);
    const auto result  = Saturation::signed_saturation_q(diff, 32);

    state.reg(rd_index) = result.value;

    if (result.saturation_occurred)
        state.cpsr().q(1);
}

void QDADD(State::ARMState& state, const uint32_t opcode)
{
    const uint32_t rd_index = Bit::get_bits<12, 15>(opcode);
    const uint64_t rn_value = state.reg(Bit::get_bits<16, 19>(opcode));
    const uint64_t rm_value = state.reg(Bit::get_bits<0, 3>(opcode));

    const auto doubled = Saturation::signed_saturation_q(2ULL * Bit::sign_extend<32>(rn_value), 32);
    const uint64_t sum = Bit::sign_extend<32>(rm_value) + Bit::sign_extend<32, uint64_t>(doubled.value);
    const auto result  = Saturation::signed_saturation_q(sum, 32);

    state.reg(rd_index) = result.value;

    if (doubled.saturation_occurred || result.saturation_occurred)
        state.cpsr().q(1);
}

void QDSUB(State::ARMState& state, const uint32_t opcode)
{
    const uint32_t rd_index = Bit::get_bits<12, 15>(opcode);
    const uint64_t rn_value = state.reg(Bit::get_bits<16, 19>(opcode));
    const uint64_t rm_value = state.reg(Bit::get_bits<0, 3>(opcode));

    const auto doubled = Saturation::signed_saturation_q(2ULL * Bit::sign_extend<32>(rn_value), 32);
    const uint64_t diff = Bit::sign_extend<32>(rm_value) - Bit::sign_extend<32, uint64_t>(doubled.value);
    const auto result  = Saturation::signed_saturation_q(diff, 32);

    state.reg(rd_index) = result.value;

    if (doubled.saturation_occurred || result.saturation_occurred)
        state.cpsr().q(1);
}

} // namespace ARM
} // namespace ForeARM

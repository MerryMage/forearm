#include "ForeARM/Interpreter/Instructions/ARM.hpp"
#include "ForeARM/Interpreter/State/ARMState.hpp"
#include "ForeARM/Utils/Bits.hpp"

namespace ForeARM
{
namespace ARM
{

void BFC(State::ARMState& state, const uint32_t opcode)
{
    const uint32_t rd    = Bit::get_bits<12, 15>(opcode);
    const uint32_t msbit = Bit::get_bits<16, 20>(opcode);
    const uint32_t lsbit = Bit::get_bits<7, 11>(opcode);
    const uint32_t mask  = Bit::create_bitmask<uint32_t>(lsbit, msbit);

    state.reg(rd) &= ~mask;
}

void BFI(State::ARMState& state, const uint32_t opcode)
{
    const uint32_t rd    = Bit::get_bits<12, 15>(opcode);
    const uint32_t rn    = Bit::get_bits<0, 3>(opcode);
    const uint32_t lsbit = Bit::get_bits<7, 11>(opcode);
    const uint32_t msbit = Bit::get_bits<16,20>(opcode);
    const uint32_t mask  = Bit::create_bitmask<uint32_t>(lsbit, msbit);

    state.reg(rd) = (state.reg(rd) & ~mask) | (state.reg(rn) & mask);
}

void SBFX(State::ARMState& state, const uint32_t opcode)
{
    const uint32_t rd           = Bit::get_bits<12, 15>(opcode);
    const uint32_t rn           = Bit::get_bits<0, 3>(opcode);
    const uint32_t widthminus1  = Bit::get_bits<16, 20>(opcode);
    const uint32_t lsbit        = Bit::get_bits<7, 11>(opcode);
    const uint32_t msbit        = lsbit + widthminus1;
    const uint32_t current_size = widthminus1 + 1;
    const uint32_t bits         = Bit::get_bits(state.reg(rn), lsbit, msbit);

    state.reg(rd) = Bit::sign_extend(bits, current_size);
}

void UBFX(State::ARMState& state, const uint32_t opcode)
{
    const uint32_t rd          = Bit::get_bits<12, 15>(opcode);
    const uint32_t rn          = Bit::get_bits<0, 3>(opcode);
    const uint32_t widthminus1 = Bit::get_bits<16, 20>(opcode);
    const uint32_t lsbit       = Bit::get_bits<7, 11>(opcode);
    const uint32_t msbit       = lsbit + widthminus1;
    const uint32_t bits        = Bit::get_bits(state.reg(rn), lsbit, msbit);

    // Unsigned values in C/C++ are already zero-extended
    state.reg(rd) = bits;
}

} // namespace ARM
} // namespace ForeARM

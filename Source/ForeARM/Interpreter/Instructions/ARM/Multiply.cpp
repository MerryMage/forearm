#include "ForeARM/Interpreter/Instructions/ARM.hpp"
#include "ForeARM/Interpreter/State/ARMState.hpp"
#include "ForeARM/Utils/Bits.hpp"

namespace ForeARM
{
namespace ARM
{

void MLA(State::ARMState& state, const uint32_t opcode)
{
    const uint32_t rd_index = Bit::get_bits<16, 19>(opcode);
    const uint64_t ra_value = state.reg(Bit::get_bits<12, 15>(opcode));
    const uint64_t rm_value = state.reg(Bit::get_bits<8, 11>(opcode));
    const uint64_t rn_value = state.reg(Bit::get_bits<0, 3>(opcode));
    const uint32_t result   = static_cast<uint32_t>((rn_value * rm_value + ra_value) & 0xFFFFFFFF);

    state.reg(rd_index) = result;

    if (Bit::is_set<20>(opcode))
    {
        state.cpsr().n(result & 0x80000000);
        state.cpsr().z(result == 0);
    }
}

void MLS(State::ARMState& state, const uint32_t opcode)
{
    const uint32_t rd_index = Bit::get_bits<16, 19>(opcode);
    const uint64_t ra_value = state.reg(Bit::get_bits<12, 15>(opcode));
    const uint64_t rm_value = state.reg(Bit::get_bits<8, 11>(opcode));
    const uint64_t rn_value = state.reg(Bit::get_bits<0, 3>(opcode));

    state.reg(rd_index) = static_cast<uint32_t>(ra_value - rm_value * rn_value);
}

void MUL(State::ARMState& state, const uint32_t opcode)
{
    const uint32_t rd_index = Bit::get_bits<16, 19>(opcode);
    const uint64_t rm_value = state.reg(Bit::get_bits<8, 11>(opcode));
    const uint64_t rn_value = state.reg(Bit::get_bits<0,  3>(opcode));
    const uint32_t result   = static_cast<uint32_t>((rm_value * rn_value) & 0xFFFFFFFF);

    state.reg(rd_index) = result;

    if (Bit::is_set<20>(opcode))
    {
        state.cpsr().n(result & 0x80000000);
        state.cpsr().z(result == 0);
    }
}

} // namespace ARM
} // namespace ForeARM

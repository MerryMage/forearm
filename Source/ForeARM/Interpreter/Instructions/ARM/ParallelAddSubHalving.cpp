#include "ForeARM/Interpreter/Instructions/ARM.hpp"
#include "ForeARM/Interpreter/State/ARMState.hpp"
#include "ForeARM/Utils/Bits.hpp"

namespace ForeARM
{
namespace ARM
{

void SHADD8(State::ARMState& state, const uint32_t opcode)
{
    const uint32_t rd_index = Bit::get_bits<12, 15>(opcode);
    const uint32_t rm_value = state.reg(Bit::get_bits<0, 3>(opcode));
    const uint32_t rn_value = state.reg(Bit::get_bits<16, 19>(opcode));

    const uint32_t sum1 = (Bit::sign_extend<8>(rn_value) + Bit::sign_extend<8>(rm_value)) >> 1;
    const uint32_t sum2 = (Bit::sign_extend<8>(rn_value >>  8) + Bit::sign_extend<8>(rm_value >>  8)) >> 1;
    const uint32_t sum3 = (Bit::sign_extend<8>(rn_value >> 16) + Bit::sign_extend<8>(rm_value >> 16)) >> 1;
    const uint32_t sum4 = (Bit::sign_extend<8>(rn_value >> 24) + Bit::sign_extend<8>(rm_value >> 24)) >> 1;

    state.reg(rd_index) = (sum1 & 0xFF) | ((sum2 & 0xFF) << 8) | ((sum3 & 0xFF) << 16) | ((sum4 & 0xFF) << 24);
}

void SHADD16(State::ARMState& state, const uint32_t opcode)
{
    const uint32_t rd_index = Bit::get_bits<12, 15>(opcode);
    const uint32_t rm_value = state.reg(Bit::get_bits<0, 3>(opcode));
    const uint32_t rn_value = state.reg(Bit::get_bits<16, 19>(opcode));

    const uint32_t sum1 = (Bit::sign_extend<16>(rn_value) + Bit::sign_extend<16>(rm_value)) >> 1;
    const uint32_t sum2 = (Bit::sign_extend<16>(rn_value >> 16) + Bit::sign_extend<16>(rm_value >> 16)) >> 1;

    state.reg(rd_index) = (sum1 & 0xFFFF) | ((sum2 & 0xFFFF) << 16);
}

void SHASX(State::ARMState& state, const uint32_t opcode)
{
    const uint32_t rd_index = Bit::get_bits<12, 15>(opcode);
    const uint32_t rm_value = state.reg(Bit::get_bits<0, 3>(opcode));
    const uint32_t rn_value = state.reg(Bit::get_bits<16, 19>(opcode));

    const uint32_t diff = (Bit::sign_extend<16>(rn_value) - Bit::sign_extend<16>(rm_value >> 16)) >> 1;
    const uint32_t sum  = (Bit::sign_extend<16>(rn_value >> 16) + Bit::sign_extend<16>(rm_value)) >> 1;

    state.reg(rd_index) = (diff & 0xFFFF) | ((sum & 0xFFFF) << 16);
}

void SHSAX(State::ARMState& state, const uint32_t opcode)
{
    const uint32_t rd_index = Bit::get_bits<12, 15>(opcode);
    const uint32_t rm_value = state.reg(Bit::get_bits<0, 3>(opcode));
    const uint32_t rn_value = state.reg(Bit::get_bits<16, 19>(opcode));

    const uint32_t sum  = (Bit::sign_extend<16>(rn_value) + Bit::sign_extend<16>(rm_value >> 16)) >> 1;
    const uint32_t diff = (Bit::sign_extend<16>(rn_value >> 16) - Bit::sign_extend<16>(rm_value)) >> 1;

    state.reg(rd_index) = (sum & 0xFFFF) | ((diff & 0xFFFF) << 16);
}

void SHSUB8(State::ARMState& state, const uint32_t opcode)
{
    const uint32_t rd_index = Bit::get_bits<12, 15>(opcode);
    const uint32_t rm_value = state.reg(Bit::get_bits<0, 3>(opcode));
    const uint32_t rn_value = state.reg(Bit::get_bits<16, 19>(opcode));

    const uint32_t diff1 = (Bit::sign_extend<8>(rn_value) - Bit::sign_extend<8>(rm_value)) >> 1;
    const uint32_t diff2 = (Bit::sign_extend<8>(rn_value >>  8) - Bit::sign_extend<8>(rm_value >>  8)) >> 1;
    const uint32_t diff3 = (Bit::sign_extend<8>(rn_value >> 16) - Bit::sign_extend<8>(rm_value >> 16)) >> 1;
    const uint32_t diff4 = (Bit::sign_extend<8>(rn_value >> 24) - Bit::sign_extend<8>(rm_value >> 24)) >> 1;

    state.reg(rd_index) = (diff1 & 0xFF) | ((diff2 & 0xFF) << 8) | ((diff3 & 0xFF) << 16) | ((diff4 & 0xFF) << 24);
}

void SHSUB16(State::ARMState& state, const uint32_t opcode)
{
    const uint32_t rd_index = Bit::get_bits<12, 15>(opcode);
    const uint32_t rm_value = state.reg(Bit::get_bits<0, 3>(opcode));
    const uint32_t rn_value = state.reg(Bit::get_bits<16, 19>(opcode));

    const uint32_t diff1 = (Bit::sign_extend<16>(rn_value) - Bit::sign_extend<16>(rm_value)) >> 1;
    const uint32_t diff2 = (Bit::sign_extend<16>(rn_value >> 16) - Bit::sign_extend<16>(rm_value >> 16)) >> 1;

    state.reg(rd_index) = (diff1 & 0xFFFF) | ((diff2 & 0xFFFF) << 16);
}

void UHADD8(State::ARMState& state, const uint32_t opcode)
{
    const uint32_t rd_index = Bit::get_bits<12, 15>(opcode);
    const uint32_t rm_value = state.reg(Bit::get_bits<0, 3>(opcode));
    const uint32_t rn_value = state.reg(Bit::get_bits<16, 19>(opcode));

    const uint32_t sum1 = ((rn_value & 0xFF) + (rm_value & 0xFF)) >> 1;
    const uint32_t sum2 = (((rn_value >>  8) & 0xFF) + ((rm_value >>  8) & 0xFF)) >> 1;
    const uint32_t sum3 = (((rn_value >> 16) & 0xFF) + ((rm_value >> 16) & 0xFF)) >> 1;
    const uint32_t sum4 = (((rn_value >> 24) & 0xFF) + ((rm_value >> 24) & 0xFF)) >> 1;

    state.reg(rd_index) = (sum1 & 0xFF) | ((sum2 & 0xFF) << 8) | ((sum3 & 0xFF) << 16) | ((sum4 & 0xFF) << 24);
}

void UHADD16(State::ARMState& state, const uint32_t opcode)
{
    const uint32_t rd_index = Bit::get_bits<12, 15>(opcode);
    const uint32_t rm_value = state.reg(Bit::get_bits<0, 3>(opcode));
    const uint32_t rn_value = state.reg(Bit::get_bits<16, 19>(opcode));

    const uint32_t sum1 = ((rn_value & 0xFFFF) + (rm_value & 0xFFFF)) >> 1;
    const uint32_t sum2 = ((rn_value >> 16)    + (rm_value >> 16))    >> 1;

    state.reg(rd_index) = (sum1 & 0xFFFF) | ((sum2 & 0xFFFF) << 16);
}

void UHASX(State::ARMState& state, const uint32_t opcode)
{
    const uint32_t rd_index = Bit::get_bits<12, 15>(opcode);
    const uint32_t rm_value = state.reg(Bit::get_bits<0, 3>(opcode));
    const uint32_t rn_value = state.reg(Bit::get_bits<16, 19>(opcode));

    const uint32_t diff = ((rn_value & 0xFFFF) - (rm_value >> 16))    >> 1;
    const uint32_t sum  = ((rn_value >> 16)    + (rm_value & 0xFFFF)) >> 1;

    state.reg(rd_index) = (diff & 0xFFFF) | ((sum & 0xFFFF) << 16);
}

void UHSAX(State::ARMState& state, const uint32_t opcode)
{
    const uint32_t rd_index = Bit::get_bits<12, 15>(opcode);
    const uint32_t rm_value = state.reg(Bit::get_bits<0, 3>(opcode));
    const uint32_t rn_value = state.reg(Bit::get_bits<16, 19>(opcode));

    const uint32_t sum  = ((rn_value & 0xFFFF) + (rm_value >> 16))    >> 1;
    const uint32_t diff = ((rn_value >> 16)    - (rm_value & 0xFFFF)) >> 1;

    state.reg(rd_index) = (sum & 0xFFFF) | ((diff & 0xFFFF) << 16);
}

void UHSUB8(State::ARMState& state, const uint32_t opcode)
{
    const uint32_t rd_index = Bit::get_bits<12, 15>(opcode);
    const uint32_t rm_value = state.reg(Bit::get_bits<0, 3>(opcode));
    const uint32_t rn_value = state.reg(Bit::get_bits<16, 19>(opcode));

    const uint32_t diff1 = ((rn_value & 0xFF) - (rm_value & 0xFF)) >> 1;
    const uint32_t diff2 = (((rn_value >>  8) & 0xFF) - ((rm_value >>  8) & 0xFF)) >> 1;
    const uint32_t diff3 = (((rn_value >> 16) & 0xFF) - ((rm_value >> 16) & 0xFF)) >> 1;
    const uint32_t diff4 = (((rn_value >> 24) & 0xFF) - ((rm_value >> 24) & 0xFF)) >> 1;

    state.reg(rd_index) = (diff1 & 0xFF) | ((diff2 & 0xFF) << 8) | ((diff3 & 0xFF) << 16) | ((diff4 & 0xFF) << 24);
}

void UHSUB16(State::ARMState& state, const uint32_t opcode)
{
    const uint32_t rd_index = Bit::get_bits<12, 15>(opcode);
    const uint32_t rm_value = state.reg(Bit::get_bits<0, 3>(opcode));
    const uint32_t rn_value = state.reg(Bit::get_bits<16, 19>(opcode));

    const uint32_t diff1 = ((rn_value & 0xFFFF) - (rm_value & 0xFFFF)) >> 1;
    const uint32_t diff2 = ((rn_value >> 16)    - (rm_value >> 16))    >> 1;

    state.reg(rd_index) = (diff1 & 0xFFFF) | ((diff2 & 0xFFFF) << 16);
}

} // namespace ARM
} // namespace ForeARM

#include "ForeARM/Interpreter/Instructions/ARM.hpp"
#include "ForeARM/Interpreter/State/ARMState.hpp"
#include "ForeARM/Utils/Bits.hpp"

namespace ForeARM
{
namespace ARM
{

void SMLAL(State::ARMState& state, const uint32_t opcode)
{
    const uint32_t rd_lo_index = Bit::get_bits<12, 15>(opcode);
    const uint32_t rd_hi_index = Bit::get_bits<16, 19>(opcode);
    const uint32_t rm_index    = Bit::get_bits<8, 11>(opcode);
    const uint32_t rn_index    = Bit::get_bits<0, 3>(opcode);

    const uint64_t rm_value    = Bit::sign_extend<32, uint64_t>(state.reg(rm_index));
    const uint64_t rn_value    = Bit::sign_extend<32, uint64_t>(state.reg(rn_index));
    const uint64_t rd_lo_value = state.reg(rd_lo_index);
    const uint64_t rd_hi_value = state.reg(rd_hi_index);
    const uint64_t accumulate  = (rd_hi_value << 32) | rd_lo_value;

    const uint64_t result = (rn_value * rm_value) + accumulate;

    state.reg(rd_lo_index) = static_cast<uint32_t>(result);
    state.reg(rd_hi_index) = static_cast<uint32_t>(result >> 32);

    if (Bit::is_set<20>(opcode))
    {
        state.cpsr().n((result & 0x8000000000000000) != 0);
        state.cpsr().z(result == 0);
    }
}

void SMULL(State::ARMState& state, const uint32_t opcode)
{
    const uint32_t rd_lo_index = Bit::get_bits<12, 15>(opcode);
    const uint32_t rd_hi_index = Bit::get_bits<16, 19>(opcode);
    const uint32_t rm_index    = Bit::get_bits<8, 11>(opcode);
    const uint32_t rn_index    = Bit::get_bits<0, 3>(opcode);

    const uint64_t rm_value = Bit::sign_extend<32, uint64_t>(state.reg(rm_index));
    const uint64_t rn_value = Bit::sign_extend<32, uint64_t>(state.reg(rn_index));

    const uint64_t result = rm_value * rn_value;

    state.reg(rd_lo_index) = static_cast<uint32_t>(result);
    state.reg(rd_hi_index) = static_cast<uint32_t>(result >> 32);

    if (Bit::is_set<20>(opcode))
    {
        state.cpsr().n((result & 0x8000000000000000) != 0);
        state.cpsr().z(result == 0);
    }
}

void UMAAL(State::ARMState& state, const uint32_t opcode)
{
    const uint32_t rd_lo_index = Bit::get_bits<12, 15>(opcode);
    const uint32_t rd_hi_index = Bit::get_bits<16, 19>(opcode);

    const uint64_t rd_lo_value = state.reg(rd_lo_index);
    const uint64_t rd_hi_value = state.reg(rd_hi_index);
    const uint64_t rn_value    = state.reg(Bit::get_bits<0, 3>(opcode));
    const uint64_t rm_value    = state.reg(Bit::get_bits<8, 11>(opcode));

    const uint64_t result = (rn_value * rm_value) + (rd_hi_value + rd_lo_value);

    state.reg(rd_lo_index) = static_cast<uint32_t>(result);
    state.reg(rd_hi_index) = static_cast<uint32_t>(result >> 32);
}

void UMLAL(State::ARMState& state, const uint32_t opcode)
{
    const uint32_t rd_lo_index = Bit::get_bits<12, 15>(opcode);
    const uint32_t rd_hi_index = Bit::get_bits<16, 19>(opcode);

    const uint64_t rd_lo_value = state.reg(rd_lo_index);
    const uint64_t rd_hi_value = state.reg(rd_hi_index);
    const uint64_t rm_value    = state.reg(Bit::get_bits<8, 11>(opcode));
    const uint64_t rn_value    = state.reg(Bit::get_bits<0, 3>(opcode));

    const uint64_t result = (rn_value * rm_value) + (rd_lo_value | (rd_hi_value << 32));

    state.reg(rd_lo_index) = static_cast<uint32_t>(result);
    state.reg(rd_hi_index) = static_cast<uint32_t>(result >> 32);

    if (Bit::is_set<20>(opcode))
    {
        state.cpsr().n((result & 0x8000000000000000) != 0);
        state.cpsr().z(result == 0);
    }
}

void UMULL(State::ARMState& state, const uint32_t opcode)
{
    const uint32_t rd_lo_index = Bit::get_bits<12, 15>(opcode);
    const uint32_t rd_hi_index = Bit::get_bits<16, 19>(opcode);

    const uint64_t rm_value = state.reg(Bit::get_bits<8, 11>(opcode));
    const uint64_t rn_value = state.reg(Bit::get_bits<0, 3>(opcode));

    const uint64_t result = rm_value * rn_value;

    state.reg(rd_lo_index) = static_cast<uint32_t>(result);
    state.reg(rd_hi_index) = static_cast<uint32_t>(result >> 32);

    if (Bit::is_set<20>(opcode))
    {
        state.cpsr().n((result & 0x8000000000000000) != 0);
        state.cpsr().z(result == 0);
    }
}

} // namespace ARM
} // namespace ForeARM

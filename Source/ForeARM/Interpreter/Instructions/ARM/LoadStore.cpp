#include "ForeARM/Interpreter/Instructions/ARM.hpp"
#include "ForeARM/Interpreter/Instructions/Helper/Branch.hpp"
#include "ForeARM/Interpreter/Instructions/Helper/LoadStore.hpp"
#include "ForeARM/Interpreter/Instructions/Helper/Shift.hpp"
#include "ForeARM/Interpreter/State/ARMState.hpp"
#include "ForeARM/Utils/Bits.hpp"
#include "ForeARM/Utils/Endian.hpp"

namespace
{
using namespace ForeARM::State;

// An unprivileged load store is the exact same as a normal load-store variant,
// however it simply acts as if the load-store was done in user mode.
template <typename Func>
void unprivileged_load_store(ARMState& state, const uint32_t opcode, const Func function)
{
    const auto current_mode = state.cpsr().mode();

    state.select_processor_mode(PSR::Mode::User);
    function(state, opcode);
    state.select_processor_mode(current_mode);
}

} // Anonymous namespace

namespace ForeARM
{
namespace ARM
{

void LDR_imm(State::ARMState& state, const uint32_t opcode)
{
    const uint32_t rn_index = Bit::get_bits<16, 19>(opcode);
    const uint32_t rt_index = Bit::get_bits<12, 15>(opcode);
    const uint32_t rn_value = State::get_register_value_and_offset_if_pc(state, rn_index);
    const uint32_t imm12    = Bit::get_bits<0, 11>(opcode);

    uint32_t offset_address, target_address;
    std::tie(offset_address, target_address) = LoadStore::get_addresses(opcode, rn_value, imm12);

    const uint32_t data = state.memory_interface().read32(target_address, state);

    if (LoadStore::do_writeback(opcode))
        state.reg(rn_index) = offset_address;

    if (rt_index == 15)
    {
        // Unpredictable
        //
        // TODO: It may be more sane overall to signal an
        //       undefined instruction exception in this case.
        //       This is here as a reminder for me to do this
        //       when I implement the exception vectors.
        //
        //       This also applies to any other occurrences of
        //       'Unpredictable' in this source file.
        //
        if (!LoadStore::is_word_aligned(target_address))
            return;

        Branch::bx_write_pc(state, data);
    }
    else
    {
        state.reg(rt_index) = data;
    }
}

void LDR_reg(State::ARMState& state, const uint32_t opcode)
{
    const uint32_t rm_index   = Bit::get_bits<0, 3>(opcode);
    const uint32_t rn_index   = Bit::get_bits<16, 19>(opcode);
    const uint32_t rt_index   = Bit::get_bits<12, 15>(opcode);
    const uint32_t shift_type = Bit::get_bits<5, 6>(opcode);
    const uint32_t shift_imm  = Bit::get_bits<7, 11>(opcode);

    const uint32_t rm_value = state.reg(rm_index);
    const uint32_t rn_value = State::get_register_value_and_offset_if_pc(state, rn_index);

    const auto decoded_shift = Shift::decode_imm_shift(shift_type, shift_imm);
    const auto offset = Shift::shift_without_carry(rm_value, decoded_shift.type, decoded_shift.amount, state.cpsr().c());

    uint32_t offset_address, target_address;
    std::tie(offset_address, target_address) = LoadStore::get_addresses(opcode, rn_value, offset);

    const uint32_t data = state.memory_interface().read32(target_address, state);

    if (LoadStore::do_writeback(opcode))
        state.reg(rn_index) = offset_address;

    if (rt_index == 15)
    {
        // Unpredictable
        if (!LoadStore::is_word_aligned(target_address))
            return;

        Branch::bx_write_pc(state, data);
    }
    else
    {
        state.reg(rt_index) = data;
    }
}

void LDRB_imm(State::ARMState& state, const uint32_t opcode)
{
    const uint32_t imm12    = Bit::get_bits<0, 11>(opcode);
    const uint32_t rn_index = Bit::get_bits<16, 19>(opcode);
    const uint32_t rn_value = State::get_register_value_and_offset_if_pc(state, rn_index);
    const uint32_t rt_index = Bit::get_bits<12, 15>(opcode);

    uint32_t offset_address, target_address;
    std::tie(offset_address, target_address) = LoadStore::get_addresses(opcode, rn_value, imm12);

    state.reg(rt_index) = state.memory_interface().read8(target_address);

    if (LoadStore::do_writeback(opcode))
        state.reg(rn_index) = offset_address;
}

void LDRB_reg(State::ARMState& state, const uint32_t opcode)
{
    const uint32_t rm_index   = Bit::get_bits<0, 3>(opcode);
    const uint32_t rn_index   = Bit::get_bits<16, 19>(opcode);
    const uint32_t rt_index   = Bit::get_bits<12, 15>(opcode);
    const uint32_t shift_imm  = Bit::get_bits<7, 11>(opcode);
    const uint32_t shift_type = Bit::get_bits<5, 6>(opcode);

    const uint32_t rm_value = state.reg(rm_index);
    const uint32_t rn_value = State::get_register_value_and_offset_if_pc(state, rn_index);

    const auto decoded_shift = Shift::decode_imm_shift(shift_type, shift_imm);
    const auto offset = Shift::shift_without_carry(rm_value, decoded_shift.type, decoded_shift.amount, state.cpsr().c());

    uint32_t offset_address, target_address;
    std::tie(offset_address, target_address) = LoadStore::get_addresses(opcode, rn_value, offset);

    state.reg(rt_index) = state.memory_interface().read8(target_address);

    if (LoadStore::do_writeback(opcode))
        state.reg(rn_index) = offset_address;
}

void LDRBT_imm(State::ARMState& state, const uint32_t opcode)
{
    unprivileged_load_store(state, opcode, LDRB_imm);
}

void LDRBT_reg(State::ARMState& state, const uint32_t opcode)
{
    unprivileged_load_store(state, opcode, LDRB_reg);
}

void LDRD_imm(State::ARMState& state, const uint32_t opcode)
{
    const uint32_t imm = Bit::get_bits<0, 3>(opcode) | (Bit::get_bits<8, 11>(opcode) << 4);
    const uint32_t rn_index = Bit::get_bits<16, 19>(opcode);
    const uint32_t rn_value = State::get_register_value_and_offset_if_pc(state, rn_index);
    const uint32_t rt_index = Bit::get_bits<12, 15>(opcode);

    uint32_t offset_address, target_address;
    std::tie(offset_address, target_address) = LoadStore::get_addresses(opcode, rn_value, imm);

    // TODO: Handle simulated Large Physical Address Extension implementation.
    //       In the case LPAE is supported, this would simply be a single
    //       64-bit read with the endian swaps done on the top and low 32-bits
    //       of the 64-bit data respectively.
    state.reg(rt_index + 0) = state.memory_interface().read32(target_address + 0, state);
    state.reg(rt_index + 1) = state.memory_interface().read32(target_address + 4, state);

    if (LoadStore::do_writeback(opcode))
        state.reg(rn_index) = offset_address;
}

void LDRD_reg(State::ARMState& state, const uint32_t opcode)
{
    const uint32_t rm_index = Bit::get_bits<0, 3>(opcode);
    const uint32_t rn_index = Bit::get_bits<16, 19>(opcode);
    const uint32_t rt_index = Bit::get_bits<12, 15>(opcode);

    const uint32_t rm_value = state.reg(rm_index);
    const uint32_t rn_value = State::get_register_value_and_offset_if_pc(state, rn_index);

    uint32_t offset_address, target_address;
    std::tie(offset_address, target_address) = LoadStore::get_addresses(opcode, rn_value, rm_value);

    // TODO: Ditto here with LPAE
    state.reg(rt_index + 0) = state.memory_interface().read32(target_address + 0, state);
    state.reg(rt_index + 1) = state.memory_interface().read32(target_address + 4, state);

    if (LoadStore::do_writeback(opcode))
        state.reg(rn_index) = offset_address;
}

void LDRH_imm(State::ARMState& state, const uint32_t opcode)
{
    const uint32_t imm = Bit::get_bits<0, 3>(opcode) | (Bit::get_bits<8, 11>(opcode) << 4);
    const uint32_t rn_index = Bit::get_bits<16, 19>(opcode);
    const uint32_t rn_value = State::get_register_value_and_offset_if_pc(state, rn_index);
    const uint32_t rt_index = Bit::get_bits<12, 15>(opcode);

    uint32_t offset_address, target_address;
    std::tie(offset_address, target_address) = LoadStore::get_addresses(opcode, rn_value, imm);

    state.reg(rt_index) = state.memory_interface().read16(target_address, state);

    if (LoadStore::do_writeback(opcode))
        state.reg(rn_index) = offset_address;
}

void LDRH_reg(State::ARMState& state, const uint32_t opcode)
{
    const uint32_t rm_index = Bit::get_bits<0, 3>(opcode);
    const uint32_t rn_index = Bit::get_bits<16, 19>(opcode);
    const uint32_t rt_index = Bit::get_bits<12, 15>(opcode);

    const uint32_t rm_value = state.reg(rm_index);
    const uint32_t rn_value = State::get_register_value_and_offset_if_pc(state, rn_index);

    // TODO: Handle thumb-2's shift encoding when implementing translation for this instruction.

    uint32_t offset_address, target_address;
    std::tie(offset_address, target_address) = LoadStore::get_addresses(opcode, rn_value, rm_value);

    state.reg(rt_index) = state.memory_interface().read16(target_address, state);

    if (LoadStore::do_writeback(opcode))
        state.reg(rn_index) = offset_address;
}

void LDRHT_imm(State::ARMState& state, const uint32_t opcode)
{
    unprivileged_load_store(state, opcode, LDRH_imm);
}

void LDRHT_reg(State::ARMState& state, const uint32_t opcode)
{
    unprivileged_load_store(state, opcode, LDRH_reg);
}

void LDRSB_imm(State::ARMState& state, const uint32_t opcode)
{
    const uint32_t imm = Bit::get_bits<0, 3>(opcode) | (Bit::get_bits<8, 11>(opcode) << 4);
    const uint32_t rn_index = Bit::get_bits<16, 19>(opcode);
    const uint32_t rn_value = State::get_register_value_and_offset_if_pc(state, rn_index);
    const uint32_t rt_index = Bit::get_bits<12, 15>(opcode);

    uint32_t offset_address, target_address;
    std::tie(offset_address, target_address) = LoadStore::get_addresses(opcode, rn_value, imm);

    state.reg(rt_index) = Bit::sign_extend<8, uint32_t>(state.memory_interface().read8(target_address));

    if (LoadStore::do_writeback(opcode))
        state.reg(rn_index) = offset_address;
}

void LDRSB_reg(State::ARMState& state, const uint32_t opcode)
{
    const uint32_t rm_index = Bit::get_bits<0, 3>(opcode);
    const uint32_t rn_index = Bit::get_bits<16, 19>(opcode);
    const uint32_t rt_index = Bit::get_bits<12, 15>(opcode);

    const uint32_t rm_value = state.reg(rm_index);
    const uint32_t rn_value = State::get_register_value_and_offset_if_pc(state, rn_index);

    // TODO: Handle thumb-2's shift encoding when implementing translation for this instruction.

    uint32_t offset_address, target_address;
    std::tie(offset_address, target_address) = LoadStore::get_addresses(opcode, rn_value, rm_value);

    state.reg(rt_index) = Bit::sign_extend<8, uint32_t>(state.memory_interface().read8(target_address));

    if (LoadStore::do_writeback(opcode))
        state.reg(rn_index) = offset_address;
}

void LDRSBT_imm(State::ARMState& state, const uint32_t opcode)
{
    unprivileged_load_store(state, opcode, LDRSB_imm);
}

void LDRSBT_reg(State::ARMState& state, const uint32_t opcode)
{
    unprivileged_load_store(state, opcode, LDRSB_reg);
}

void LDRSH_imm(State::ARMState& state, const uint32_t opcode)
{
    const uint32_t imm = Bit::get_bits<0, 3>(opcode) | (Bit::get_bits<8, 11>(opcode) << 4);
    const uint32_t rn_index = Bit::get_bits<16, 19>(opcode);
    const uint32_t rn_value = State::get_register_value_and_offset_if_pc(state, rn_index);
    const uint32_t rt_index = Bit::get_bits<12, 15>(opcode);

    uint32_t offset_address, target_address;
    std::tie(offset_address, target_address) = LoadStore::get_addresses(opcode, rn_value, imm);

    state.reg(rt_index) = Bit::sign_extend<16, uint32_t>(state.memory_interface().read16(target_address, state));

    if (LoadStore::do_writeback(opcode))
        state.reg(rn_index) = offset_address;
}

void LDRSH_reg(State::ARMState& state, const uint32_t opcode)
{
    const uint32_t rm_index = Bit::get_bits<0, 3>(opcode);
    const uint32_t rn_index = Bit::get_bits<16, 19>(opcode);
    const uint32_t rt_index = Bit::get_bits<12, 15>(opcode);

    const uint32_t rm_value = state.reg(rm_index);
    const uint32_t rn_value = State::get_register_value_and_offset_if_pc(state, rn_index);

    // TODO: Handle thumb-2's shift encoding when implementing translation for this instruction.

    uint32_t offset_address, target_address;
    std::tie(offset_address, target_address) = LoadStore::get_addresses(opcode, rn_value, rm_value);

    state.reg(rt_index) = Bit::sign_extend<16, uint32_t>(state.memory_interface().read16(target_address, state));

    if (LoadStore::do_writeback(opcode))
        state.reg(rn_index) = offset_address;
}

void LDRSHT_imm(State::ARMState& state, const uint32_t opcode)
{
    unprivileged_load_store(state, opcode, LDRSH_imm);
}

void LDRSHT_reg(State::ARMState& state, const uint32_t opcode)
{
    unprivileged_load_store(state, opcode, LDRSH_reg);
}

void LDRT_imm(State::ARMState& state, const uint32_t opcode)
{
    unprivileged_load_store(state, opcode, LDR_imm);
}

void LDRT_reg(State::ARMState& state, const uint32_t opcode)
{
    unprivileged_load_store(state, opcode, LDR_reg);
}

void STR_imm(State::ARMState& state, const uint32_t opcode)
{
    const uint32_t imm12    = Bit::get_bits<0, 11>(opcode);
    const uint32_t rn_index = Bit::get_bits<16, 19>(opcode);
    const uint32_t rt_index = Bit::get_bits<12, 15>(opcode);

    const uint32_t rn_value = State::get_register_value_and_offset_if_pc(state, rn_index);
    const uint32_t rt_value = State::get_register_value_and_offset_if_pc(state, rt_index);

    uint32_t offset_address, target_address;
    std::tie(offset_address, target_address) = LoadStore::get_addresses(opcode, rn_value, imm12);

    state.memory_interface().store32(target_address, rt_value, state);

    if (LoadStore::do_writeback(opcode))
        state.reg(rn_index) = offset_address;
}

void STR_reg(State::ARMState& state, const uint32_t opcode)
{
    const uint32_t rm_index   = Bit::get_bits<0, 3>(opcode);
    const uint32_t rn_index   = Bit::get_bits<16, 19>(opcode);
    const uint32_t rt_index   = Bit::get_bits<12, 15>(opcode);
    const uint32_t shift_imm  = Bit::get_bits<7, 11>(opcode);
    const uint32_t shift_type = Bit::get_bits<5, 6>(opcode);

    const uint32_t rm_value = state.reg(rm_index);
    const uint32_t rn_value = State::get_register_value_and_offset_if_pc(state, rn_index);
    const uint32_t rt_value = State::get_register_value_and_offset_if_pc(state, rt_index);

    const auto decoded_shift = Shift::decode_imm_shift(shift_type, shift_imm);
    const auto offset = Shift::shift_without_carry(rm_value, decoded_shift.type, decoded_shift.amount, state.cpsr().c());

    uint32_t offset_address, target_address;
    std::tie(offset_address, target_address) = LoadStore::get_addresses(opcode, rn_value, offset);

    state.memory_interface().store32(target_address, rt_value, state);

    if (LoadStore::do_writeback(opcode))
        state.reg(rn_index) = offset_address;
}

void STRB_imm(State::ARMState& state, const uint32_t opcode)
{
    const uint32_t imm12    = Bit::get_bits<0, 11>(opcode);
    const uint32_t rn_index = Bit::get_bits<16, 19>(opcode);
    const uint32_t rt_index = Bit::get_bits<12, 15>(opcode);

    const uint32_t rn_value = State::get_register_value_and_offset_if_pc(state, rn_index);
    const uint32_t rt_value = state.reg(rt_index);

    uint32_t offset_address, target_address;
    std::tie(offset_address, target_address) = LoadStore::get_addresses(opcode, rn_value, imm12);

    state.memory_interface().store8(target_address, static_cast<uint8_t>(rt_value & 0xFF));

    if (LoadStore::do_writeback(opcode))
        state.reg(rn_index) = offset_address;
}

void STRB_reg(State::ARMState& state, const uint32_t opcode)
{
    const uint32_t rm_index   = Bit::get_bits<0, 3>(opcode);
    const uint32_t rn_index   = Bit::get_bits<16, 19>(opcode);
    const uint32_t rt_index   = Bit::get_bits<12, 15>(opcode);
    const uint32_t shift_imm  = Bit::get_bits<7, 11>(opcode);
    const uint32_t shift_type = Bit::get_bits<5, 6>(opcode);

    const uint32_t rm_value = state.reg(rm_index);
    const uint32_t rn_value = State::get_register_value_and_offset_if_pc(state, rn_index);
    const uint32_t rt_value = state.reg(rt_index);

    const auto decoded_shift = Shift::decode_imm_shift(shift_type, shift_imm);
    const auto offset = Shift::shift_without_carry(rm_value, decoded_shift.type, decoded_shift.amount, state.cpsr().c());

    uint32_t offset_address, target_address;
    std::tie(offset_address, target_address) = LoadStore::get_addresses(opcode, rn_value, offset);

    state.memory_interface().store8(target_address, static_cast<uint8_t>(rt_value & 0xFF));

    if (LoadStore::do_writeback(opcode))
        state.reg(rn_index) = offset_address;
}

void STRBT_imm(State::ARMState& state, const uint32_t opcode)
{
    unprivileged_load_store(state, opcode, STRB_imm);
}

void STRBT_reg(State::ARMState& state, const uint32_t opcode)
{
    unprivileged_load_store(state, opcode, STRB_reg);
}

void STRD_imm(State::ARMState& state, const uint32_t opcode)
{
    const uint32_t imm      = Bit::get_bits<0, 3>(opcode) | Bit::get_bits<8, 11>(opcode) << 4;
    const uint32_t rn_index = Bit::get_bits<16, 19>(opcode);
    const uint32_t rt_index = Bit::get_bits<12, 15>(opcode);

    const uint32_t rn_value = State::get_register_value_and_offset_if_pc(state, rn_index);
    const uint32_t data_low  = state.reg(rt_index);
    const uint32_t data_high = state.reg(rt_index + 1);

    uint32_t offset_address, target_address;
    std::tie(offset_address, target_address) = LoadStore::get_addresses(opcode, rn_value, imm);

    // TODO: Ditto here with LPAE (see comment in LDRD_imm)
    state.memory_interface().store32(target_address + 0, data_low, state);
    state.memory_interface().store32(target_address + 4, data_high, state);

    if (LoadStore::do_writeback(opcode))
        state.reg(rn_index) = offset_address;
}

void STRD_reg(State::ARMState& state, const uint32_t opcode)
{
    const uint32_t rm_index = Bit::get_bits<0, 3>(opcode);
    const uint32_t rn_index = Bit::get_bits<16, 19>(opcode);
    const uint32_t rt_index = Bit::get_bits<12, 15>(opcode);

    const uint32_t rm_value = state.reg(rm_index);
    const uint32_t rn_value = State::get_register_value_and_offset_if_pc(state, rn_index);
    const uint32_t data_low  = state.reg(rt_index);
    const uint32_t data_high = state.reg(rt_index + 1);

    uint32_t offset_address, target_address;
    std::tie(offset_address, target_address) = LoadStore::get_addresses(opcode, rn_value, rm_value);

    // TODO: Ditto here with LPAE (see comment in LDRD_imm)
    state.memory_interface().store32(target_address + 0, data_low, state);
    state.memory_interface().store32(target_address + 4, data_high, state);

    if (LoadStore::do_writeback(opcode))
        state.reg(rn_index) = offset_address;
}

void STRH_imm(State::ARMState& state, const uint32_t opcode)
{
    const uint32_t imm = Bit::get_bits<0, 3>(opcode) | Bit::get_bits<8, 11>(opcode) << 4;
    const uint32_t rn_index = Bit::get_bits<16, 19>(opcode);
    const uint32_t rt_index = Bit::get_bits<12, 15>(opcode);

    const uint32_t rn_value = State::get_register_value_and_offset_if_pc(state, rn_index);
    const uint32_t rt_value = state.reg(rt_index);

    uint32_t offset_address, target_address;
    std::tie(offset_address, target_address) = LoadStore::get_addresses(opcode, rn_value, imm);

    const uint16_t data = static_cast<uint16_t>(rt_value & 0xFFFF);
    state.memory_interface().store16(target_address, data, state);

    if (LoadStore::do_writeback(opcode))
        state.reg(rn_index) = offset_address;
}

void STRH_reg(State::ARMState& state, const uint32_t opcode)
{
    const uint32_t rm_index = Bit::get_bits<0, 3>(opcode);
    const uint32_t rn_index = Bit::get_bits<16, 19>(opcode);
    const uint32_t rt_index = Bit::get_bits<12, 15>(opcode);

    const uint32_t rm_value = state.reg(rm_index);
    const uint32_t rn_value = State::get_register_value_and_offset_if_pc(state, rn_index);
    const uint32_t rt_value = state.reg(rt_index);

    // TODO: Handle thumb-2's shift encoding when implementing translation for this instruction.

    uint32_t offset_address, target_address;
    std::tie(offset_address, target_address) = LoadStore::get_addresses(opcode, rn_value, rm_value);

    const uint16_t data = static_cast<uint16_t>(rt_value & 0xFFFF);
    state.memory_interface().store16(target_address, data, state);

    if (LoadStore::do_writeback(opcode))
        state.reg(rn_index) = offset_address;
}

void STRHT_imm(State::ARMState& state, const uint32_t opcode)
{
    unprivileged_load_store(state, opcode, STRH_imm);
}

void STRHT_reg(State::ARMState& state, const uint32_t opcode)
{
    unprivileged_load_store(state, opcode, STRH_reg);
}

void STRT_imm(State::ARMState& state, const uint32_t opcode)
{
    unprivileged_load_store(state, opcode, STR_imm);
}

void STRT_reg(State::ARMState& state, const uint32_t opcode)
{
    unprivileged_load_store(state, opcode, STR_reg);
}

} // namespace ARM
} // namespace ForeARM



#include <cstdint>
#include "ForeARM/Interpreter/Instructions/Helper/Arithmetic.hpp"
#include "ForeARM/Interpreter/Instructions/Helper/Shift.hpp"
#include "ForeARM/Interpreter/State/ARMState.hpp"
#include "ForeARM/Utils/Bits.hpp"

namespace ForeARM
{
namespace Arithmetic
{

AddWithCarryResult add_with_carry(const uint32_t x, const uint32_t y, const uint32_t carry_in) noexcept
{
    const uint64_t unsigned_sum = static_cast<uint64_t>(x) +
                                  static_cast<uint64_t>(y) +
                                  static_cast<uint64_t>(carry_in);

    const uint64_t signed_sum   = Bit::sign_extend<32, uint64_t>(x) +
                                  Bit::sign_extend<32, uint64_t>(y) +
                                  static_cast<uint64_t>(carry_in);

    const uint32_t result    = static_cast<uint32_t>(unsigned_sum);
    const uint32_t carry_out = result != unsigned_sum;
    const uint32_t overflow  = Bit::sign_extend<32, uint64_t>(result) != signed_sum;

    return { result, carry_out, overflow };
}

uint32_t expand_immediate(const uint32_t imm12) noexcept
{
    const uint32_t unrotated_value = Bit::get_bits<0, 7>(imm12);
    const uint32_t rotation = Bit::get_bits<8,11>(imm12);

    return Bit::rotate_right(unrotated_value, 2 * rotation);
}

Shift::ShiftWithCarryResult expand_immediate_with_carry_out(const uint32_t imm12, const uint32_t carry_in)
{
    const uint32_t unrotated_value = Bit::get_bits<0, 7>(imm12);
    const uint32_t rotation = Bit::get_bits<8,11>(imm12);

    return Shift::shift_with_carry(unrotated_value, Shift::ShiftType::ROR, 2 * rotation, carry_in);
}

} // namespace Arithmetic
} // namespace ForeARM

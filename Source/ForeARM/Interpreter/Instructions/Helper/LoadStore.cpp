#include <cstdint>
#include <tuple>
#include "ForeARM/Interpreter/Instructions/Helper/LoadStore.hpp"

namespace
{

// Whether or not the given load-store opcode indicates indexed semantics.
constexpr bool is_indexed(const uint32_t opcode) noexcept
{
    return (opcode & 0x1000000) != 0;
}

// Whether or not the given load-store opcode indicates the base register
// should be offset by addition. If this is false, then a subtraction is
// performed instead.
constexpr bool add_offset(const uint32_t opcode) noexcept
{
    return (opcode & 0x800000) != 0;
}

// Gets the correct offset address depending on the given opcode's addressing mode.
constexpr uint32_t get_offset_address(const uint32_t opcode, const uint32_t register_value, const uint32_t offset) noexcept
{
    return add_offset(opcode)
           ? register_value + offset
           : register_value - offset;
}

// Gets the correct target address depending on whether or not the opcode indicates indexed semantics.
constexpr uint32_t get_target_address(const uint32_t opcode, const uint32_t register_value, const uint32_t offset_address) noexcept
{
    return is_indexed(opcode)
           ? offset_address
           : register_value;
}

} // Anonymous namespace

namespace ForeARM
{
namespace LoadStore
{

std::tuple<uint32_t, uint32_t> get_addresses(const uint32_t opcode, const uint32_t register_value, const uint32_t offset) noexcept
{
    const uint32_t offset_address = get_offset_address(opcode, register_value, offset);
    const uint32_t target_address = get_target_address(opcode, register_value, offset_address);

    return std::make_tuple(offset_address, target_address);
}

bool do_writeback(const uint32_t opcode) noexcept
{
    return !is_indexed(opcode) || (opcode & 0x200000) != 0;
}

bool is_word_aligned(const uint32_t value) noexcept
{
    return (value & 0b11) == 0;
}

} // namespace LoadStore
} // namespace ForeARM

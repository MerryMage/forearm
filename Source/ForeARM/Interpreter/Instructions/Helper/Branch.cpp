#include <cstdint>
#include "ForeARM/Interpreter/Instructions/Helper/Branch.hpp"
#include "ForeARM/Interpreter/State/ARMState.hpp"

namespace ForeARM
{
namespace Branch
{

void branch_to(State::ARMState& state, const uint32_t address) noexcept
{
    state.reg(15) = address;
}

void branch_write_pc(State::ARMState& state, const uint32_t address) noexcept
{
    if (state.cpsr().t() == 0)
        branch_to(state, address & 0xFFFFFFFC);
    else // Thumb
        branch_to(state, address & 0xFFFFFFFE);
}

void bx_write_pc(State::ARMState& state, const uint32_t address) noexcept
{
    if (address & 1)
    {
        state.select_instruction_set(State::InstructionSet::Thumb);
        branch_to(state, address & ~1U);
    }
    else if ((address & 2) == 0)
    {
        state.select_instruction_set(State::InstructionSet::ARM);
        branch_to(state, address);
    }
}

} // namespace Branch
} // namespace ForeARM

#include <cmath>
#include <cstdint>
#include "ForeARM/Interpreter/Instructions/Helper/Saturation.hpp"
#include "ForeARM/Utils/Bits.hpp"

namespace ForeARM
{
namespace Saturation
{

SaturationResult signed_saturation_q(const uint64_t value, const uint32_t to) noexcept
{
    const uint64_t ceiling =  (1ULL << (to - 1ULL)) - 1ULL;
    const uint64_t floor   = ~ceiling;
    const bool sign_bit    = (value & 0x8000000000000000) != 0;

    if (sign_bit && value < floor)
        return {static_cast<uint32_t>(floor), true};

    if (!sign_bit && value > ceiling)
        return {static_cast<uint32_t>(ceiling), true};

    return {static_cast<uint32_t>(value), false};
}

SaturationResult unsigned_saturation_q(const uint64_t value, const uint32_t to) noexcept
{
    const uint64_t ceiling = (1ULL << to) - 1ULL;

    // Negative
    if (value & 0x8000000000000000)
        return {0, true};

    if (value > ceiling)
        return {static_cast<uint32_t>(ceiling), true};

    return {static_cast<uint32_t>(value), false};
}

uint32_t signed_saturation(const uint64_t value, const uint32_t to) noexcept
{
    return signed_saturation_q(value, to).value;
}

uint32_t unsigned_saturation(const uint64_t value, const uint32_t to) noexcept
{
    return unsigned_saturation_q(value, to).value;
}

} // namespace Saturation
} // namespace ForeARM

#pragma once

#include <cstdint>

namespace ForeARM
{
namespace Saturation
{

struct SaturationResult final
{
    const uint32_t value{};
    const bool     saturation_occurred{};
};

SaturationResult signed_saturation_q(uint64_t value, uint32_t to) noexcept;
SaturationResult unsigned_saturation_q(uint64_t value, uint32_t to) noexcept;

uint32_t signed_saturation(uint64_t value, uint32_t to) noexcept;
uint32_t unsigned_saturation(uint64_t value, uint32_t to) noexcept;

} // namespace Saturation
} // namespace ForeARM

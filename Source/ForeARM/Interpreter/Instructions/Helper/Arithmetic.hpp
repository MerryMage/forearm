#pragma once

#include <cstdint>

namespace ForeARM
{

namespace Shift { struct ShiftWithCarryResult; }

namespace Arithmetic
{

struct AddWithCarryResult final
{
    const uint32_t value{};
    const uint32_t carry_out{};
    const uint32_t overflow{};
};

// Performs an add-with-carry for two arbitary 32-bit integers.
AddWithCarryResult add_with_carry(uint32_t x, uint32_t y, uint32_t carry_in) noexcept;

// Expands a modified immediate.
uint32_t expand_immediate(uint32_t imm12) noexcept;

// Expands a modified immediate, getting the carry out value alongside it.
Shift::ShiftWithCarryResult expand_immediate_with_carry_out(uint32_t imm12, uint32_t carry_in);

} // namespace Arithmetic
} // namespace ForeARM

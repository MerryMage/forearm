#pragma once

#include <cstdint>

namespace ForeARM
{
namespace Shift
{

enum class ShiftType
{
    LSL,
    LSR,
    ASR,
    ROR,
    RRX,
    Invalid,
};

struct ImmediateShiftResult final
{
    const ShiftType type{};
    const uint32_t  amount{};
};

struct ShiftWithCarryResult final
{
    const uint32_t value{};
    const uint32_t carry_out{};
};

ImmediateShiftResult decode_imm_shift(uint32_t type, uint32_t imm5) noexcept;
ShiftType decode_reg_shift(uint32_t type) noexcept;

uint32_t shift_without_carry(uint32_t value, ShiftType type, uint32_t amount, bool carry_in);
ShiftWithCarryResult shift_with_carry(uint32_t value, ShiftType type, uint32_t amount, bool carry_in);

} // namespace Shift
} // namespace ForeARM

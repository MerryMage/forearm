#include <cassert>
#include <cstdint>
#include "ForeARM/Interpreter/Instructions/Helper/Shift.hpp"
#include "ForeARM/Utils/Bits.hpp"

namespace
{

using namespace ForeARM::Shift;

uint32_t LSL(const uint32_t value, const uint32_t shift_amount) noexcept
{
    return value << shift_amount;
}

ShiftWithCarryResult LSL_C(const uint32_t value, const uint32_t shift_amount) noexcept
{
    // Don't do the full shift, so the value can be inspected for a carry.
    const uint32_t semi_shifted_value = value << (shift_amount - 1);
    const uint32_t carry = (value & 0x80000000) != 0;

    return {semi_shifted_value << 1, carry};
}

uint32_t LSR(const uint32_t value, const uint32_t shift_amount) noexcept
{
    return value >> shift_amount;
}

ShiftWithCarryResult LSR_C(const uint32_t value, const uint32_t shift_amount) noexcept
{
    // Don't do the full shift, so the value can be inspected for a carry.
    const uint32_t semi_shifted_value = value >> (shift_amount - 1);
    const uint32_t carry = (value & 1) != 0;

    return {semi_shifted_value >> 1, carry};
}

uint32_t ASR(const uint32_t value, const uint32_t shift_amount) noexcept
{
    const bool sign_bit = (value & 0x80000000) != 0;

    uint32_t shifted_value = value >> shift_amount;

    if (sign_bit)
    {
        constexpr const uint32_t bit_size = Bit::bit_size<uint32_t>() - 1;
        shifted_value |= Bit::create_bitmask<uint32_t>(bit_size - (shift_amount - 1), bit_size);
    }

    return shifted_value;
}

ShiftWithCarryResult ASR_C(const uint32_t value, const uint32_t shift_amount) noexcept
{
    const bool sign_bit = (value & 0x80000000) != 0;

    // Don't do the full shift, so the value can be inspected for a carry.
    const uint32_t semi_shifted_value = value >> (shift_amount - 1);
    const uint32_t carry = (semi_shifted_value & 1) == 1;

    uint32_t fully_shifted_value = semi_shifted_value >> 1;

    if (sign_bit)
    {
        constexpr uint32_t bit_size = Bit::bit_size<uint32_t>() - 1;
        fully_shifted_value |= Bit::create_bitmask<uint32_t>(bit_size - (shift_amount - 1), bit_size);
    }

    return {fully_shifted_value, carry};
}

uint32_t ROR(const uint32_t value, const uint32_t shift_amount) noexcept
{
    return Bit::rotate_right(value, shift_amount);
}

ShiftWithCarryResult ROR_C(const uint32_t value, const uint32_t shift_amount) noexcept
{
    const uint32_t rotated_value = Bit::rotate_right(value, shift_amount);
    const uint32_t carry = (rotated_value & 0x80000000) != 0;

    return {rotated_value, carry};
}

uint32_t RRX(const uint32_t value, const bool carry_in) noexcept
{
    uint32_t shifted_value = value >> 1;

    if (carry_in)
        shifted_value |= 0x80000000;

    return shifted_value;
}

ShiftWithCarryResult RRX_C(const uint32_t value, const bool carry_in) noexcept
{
    const uint32_t carry_out = (value & 1) != 0;
    uint32_t shifted_value = value >> 1;

    if (carry_in)
        shifted_value |= 0x80000000;

    return {shifted_value, carry_out};
}

} // Anonymous namespace

namespace ForeARM
{
namespace Shift
{

ImmediateShiftResult decode_imm_shift(const uint32_t type, const uint32_t imm5) noexcept
{
    switch (type)
    {
    case 0:
        return {ShiftType::LSL, imm5};
    case 1:
        return {ShiftType::LSR, (imm5 == 0) ? 32 : imm5};
    case 2:
        return {ShiftType::ASR, (imm5 == 0) ? 32 : imm5};
    case 3:
        if (imm5 == 0)
            return {ShiftType::RRX, 1};

        return {ShiftType::ROR, imm5};
    }

    return {ShiftType::Invalid, 0};
}

ShiftType decode_reg_shift(const uint32_t type) noexcept
{
    switch (type)
    {
    case 0:
        return ShiftType::LSL;
    case 1:
        return ShiftType::LSR;
    case 2:
        return ShiftType::ASR;
    case 3:
        return ShiftType::ROR;
    }

    return ShiftType::Invalid;
}

uint32_t shift_without_carry(const uint32_t value, const ShiftType type, uint32_t amount, const bool carry_in)
{
    if (amount == 0)
        return value;

    amount &= 0x3F;

    switch (type)
    {
    case ShiftType::LSL:
        return LSL(value, amount);
    case ShiftType::LSR:
        return LSR(value, amount);
    case ShiftType::ASR:
        return ASR(value, amount);
    case ShiftType::ROR:
        return ROR(value, amount);
    case ShiftType::RRX:
        return RRX(value, carry_in);
    case ShiftType::Invalid:
        break;
    }

    // Should never happen.
    assert(false);
    return 0;
}

ShiftWithCarryResult shift_with_carry(const uint32_t value, const ShiftType type, uint32_t amount, const bool carry_in)
{
    if (amount == 0)
        return {value, carry_in};

    amount &= 0x3F;

    switch (type)
    {
    case ShiftType::LSL:
        return LSL_C(value, amount);
    case ShiftType::LSR:
        return LSR_C(value, amount);
    case ShiftType::ASR:
        return ASR_C(value, amount);
    case ShiftType::ROR:
        return ROR_C(value, amount);
    case ShiftType::RRX:
        return RRX_C(value, carry_in);
    case ShiftType::Invalid:
        break;
    }

    // Should never happen.
    assert(false);
    return {0, 0};
}

} // namespace Shift
} // namespace ForeARM

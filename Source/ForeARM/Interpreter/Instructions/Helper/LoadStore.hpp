#pragma once

#include <cstdint>
#include <tuple>

namespace ForeARM
{
namespace LoadStore
{

// Retrieves the offset address and target address (in that order) for load store operations.
std::tuple<uint32_t, uint32_t> get_addresses(uint32_t opcode, uint32_t register_value, uint32_t offset) noexcept;

// Whether or not the given load-store opcode indicates a writeback
// should be performed to the base register.
bool do_writeback(uint32_t opcode) noexcept;

// Indicates whether or not the given value is word-aligned.
bool is_word_aligned(uint32_t value) noexcept;

} // namespace LoadStore
} // namespace ForeARM

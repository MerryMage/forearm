#pragma once

#include <cstdint>

namespace ForeARM
{
namespace State { class ARMState; }
namespace Branch
{

// Branches to the given address
void branch_to(State::ARMState&, uint32_t) noexcept;

// Writes a given address to the program counter without an interworking branch.
void branch_write_pc(State::ARMState&, uint32_t) noexcept;

// Writes a given address to the program counter with an interworking branch
void bx_write_pc(State::ARMState&, uint32_t) noexcept;

} // namespace Branch
} // namespace ForeARM

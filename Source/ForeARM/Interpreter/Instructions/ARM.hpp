#pragma once

#include <cstdint>

namespace ForeARM
{
namespace State { class ARMState; }
namespace ARM
{

// Barrier instructions
void DMB(State::ARMState&, uint32_t);
void DSB(State::ARMState&, uint32_t);
void ISB(State::ARMState&, uint32_t);

// Bit Field instructions
void BFC(State::ARMState&, uint32_t);
void BFI(State::ARMState&, uint32_t);
void SBFX(State::ARMState&, uint32_t);
void UBFX(State::ARMState&, uint32_t);

// Branch instructions
void B(State::ARMState&, uint32_t);
void BLX_imm(State::ARMState&, uint32_t);
void BLX_reg(State::ARMState&, uint32_t);
void BX(State::ARMState&, uint32_t);
void BXJ(State::ARMState&, uint32_t);

// Coprocessor instructions
void CDP(State::ARMState&, uint32_t);
void LDC(State::ARMState&, uint32_t);
void MCR(State::ARMState&, uint32_t);
void MCRR(State::ARMState&, uint32_t);
void MRC(State::ARMState&, uint32_t);
void MRRC(State::ARMState&, uint32_t);
void STC(State::ARMState&, uint32_t);

// Data processing instructions
void ADC_imm(State::ARMState&, uint32_t);
void ADC_reg(State::ARMState&, uint32_t);
void ADC_rsr(State::ARMState&, uint32_t);
void ADD_imm(State::ARMState&, uint32_t);
void ADD_reg(State::ARMState&, uint32_t);
void ADD_rsr(State::ARMState&, uint32_t);
void AND_imm(State::ARMState&, uint32_t);
void AND_reg(State::ARMState&, uint32_t);
void AND_rsr(State::ARMState&, uint32_t);
void BIC_imm(State::ARMState&, uint32_t);
void BIC_reg(State::ARMState&, uint32_t);
void BIC_rsr(State::ARMState&, uint32_t);
void CMN_imm(State::ARMState&, uint32_t);
void CMN_reg(State::ARMState&, uint32_t);
void CMN_rsr(State::ARMState&, uint32_t);
void CMP_imm(State::ARMState&, uint32_t);
void CMP_reg(State::ARMState&, uint32_t);
void CMP_rsr(State::ARMState&, uint32_t);
void EOR_imm(State::ARMState&, uint32_t);
void EOR_reg(State::ARMState&, uint32_t);
void EOR_rsr(State::ARMState&, uint32_t);
void MOV_imm(State::ARMState&, uint32_t);
void MOV_reg(State::ARMState&, uint32_t);
void MOVT(State::ARMState&, uint32_t);
void MOVW(State::ARMState&, uint32_t);
void MVN_imm(State::ARMState&, uint32_t);
void MVN_reg(State::ARMState&, uint32_t);
void MVN_rsr(State::ARMState&, uint32_t);
void ORR_imm(State::ARMState&, uint32_t);
void ORR_reg(State::ARMState&, uint32_t);
void ORR_rsr(State::ARMState&, uint32_t);
void RSB_imm(State::ARMState&, uint32_t);
void RSB_reg(State::ARMState&, uint32_t);
void RSB_rsr(State::ARMState&, uint32_t);
void RSC_imm(State::ARMState&, uint32_t);
void RSC_reg(State::ARMState&, uint32_t);
void RSC_rsr(State::ARMState&, uint32_t);
void SBC_imm(State::ARMState&, uint32_t);
void SBC_reg(State::ARMState&, uint32_t);
void SBC_rsr(State::ARMState&, uint32_t);
void SUB_imm(State::ARMState&, uint32_t);
void SUB_reg(State::ARMState&, uint32_t);
void SUB_rsr(State::ARMState&, uint32_t);
void TEQ_imm(State::ARMState&, uint32_t);
void TEQ_reg(State::ARMState&, uint32_t);
void TEQ_rsr(State::ARMState&, uint32_t);
void TST_imm(State::ARMState&, uint32_t);
void TST_reg(State::ARMState&, uint32_t);
void TST_rsr(State::ARMState&, uint32_t);

// Divide instructions
void SDIV(State::ARMState&, uint32_t);
void UDIV(State::ARMState&, uint32_t);

// Exception generation instructions
void BKPT(State::ARMState&, uint32_t);
void HVC(State::ARMState&, uint32_t);
void SMC(State::ARMState&, uint32_t);
void SVC(State::ARMState&, uint32_t);
void UDF(State::ARMState&, uint32_t);

// Extension functions
void SXTAB(State::ARMState&, uint32_t);
void SXTAB16(State::ARMState&, uint32_t);
void SXTAH(State::ARMState&, uint32_t);
void SXTB(State::ARMState&, uint32_t);
void SXTB16(State::ARMState&, uint32_t);
void SXTH(State::ARMState&, uint32_t);
void UXTAB(State::ARMState&, uint32_t);
void UXTAB16(State::ARMState&, uint32_t);
void UXTAH(State::ARMState&, uint32_t);
void UXTB(State::ARMState&, uint32_t);
void UXTB16(State::ARMState&, uint32_t);
void UXTH(State::ARMState&, uint32_t);

// Hint instructions
void DBG(State::ARMState&, uint32_t);
void PLD(State::ARMState&, uint32_t);
void PLI(State::ARMState&, uint32_t);
void SEV(State::ARMState&, uint32_t);
void WFE(State::ARMState&, uint32_t);
void WFI(State::ARMState&, uint32_t);
void YIELD(State::ARMState&, uint32_t);

// Load/Store instructions
void LDR_imm(State::ARMState&, uint32_t);
void LDR_reg(State::ARMState&, uint32_t);
void LDRB_imm(State::ARMState&, uint32_t);
void LDRB_reg(State::ARMState&, uint32_t);
void LDRBT_imm(State::ARMState&, uint32_t);
void LDRBT_reg(State::ARMState&, uint32_t);
void LDRD_imm(State::ARMState&, uint32_t);
void LDRD_reg(State::ARMState&, uint32_t);
void LDRH_imm(State::ARMState&, uint32_t);
void LDRH_reg(State::ARMState&, uint32_t);
void LDRHT_imm(State::ARMState&, uint32_t);
void LDRHT_reg(State::ARMState&, uint32_t);
void LDRSB_imm(State::ARMState&, uint32_t);
void LDRSB_reg(State::ARMState&, uint32_t);
void LDRSBT_imm(State::ARMState&, uint32_t);
void LDRSBT_reg(State::ARMState&, uint32_t);
void LDRSH_imm(State::ARMState&, uint32_t);
void LDRSH_reg(State::ARMState&, uint32_t);
void LDRSHT_imm(State::ARMState&, uint32_t);
void LDRSHT_reg(State::ARMState&, uint32_t);
void LDRT_imm(State::ARMState&, uint32_t);
void LDRT_reg(State::ARMState&, uint32_t);
void STR_imm(State::ARMState&, uint32_t);
void STR_reg(State::ARMState&, uint32_t);
void STRB_imm(State::ARMState&, uint32_t);
void STRB_reg(State::ARMState&, uint32_t);
void STRBT_imm(State::ARMState&, uint32_t);
void STRBT_reg(State::ARMState&, uint32_t);
void STRD_imm(State::ARMState&, uint32_t);
void STRD_reg(State::ARMState&, uint32_t);
void STRH_imm(State::ARMState&, uint32_t);
void STRH_reg(State::ARMState&, uint32_t);
void STRHT_imm(State::ARMState&, uint32_t);
void STRHT_reg(State::ARMState&, uint32_t);
void STRT_imm(State::ARMState&, uint32_t);
void STRT_reg(State::ARMState&, uint32_t);

// Load/Store multiple instructions
void LDMIA(State::ARMState&, uint32_t);
void LDMDA(State::ARMState&, uint32_t);
void LDMDB(State::ARMState&, uint32_t);
void LDMIB(State::ARMState&, uint32_t);
void STMIA(State::ARMState&, uint32_t);
void STMDA(State::ARMState&, uint32_t);
void STMDB(State::ARMState&, uint32_t);
void STMIB(State::ARMState&, uint32_t);
// TODO: Implement LDM and STM exception return and user register system instructions.
void LDM_stub(State::ARMState&, uint32_t);
void STM_stub(State::ARMState&, uint32_t);

// Miscellaneous instructions
void CLZ(State::ARMState&, uint32_t);
void ERET(State::ARMState&, uint32_t);
void NOP(State::ARMState&, uint32_t);
void SEL(State::ARMState&, uint32_t);

// Unsigned sum of absolute difference functions
void USAD8(State::ARMState&, uint32_t);
void USADA8(State::ARMState&, uint32_t);

// Packing instructions
void PKH(State::ARMState&, uint32_t);

// Reversal instructions
void RBIT(State::ARMState&, uint32_t);
void REV(State::ARMState&, uint32_t);
void REV16(State::ARMState&, uint32_t);
void REVSH(State::ARMState&, uint32_t);

// Saturation instructions
void SSAT(State::ARMState&, uint32_t);
void SSAT16(State::ARMState&, uint32_t);
void USAT(State::ARMState&, uint32_t);
void USAT16(State::ARMState&, uint32_t);

// Multiply (Normal) instructions
void MLA(State::ARMState&, uint32_t);
void MLS(State::ARMState&, uint32_t);
void MUL(State::ARMState&, uint32_t);

// Multiply (Long) instructions
void SMLAL(State::ARMState&, uint32_t);
void SMULL(State::ARMState&, uint32_t);
void UMAAL(State::ARMState&, uint32_t);
void UMLAL(State::ARMState&, uint32_t);
void UMULL(State::ARMState&, uint32_t);

// Multiply (Halfword) instructions
void SMLALXY(State::ARMState&, uint32_t);
void SMLAXY(State::ARMState&, uint32_t);
void SMULXY(State::ARMState&, uint32_t);

// Multiply (word by halfword) instructions
void SMLAWY(State::ARMState&, uint32_t);
void SMULWY(State::ARMState&, uint32_t);

// Multiply (Most significant word) instructions
void SMMLA(State::ARMState&, uint32_t);
void SMMLS(State::ARMState&, uint32_t);
void SMMUL(State::ARMState&, uint32_t);

// Multiply (Dual) instructions
void SMLAD(State::ARMState&, uint32_t);
void SMLALD(State::ARMState&, uint32_t);
void SMLSD(State::ARMState&, uint32_t);
void SMLSLD(State::ARMState&, uint32_t);
void SMUAD(State::ARMState&, uint32_t);
void SMUSD(State::ARMState&, uint32_t);

// Parallel Add/Subtract (Modulo arithmetic) instructions
void SADD8(State::ARMState&, uint32_t);
void SADD16(State::ARMState&, uint32_t);
void SASX(State::ARMState&, uint32_t);
void SSAX(State::ARMState&, uint32_t);
void SSUB8(State::ARMState&, uint32_t);
void SSUB16(State::ARMState&, uint32_t);
void UADD8(State::ARMState&, uint32_t);
void UADD16(State::ARMState&, uint32_t);
void UASX(State::ARMState&, uint32_t);
void USAX(State::ARMState&, uint32_t);
void USUB8(State::ARMState&, uint32_t);
void USUB16(State::ARMState&, uint32_t);

// Parallel Add/Subtract (Saturating) instructions
void QADD8(State::ARMState&, uint32_t);
void QADD16(State::ARMState&, uint32_t);
void QASX(State::ARMState&, uint32_t);
void QSAX(State::ARMState&, uint32_t);
void QSUB8(State::ARMState&, uint32_t);
void QSUB16(State::ARMState&, uint32_t);
void UQADD8(State::ARMState&, uint32_t);
void UQADD16(State::ARMState&, uint32_t);
void UQASX(State::ARMState&, uint32_t);
void UQSAX(State::ARMState&, uint32_t);
void UQSUB8(State::ARMState&, uint32_t);
void UQSUB16(State::ARMState&, uint32_t);

// Parallel Add/Subtract (Halving) instructions
void SHADD8(State::ARMState&, uint32_t);
void SHADD16(State::ARMState&, uint32_t);
void SHASX(State::ARMState&, uint32_t);
void SHSAX(State::ARMState&, uint32_t);
void SHSUB8(State::ARMState&, uint32_t);
void SHSUB16(State::ARMState&, uint32_t);
void UHADD8(State::ARMState&, uint32_t);
void UHADD16(State::ARMState&, uint32_t);
void UHASX(State::ARMState&, uint32_t);
void UHSAX(State::ARMState&, uint32_t);
void UHSUB8(State::ARMState&, uint32_t);
void UHSUB16(State::ARMState&, uint32_t);

// Saturated Add/Subtract instructions
void QADD(State::ARMState&, uint32_t);
void QSUB(State::ARMState&, uint32_t);
void QDADD(State::ARMState&, uint32_t);
void QDSUB(State::ARMState&, uint32_t);

// Shift instructions
void ASR_imm(State::ARMState&, uint32_t);
void ASR_reg(State::ARMState&, uint32_t);
void LSL_imm(State::ARMState&, uint32_t);
void LSL_reg(State::ARMState&, uint32_t);
void LSR_imm(State::ARMState&, uint32_t);
void LSR_reg(State::ARMState&, uint32_t);
void ROR_imm(State::ARMState&, uint32_t);
void ROR_reg(State::ARMState&, uint32_t);
void RRX(State::ARMState&, uint32_t);

// Synchronization Primitive instructions
void CLREX(State::ARMState&, uint32_t);
void LDREX(State::ARMState&, uint32_t);
void LDREXB(State::ARMState&, uint32_t);
void LDREXD(State::ARMState&, uint32_t);
void LDREXH(State::ARMState&, uint32_t);
void STREX(State::ARMState&, uint32_t);
void STREXB(State::ARMState&, uint32_t);
void STREXD(State::ARMState&, uint32_t);
void STREXH(State::ARMState&, uint32_t);
void SWP(State::ARMState&, uint32_t);

// Status register access instructions
void CPS(State::ARMState&, uint32_t);
void MRS(State::ARMState&, uint32_t);
void MRS_banked(State::ARMState&, uint32_t);
void MSR_imm(State::ARMState&, uint32_t);
void MSR_reg(State::ARMState&, uint32_t);
void MSR_banked(State::ARMState&, uint32_t);
void RFE(State::ARMState&, uint32_t);
void SETEND(State::ARMState&, uint32_t);
void SRS(State::ARMState&, uint32_t);

} // namespace ARM
} // namespace ForeARM

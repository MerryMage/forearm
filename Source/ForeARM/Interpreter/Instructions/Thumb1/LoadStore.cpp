#include "ForeARM/Interpreter/Instructions/Thumb1.hpp"
#include "ForeARM/Interpreter/Instructions/Helper/Branch.hpp"
#include "ForeARM/Interpreter/Instructions/Helper/Shift.hpp"
#include "ForeARM/Interpreter/State/ARMState.hpp"
#include "ForeARM/Utils/Bits.hpp"
#include "ForeARM/Utils/Endian.hpp"

namespace ForeARM
{
namespace Thumb1
{

void LDR_imm(State::ARMState& state, const uint32_t opcode)
{
    const bool is_t1_encoding = Bit::is_set<13>(opcode);

    uint32_t rn_index = 0;
    uint32_t rt_index = 0;
    uint32_t imm = 0;

    if (is_t1_encoding)
    {
        imm      = Bit::get_bits<6, 10>(opcode) << 2;
        rn_index = Bit::get_bits<3, 5>(opcode);
        rt_index = Bit::get_bits<0, 2>(opcode);
    }
    else
    {
        imm      = Bit::get_bits<0, 7>(opcode) << 2;
        rn_index = 13;
        rt_index = Bit::get_bits<8, 10>(opcode);
    }

    const uint32_t rn_value = state.reg(rn_index);
    const uint32_t target_address = rn_value + imm;

    state.reg(rt_index) = state.memory_interface().read32(target_address, state);
}

void LDR_lit(State::ARMState& state, const uint32_t opcode)
{
    const uint32_t imm      = Bit::get_bits<0, 7>(opcode) << 2;
    const uint32_t rt_index = Bit::get_bits<8, 10>(opcode);
    const uint32_t base     = State::get_word_aligned_program_counter(state);

    const uint32_t target_address = base + imm;

    state.reg(rt_index) = state.memory_interface().read32(target_address, state);
}

void LDR_reg(State::ARMState& state, const uint32_t opcode)
{
    const uint32_t rm_index = Bit::get_bits<6, 8>(opcode);
    const uint32_t rn_index = Bit::get_bits<3, 5>(opcode);
    const uint32_t rt_index = Bit::get_bits<0, 2>(opcode);

    const uint32_t rm_value = state.reg(rm_index);
    const uint32_t rn_value = state.reg(rn_index);

    const uint32_t target_address = rn_value + rm_value;

    state.reg(rt_index) = state.memory_interface().read32(target_address, state);
}

void LDRB_imm(State::ARMState& state, const uint32_t opcode)
{
    const uint32_t imm      = Bit::get_bits<6, 10>(opcode);
    const uint32_t rn_index = Bit::get_bits<3, 5>(opcode);
    const uint32_t rt_index = Bit::get_bits<0, 2>(opcode);

    const uint32_t rn_value = state.reg(rn_index);
    const uint32_t target_address = rn_value + imm;

    state.reg(rt_index) = state.memory_interface().read8(target_address);
}

void LDRB_reg(State::ARMState& state, const uint32_t opcode)
{
    const uint32_t rm_index = Bit::get_bits<6, 8>(opcode);
    const uint32_t rn_index = Bit::get_bits<3, 5>(opcode);
    const uint32_t rt_index = Bit::get_bits<0, 2>(opcode);

    const uint32_t rm_value = state.reg(rm_index);
    const uint32_t rn_value = state.reg(rn_index);

    const uint32_t target_address = rn_value + rm_value;

    state.reg(rt_index) = state.memory_interface().read8(target_address);
}

void LDRH_imm(State::ARMState& state, const uint32_t opcode)
{
    const uint32_t imm      = Bit::get_bits<6, 10>(opcode) << 1;
    const uint32_t rn_index = Bit::get_bits<3, 5>(opcode);
    const uint32_t rt_index = Bit::get_bits<12, 15>(opcode);

    const uint32_t rn_value = state.reg(rn_index);
    const uint32_t target_address = rn_value + imm;

    state.reg(rt_index) = state.memory_interface().read16(target_address, state);
}

void LDRH_reg(State::ARMState& state, const uint32_t opcode)
{
    const uint32_t rm_index = Bit::get_bits<6, 8>(opcode);
    const uint32_t rn_index = Bit::get_bits<3, 5>(opcode);
    const uint32_t rt_index = Bit::get_bits<0, 2>(opcode);

    const uint32_t rm_value = state.reg(rm_index);
    const uint32_t rn_value = state.reg(rn_index);

    const uint32_t target_address = rn_value + rm_value;

    state.reg(rt_index) = state.memory_interface().read16(target_address, state);
}

void LDRSB_reg(State::ARMState& state, const uint32_t opcode)
{
    const uint32_t rm_index = Bit::get_bits<6, 8>(opcode);
    const uint32_t rn_index = Bit::get_bits<3, 5>(opcode);
    const uint32_t rt_index = Bit::get_bits<0, 2>(opcode);

    const uint32_t rm_value = state.reg(rm_index);
    const uint32_t rn_value = state.reg(rn_index);

    const uint32_t target_address = rn_value + rm_value;

    state.reg(rt_index) = Bit::sign_extend<8, uint32_t>(state.memory_interface().read8(target_address));
}

void LDRSH_reg(State::ARMState& state, const uint32_t opcode)
{
    const uint32_t rm_index = Bit::get_bits<6, 8>(opcode);
    const uint32_t rn_index = Bit::get_bits<3, 5>(opcode);
    const uint32_t rt_index = Bit::get_bits<0, 2>(opcode);

    const uint32_t rm_value = state.reg(rm_index);
    const uint32_t rn_value = state.reg(rn_index);

    const uint32_t target_address = rn_value + rm_value;

    state.reg(rt_index) = Bit::sign_extend<16, uint32_t>(state.memory_interface().read16(target_address, state));
}

void STR_imm(State::ARMState& state, const uint32_t opcode)
{
    const bool is_sp_store = Bit::is_set<12>(opcode);

    uint32_t imm = 0;
    uint32_t rn_index = 0;
    uint32_t rt_index = 0;

    if (is_sp_store)
    {
        imm      = Bit::get_bits<0, 7>(opcode) << 2;
        rn_index = 13;
        rt_index = Bit::get_bits<8, 10>(opcode);
    }
    else
    {
        imm      = Bit::get_bits<6, 10>(opcode) << 2;
        rn_index = Bit::get_bits<3, 5>(opcode);
        rt_index = Bit::get_bits<0, 2>(opcode);
    }

    const uint32_t rn_value = state.reg(rn_index);
    const uint32_t rt_value = state.reg(rt_index);
    const uint32_t target_address = rn_value + imm;

    state.memory_interface().store32(target_address, rt_value, state);
}

void STR_reg(State::ARMState& state, const uint32_t opcode)
{
    const uint32_t rm_index = Bit::get_bits<6, 8>(opcode);
    const uint32_t rn_index = Bit::get_bits<3, 5>(opcode);
    const uint32_t rt_index = Bit::get_bits<0, 2>(opcode);

    const uint32_t rm_value = state.reg(rm_index);
    const uint32_t rn_value = state.reg(rn_index);
    const uint32_t rt_value = state.reg(rt_index);

    const uint32_t target_address = rn_value + rm_value;

    state.memory_interface().store32(target_address, rt_value, state);
}

void STRB_imm(State::ARMState& state, const uint32_t opcode)
{
    const uint32_t imm      = Bit::get_bits<6, 10>(opcode);
    const uint32_t rn_index = Bit::get_bits<3, 5>(opcode);
    const uint32_t rt_index = Bit::get_bits<0, 2>(opcode);

    const uint32_t rn_value = state.reg(rn_index);
    const uint32_t rt_value = state.reg(rt_index);

    const uint32_t target_address = rn_value + imm;

    state.memory_interface().store8(target_address, static_cast<uint8_t>(rt_value & 0xFF));
}

void STRB_reg(State::ARMState& state, const uint32_t opcode)
{
    const uint32_t rm_index = Bit::get_bits<6, 8>(opcode);
    const uint32_t rn_index = Bit::get_bits<3, 5>(opcode);
    const uint32_t rt_index = Bit::get_bits<0, 2>(opcode);

    const uint32_t rm_value = state.reg(rm_index);
    const uint32_t rn_value = state.reg(rn_index);
    const uint32_t rt_value = state.reg(rt_index);

    const uint32_t target_address = rn_value + rm_value;

    state.memory_interface().store8(target_address, static_cast<uint8_t>(rt_value & 0xFF));
}

void STRH_imm(State::ARMState& state, const uint32_t opcode)
{
    const uint32_t imm      = Bit::get_bits<6, 10>(opcode) << 1;
    const uint32_t rn_index = Bit::get_bits<3, 5>(opcode);
    const uint32_t rt_index = Bit::get_bits<0, 2>(opcode);

    const uint32_t rn_value = state.reg(rn_index);
    const uint32_t rt_value = state.reg(rt_index);

    const uint32_t target_address = rn_value + imm;

    const uint16_t data = static_cast<uint16_t>(rt_value & 0xFFFF);
    state.memory_interface().store16(target_address, data, state);
}

void STRH_reg(State::ARMState& state, const uint32_t opcode)
{
    const uint32_t rm_index = Bit::get_bits<6, 8>(opcode);
    const uint32_t rn_index = Bit::get_bits<3, 5>(opcode);
    const uint32_t rt_index = Bit::get_bits<0, 2>(opcode);

    const uint32_t rm_value = state.reg(rm_index);
    const uint32_t rn_value = state.reg(rn_index);
    const uint32_t rt_value = state.reg(rt_index);

    const uint32_t target_address = rn_value + rm_value;
    const uint16_t data = static_cast<uint16_t>(rt_value & 0xFFFF);

    state.memory_interface().store16(target_address, data, state);
}

void LDR_imm_sp_rel(State::ARMState& state, const uint32_t opcode)
{
    const uint32_t imm      = Bit::get_bits<0, 7>(opcode) << 2;
    const uint32_t rt_index = Bit::get_bits<8, 10>(opcode);
    const uint32_t rn_value = state.reg(13);

    const uint32_t target_address = rn_value + imm;

    state.reg(rt_index) = state.memory_interface().read32(target_address, state);
}

void STR_imm_sp_rel(State::ARMState& state, const uint32_t opcode)
{
    const uint32_t imm      = Bit::get_bits<0, 7>(opcode) << 2;
    const uint32_t rt_index = Bit::get_bits<8, 10>(opcode);

    const uint32_t rn_value = state.reg(13);
    const uint32_t rt_value = state.reg(rt_index);

    const uint32_t target_address = rn_value + imm;
    state.memory_interface().store32(target_address, rt_value, state);
}

} // namespace Thumb1
} // namespace ForeARM

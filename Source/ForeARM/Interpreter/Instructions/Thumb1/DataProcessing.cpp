#include "ForeARM/Interpreter/Instructions/Thumb1.hpp"
#include "ForeARM/Interpreter/Instructions/Helper/Arithmetic.hpp"
#include "ForeARM/Interpreter/Instructions/Helper/Branch.hpp"
#include "ForeARM/Interpreter/Instructions/Helper/Shift.hpp"
#include "ForeARM/Interpreter/State/ARMState.hpp"
#include "ForeARM/Utils/Bits.hpp"

namespace ForeARM
{
namespace Thumb1
{

void ADC_reg(State::ARMState& state, const uint32_t opcode)
{
    const uint32_t rdn_index = Bit::get_bits<0, 2>(opcode);
    const uint32_t rm_index  = Bit::get_bits<3, 5>(opcode);

    const uint32_t rm_value = state.reg(rm_index);
    const uint32_t rn_value = state.reg(rdn_index);

    const auto result = Arithmetic::add_with_carry(rm_value, rn_value, state.cpsr().c());

    state.reg(rdn_index) = result.value;

    if (State::not_in_it_block(state))
    {
        state.cpsr().n(result.value & 0x80000000);
        state.cpsr().z(result.value == 0);
        state.cpsr().c(result.carry_out);
        state.cpsr().v(result.overflow);
    }
}

void ADD_imm3(State::ARMState& state, const uint32_t opcode)
{
    const uint32_t imm3     = Bit::get_bits<6, 8>(opcode);
    const uint32_t rd_index = Bit::get_bits<0, 2>(opcode);
    const uint32_t rn_index = Bit::get_bits<3, 5>(opcode);
    const uint32_t rn_value = state.reg(rn_index);

    const auto result = Arithmetic::add_with_carry(rn_value, imm3, 0);

    state.reg(rd_index) = result.value;

    if (State::not_in_it_block(state))
    {
        state.cpsr().n(result.value & 0x80000000);
        state.cpsr().z(result.value == 0);
        state.cpsr().c(result.carry_out);
        state.cpsr().v(result.overflow);
    }
}

void ADD_imm8(State::ARMState& state, const uint32_t opcode)
{
    const uint32_t imm8      = Bit::get_bits<0, 7>(opcode);
    const uint32_t rdn_index = Bit::get_bits<8, 10>(opcode);
    const uint32_t rn_value  = state.reg(rdn_index);

    const auto result = Arithmetic::add_with_carry(rn_value, imm8, 0);

    state.reg(rdn_index) = result.value;

    if (State::not_in_it_block(state))
    {
        state.cpsr().n(result.value & 0x80000000);
        state.cpsr().z(result.value == 0);
        state.cpsr().c(result.carry_out);
        state.cpsr().v(result.overflow);
    }
}

void ADD_reg_t1(State::ARMState& state, const uint32_t opcode)
{
    const uint32_t rd_index = Bit::get_bits<0, 2>(opcode);
    const uint32_t rm_index = Bit::get_bits<6, 8>(opcode);
    const uint32_t rn_index = Bit::get_bits<3, 5>(opcode);

    const uint32_t rm_value = state.reg(rm_index);
    const uint32_t rn_value = state.reg(rn_index);

    const auto shifted_value = Shift::shift_without_carry(rm_value, Shift::ShiftType::LSL, 0, state.cpsr().c());
    const auto result = Arithmetic::add_with_carry(rn_value, shifted_value, 0);

    state.reg(rd_index) = result.value;

    if (State::not_in_it_block(state))
    {
        state.cpsr().n(result.value & 0x80000000);
        state.cpsr().z(result.value == 0);
        state.cpsr().c(result.carry_out);
        state.cpsr().v(result.overflow);
    }
}

void ADD_reg_t2(State::ARMState& state, const uint32_t opcode)
{
    const uint32_t rm_index  = Bit::get_bits<3, 6>(opcode);
    const uint32_t rdn_index = Bit::get_bit<7>(opcode) << 3 | Bit::get_bits<0, 2>(opcode);

    const uint32_t rm_value = State::get_register_value_and_offset_if_pc(state, rm_index);
    const uint32_t rn_value = State::get_register_value_and_offset_if_pc(state, rdn_index);

    const auto shifted_value = Shift::shift_without_carry(rm_value, Shift::ShiftType::LSL, 0, state.cpsr().c());
    const auto result = Arithmetic::add_with_carry(rn_value, shifted_value, 0);

    if (rdn_index == 15)
    {
        Branch::bx_write_pc(state, result.value);
    }
    else
    {
        state.reg(rdn_index) = result.value;
    }
}

void AND_reg(State::ARMState& state, const uint32_t opcode)
{
    const uint32_t rdn_index = Bit::get_bits<0, 2>(opcode);
    const uint32_t rm_index  = Bit::get_bits<3, 5>(opcode);

    const uint32_t rm_value = state.reg(rm_index);
    const uint32_t rn_value = state.reg(rdn_index);

    const auto shifted    = Shift::shift_with_carry(rm_value, Shift::ShiftType::LSL, 0, state.cpsr().c());
    const uint32_t result = rn_value & shifted.value;

    state.reg(rdn_index) = result;

    if (State::not_in_it_block(state))
    {
        state.cpsr().n(result & 0x80000000);
        state.cpsr().z(result == 0);
        state.cpsr().c(shifted.carry_out);
    }
}

void BIC_reg(State::ARMState& state, const uint32_t opcode)
{
    const uint32_t rdn_index = Bit::get_bits<0, 2>(opcode);
    const uint32_t rm_index  = Bit::get_bits<3, 5>(opcode);

    const uint32_t rm_value = state.reg(rm_index);
    const uint32_t rn_value = state.reg(rdn_index);

    const auto shifted    = Shift::shift_with_carry(rm_value, Shift::ShiftType::LSL, 0, state.cpsr().c());
    const uint32_t result = rn_value & ~shifted.value;

    state.reg(rdn_index) = result;

    if (State::not_in_it_block(state))
    {
        state.cpsr().n(result & 0x80000000);
        state.cpsr().z(result == 0);
        state.cpsr().c(shifted.carry_out);
    }
}

void CMN_reg(State::ARMState& state, const uint32_t opcode)
{
    const uint32_t rm_index = Bit::get_bits<3, 5>(opcode);
    const uint32_t rn_index = Bit::get_bits<0, 2>(opcode);

    const uint32_t rm_value = state.reg(rm_index);
    const uint32_t rn_value = state.reg(rn_index);

    const auto result = Arithmetic::add_with_carry(rn_value, rm_value, 0);

    state.cpsr().n(result.value & 0x80000000);
    state.cpsr().z(result.value == 0);
    state.cpsr().c(result.carry_out);
    state.cpsr().v(result.overflow);
}

void CMP_imm(State::ARMState& state, const uint32_t opcode)
{
    const uint32_t rn_index = Bit::get_bits<8, 10>(opcode);
    const uint32_t rn_value = state.reg(rn_index);
    const uint32_t imm      = Bit::get_bits<0, 7>(opcode);
    const auto result       = Arithmetic::add_with_carry(rn_value, ~imm, 1);

    state.cpsr().n(result.value & 0x80000000);
    state.cpsr().z(result.value == 0);
    state.cpsr().c(result.carry_out);
    state.cpsr().v(result.overflow);
}

void CMP_reg(State::ARMState& state, const uint32_t opcode)
{
    const bool is_t2_encoding = Bit::is_not_set<9>(opcode);

    uint32_t rm_index = Bit::get_bits<3, 6>(opcode);
    uint32_t rn_index = Bit::get_bits<0, 2>(opcode);

    if (is_t2_encoding)
    {
        rn_index |= Bit::get_bit<7>(opcode) << 3;
    }

    const uint32_t rm_value = state.reg(rm_index);
    const uint32_t rn_value = state.reg(rn_index);

    const auto result = Arithmetic::add_with_carry(rn_value, ~rm_value, 1);

    state.cpsr().n(result.value & 0x80000000);
    state.cpsr().z(result.value == 0);
    state.cpsr().c(result.carry_out);
    state.cpsr().v(result.overflow);
}

void EOR_reg(State::ARMState& state, const uint32_t opcode)
{
    const uint32_t rdn_index = Bit::get_bits<0, 2>(opcode);
    const uint32_t rm_index  = Bit::get_bits<3, 5>(opcode);

    const uint32_t rm_value = state.reg(rm_index);
    const uint32_t rn_value = state.reg(rdn_index);

    const auto shifted    = Shift::shift_with_carry(rm_value, Shift::ShiftType::LSL, 0, state.cpsr().c());
    const uint32_t result = rn_value ^ shifted.value;

    state.reg(rdn_index) = result;

    if (State::not_in_it_block(state))
    {
        state.cpsr().n(result & 0x80000000);
        state.cpsr().z(result == 0);
        state.cpsr().c(shifted.carry_out);
    }
}

void MOV_imm(State::ARMState& state, const uint32_t opcode)
{
    const uint32_t rd_index = Bit::get_bits<8, 10>(opcode);
    const uint32_t imm      = Bit::get_bits<0, 7>(opcode);

    state.reg(rd_index) = imm;

    if (State::not_in_it_block(state))
    {
        state.cpsr().z(imm == 0);
    }
}

void MOV_reg_t1(State::ARMState& state, const uint32_t opcode)
{
    const uint32_t rd_index = Bit::get_bits<0, 2>(opcode) | Bit::get_bit<7>(opcode) << 3;
    const uint32_t rm_index = Bit::get_bits<3, 6>(opcode);
    const uint32_t rm_value = State::get_register_value_and_offset_if_pc(state, rm_index);

    if (rd_index == 15)
    {
        Branch::bx_write_pc(state, rm_value);
    }
    else
    {
        state.reg(rd_index) = rm_value;
    }
}

void MOV_reg_t2(State::ARMState& state, const uint32_t opcode)
{
    const uint32_t rd_index = Bit::get_bits<0, 2>(opcode);
    const uint32_t rm_index = Bit::get_bits<3, 5>(opcode);
    const uint32_t rm_value = state.reg(rm_index);

    state.reg(rd_index) = rm_value;

    state.cpsr().n(rm_value & 0x80000000);
    state.cpsr().z(rm_value == 0);
}

void MUL(State::ARMState& state, const uint32_t opcode)
{
    const uint32_t rdm_index = Bit::get_bits<0, 2>(opcode);
    const uint32_t rn_index  = Bit::get_bits<3, 5>(opcode);

    const uint64_t rm_value = Bit::sign_extend<32, uint64_t>(state.reg(rdm_index));
    const uint64_t rn_value = Bit::sign_extend<32, uint64_t>(state.reg(rn_index));

    const uint32_t result = static_cast<uint32_t>(rn_value * rm_value);

    state.reg(rdm_index) = result;

    if (State::not_in_it_block(state))
    {
        state.cpsr().n(result & 0x80000000);
        state.cpsr().z(result == 0);
    }
}

void MVN_reg(State::ARMState& state, const uint32_t opcode)
{
    const uint32_t rd_index = Bit::get_bits<0, 2>(opcode);
    const uint32_t rm_index = Bit::get_bits<3, 5>(opcode);
    const uint32_t rm_value = state.reg(rm_index);

    const auto shifted    = Shift::shift_with_carry(rm_value, Shift::ShiftType::LSL, 0, state.cpsr().c());
    const uint32_t result = ~shifted.value;

    state.reg(rd_index) = result;

    if (State::not_in_it_block(state))
    {
        state.cpsr().n(result & 0x80000000);
        state.cpsr().z(result == 0);
        state.cpsr().c(shifted.carry_out);
    }
}

void ORR_reg(State::ARMState& state, const uint32_t opcode)
{
    const uint32_t rdn_index = Bit::get_bits<0, 2>(opcode);
    const uint32_t rm_index  = Bit::get_bits<3, 5>(opcode);

    const uint32_t rm_value = state.reg(rm_index);
    const uint32_t rn_value = state.reg(rdn_index);

    const auto shifted    = Shift::shift_with_carry(rm_value, Shift::ShiftType::LSL, 0, state.cpsr().c());
    const uint32_t result = rn_value | shifted.value;

    state.reg(rdn_index) = result;

    if (State::not_in_it_block(state))
    {
        state.cpsr().n(result & 0x80000000);
        state.cpsr().z(result == 0);
        state.cpsr().c(shifted.carry_out);
    }
}

void RSB_imm(State::ARMState& state, const uint32_t opcode)
{
    const uint32_t rd_index = Bit::get_bits<0, 2>(opcode);
    const uint32_t rn_index = Bit::get_bits<3, 5>(opcode);
    const uint32_t rn_value = state.reg(rn_index);

    const auto result = Arithmetic::add_with_carry(~rn_value, 0, 1);

    state.reg(rd_index) = result.value;

    if (State::not_in_it_block(state))
    {
        state.cpsr().n(result.value & 0x80000000);
        state.cpsr().z(result.value == 0);
        state.cpsr().c(result.carry_out);
        state.cpsr().v(result.overflow);
    }
}

void SBC_reg(State::ARMState& state, const uint32_t opcode)
{
    const uint32_t rdn_index = Bit::get_bits<0, 2>(opcode);
    const uint32_t rm_index  = Bit::get_bits<3, 5>(opcode);

    const uint32_t rm_value = state.reg(rm_index);
    const uint32_t rn_value = state.reg(rdn_index);

    const auto result = Arithmetic::add_with_carry(rn_value, ~rm_value, state.cpsr().c());

    state.reg(rdn_index) = result.value;

    if (State::not_in_it_block(state))
    {
        state.cpsr().n(result.value & 0x80000000);
        state.cpsr().z(result.value == 0);
        state.cpsr().c(result.carry_out);
        state.cpsr().v(result.overflow);
    }
}

void SUB_imm3(State::ARMState& state, const uint32_t opcode)
{
    const uint32_t imm3     = Bit::get_bits<6, 8>(opcode);
    const uint32_t rd_index = Bit::get_bits<0, 2>(opcode);
    const uint32_t rn_index = Bit::get_bits<3, 5>(opcode);
    const uint32_t rn_value = state.reg(rn_index);

    const auto result = Arithmetic::add_with_carry(rn_value, ~imm3, 1);

    state.reg(rd_index) = result.value;

    if (State::not_in_it_block(state))
    {
        state.cpsr().n(result.value & 0x80000000);
        state.cpsr().z(result.value == 0);
        state.cpsr().c(result.carry_out);
        state.cpsr().v(result.overflow);
    }
}

void SUB_imm8(State::ARMState& state, const uint32_t opcode)
{
    const uint32_t imm8      = Bit::get_bits<0, 7>(opcode);
    const uint32_t rdn_index = Bit::get_bits<8, 10>(opcode);
    const uint32_t rn_value  = state.reg(rdn_index);

    const auto result = Arithmetic::add_with_carry(rn_value, ~imm8, 1);

    state.reg(rdn_index) = result.value;

    if (State::not_in_it_block(state))
    {
        state.cpsr().n(result.value & 0x80000000);
        state.cpsr().z(result.value == 0);
        state.cpsr().c(result.carry_out);
        state.cpsr().v(result.overflow);
    }
}

void SUB_reg(State::ARMState& state, const uint32_t opcode)
{
    const uint32_t rd_index = Bit::get_bits<0, 2>(opcode);
    const uint32_t rm_index = Bit::get_bits<6, 8>(opcode);
    const uint32_t rn_index = Bit::get_bits<3, 5>(opcode);

    const uint32_t rm_value = state.reg(rm_index);
    const uint32_t rn_value = state.reg(rn_index);

    const auto shifted_value = Shift::shift_with_carry(rm_value, Shift::ShiftType::LSL, 0, state.cpsr().c());
    const auto result = Arithmetic::add_with_carry(rn_value, ~shifted_value.value, 1);

    state.reg(rd_index) = result.value;

    if (State::not_in_it_block(state))
    {
        state.cpsr().n(result.value & 0x80000000);
        state.cpsr().z(result.value == 0);
        state.cpsr().c(result.carry_out);
        state.cpsr().v(result.overflow);
    }
}

void TST_reg(State::ARMState& state, const uint32_t opcode)
{
    const uint32_t rm_index = Bit::get_bits<3, 5>(opcode);
    const uint32_t rn_index = Bit::get_bits<0, 2>(opcode);
    const uint32_t rm_value = state.reg(rm_index);
    const uint32_t rn_value = state.reg(rn_index);

    const auto shifted_value = Shift::shift_with_carry(rm_value, Shift::ShiftType::LSL, 0, state.cpsr().c());
    const auto result = rn_value & shifted_value.value;

    state.cpsr().n(result & 0x80000000);
    state.cpsr().z(result == 0);
    state.cpsr().c(shifted_value.carry_out);
}

} // namespace Thumb1
} // namespace ForeARM

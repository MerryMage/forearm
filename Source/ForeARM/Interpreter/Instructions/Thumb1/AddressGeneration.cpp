#include "ForeARM/Interpreter/Instructions/Thumb1.hpp"
#include "ForeARM/Interpreter/Instructions/Helper/Arithmetic.hpp"
#include "ForeARM/Interpreter/State/ARMState.hpp"
#include "ForeARM/Utils/Bits.hpp"
#include "ForeARM/Utils/Endian.hpp"

namespace ForeARM
{
namespace Thumb1
{

void ADR(State::ARMState& state, const uint32_t opcode)
{
    const uint32_t imm10    = Bit::get_bits<0, 7>(opcode) << 2;
    const uint32_t rd_index = Bit::get_bits<8, 10>(opcode);

    state.reg(rd_index) = State::get_word_aligned_program_counter(state) + imm10;
}

void ADD_sp_plus_imm(State::ARMState& state, const uint32_t opcode)
{
    const uint32_t sp_value = state.reg(13);

    uint32_t rd_index = 0;
    uint32_t imm = 0;

    // T1 encoding
    if (Bit::is_set<11>(opcode))
    {
        imm      = Bit::get_bits<0, 7>(opcode) << 2;
        rd_index = Bit::get_bits<8, 10>(opcode);
    }
    else // T2 encoding
    {
        imm      = Bit::get_bits<0, 6>(opcode) << 2;
        rd_index = 13;
    }

    const auto result = Arithmetic::add_with_carry(sp_value, imm, 0);

    state.reg(rd_index) = result.value;
}

void SUB_sp_min_imm(State::ARMState& state, const uint32_t opcode)
{
    const uint32_t imm      = Bit::get_bits<0, 6>(opcode) << 2;
    const uint32_t sp_value = state.reg(13);

    const auto result  = Arithmetic::add_with_carry(sp_value, ~imm, 1);

    state.reg(13) = result.value;
}

} // namespace Thumb1
} // namespace ForeARM

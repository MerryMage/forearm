#include "ForeARM/Interpreter/Instructions/Thumb1.hpp"
#include "ForeARM/Interpreter/State/ARMState.hpp"
#include "ForeARM/Utils/Bits.hpp"

namespace ForeARM
{
namespace Thumb1
{

void SVC(State::ARMState&, const uint32_t)
{
}

} // namespace Thumb1
} // namespace ForeARM

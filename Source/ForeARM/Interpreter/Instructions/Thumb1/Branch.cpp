#include "ForeARM/Interpreter/Instructions/ARM.hpp"
#include "ForeARM/Interpreter/Instructions/Thumb1.hpp"
#include "ForeARM/Interpreter/Instructions/Helper/Branch.hpp"
#include "ForeARM/Interpreter/State/ARMState.hpp"
#include "ForeARM/Utils/Bits.hpp"

namespace ForeARM
{
namespace Thumb1
{

void B_imm8(State::ARMState& state, const uint32_t opcode)
{
    const uint32_t pc = State::get_program_counter(state);
    const uint32_t imm32 = Bit::sign_extend<8>(Bit::get_bits<0, 7>(opcode));

    Branch::branch_write_pc(state, pc + imm32);
}

void B_imm11(State::ARMState& state, const uint32_t opcode)
{
    const uint32_t pc = State::get_program_counter(state);
    const uint32_t imm32 = Bit::sign_extend<11>(Bit::get_bits<0, 10>(opcode));

    Branch::branch_write_pc(state, pc + imm32);
}

void BLX_reg(State::ARMState& state, const uint32_t opcode)
{
    // Translate Rm field placement to the ARM instruction equivalent
    // and execute through it, since they share the same code.
    ARM::BLX_reg(state, opcode >> 3);
}

void BX(State::ARMState& state, const uint32_t opcode)
{
    // Translate Rm field placement to the ARM instruction equivalent
    // and execute through it, since they share the same code.
    ARM::BX(state, opcode >> 3);
}

void CB(State::ARMState& state, const uint32_t opcode)
{
    const bool nonzero = Bit::is_set<11>(opcode);

    const uint32_t rn_index = Bit::get_bits<0, 2>(opcode);
    const uint32_t rn_value = state.reg(rn_index);

    if (nonzero != (rn_value == 0))
    {
        const uint32_t i    = Bit::get_bit<9>(opcode);
        const uint32_t imm5 = Bit::get_bits<3, 7>(opcode);
        const uint32_t imm  = (i << 6) | (imm5 << 1);

        const uint32_t pc = State::get_program_counter(state);

        Branch::branch_write_pc(state, pc + imm);
    }
}

} // namespace Thumb1
} // namespace ForeARM

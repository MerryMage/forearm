#include "ForeARM/Interpreter/Instructions/Thumb1.hpp"
#include "ForeARM/Interpreter/Instructions/Helper/Arithmetic.hpp"
#include "ForeARM/Interpreter/State/ARMState.hpp"
#include "ForeARM/Utils/Bits.hpp"

namespace ForeARM
{
namespace Thumb1
{

void CPS(State::ARMState& state, const uint32_t opcode)
{
    if (State::current_mode_is_user(state))
        return;

    const bool affect_f = Bit::is_set<0>(opcode);
    const bool affect_i = Bit::is_set<1>(opcode);
    const bool affect_a = Bit::is_set<2>(opcode);
    const bool disable  = Bit::is_set<4>(opcode);

    uint32_t cpsr_value = state.cpsr().value();

    if (disable)
    {
        if (affect_a)
            cpsr_value = Bit::clear_bit<8>(cpsr_value);

        if (affect_i)
            cpsr_value = Bit::clear_bit<7>(cpsr_value);

        if (affect_f)
            cpsr_value = Bit::clear_bit<6>(cpsr_value);
    }
    else
    {
        if (affect_a)
            cpsr_value = Bit::set_bit<8>(cpsr_value);

        if (affect_i)
            cpsr_value = Bit::set_bit<7>(cpsr_value);

        if (affect_f)
            cpsr_value = Bit::set_bit<6>(cpsr_value);
    }

    // Write to all applicable bits
    State::write_cpsr_from_instruction(state, cpsr_value, 0b1111, false);
}

void IT(State::ARMState& state, const uint32_t opcode)
{
    // TODO: Fully implement IT.
    //       Currently it will decode and set the IT field correctly,
    //       however determining the PC, to jump to, and actual execution
    //       of instructions in an IT block are not handled properly.
    const uint32_t it_bits = Bit::get_bits<0, 7>(opcode);

    state.cpsr().it(it_bits);
}

void SETEND(State::ARMState& state, const uint32_t opcode)
{
    state.cpsr().e(Bit::get_bit<3>(opcode));
}

} // namespace Thumb1
} // namespace ForeARM

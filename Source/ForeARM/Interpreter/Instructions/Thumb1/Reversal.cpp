#include "ForeARM/Interpreter/Instructions/Thumb1.hpp"
#include "ForeARM/Interpreter/State/ARMState.hpp"
#include "ForeARM/Utils/Bits.hpp"
#include "ForeARM/Utils/Endian.hpp"

namespace ForeARM
{
namespace Thumb1
{

void REV(State::ARMState& state, const uint32_t opcode)
{
    const uint32_t rd_index = Bit::get_bits<0, 2>(opcode);
    const uint32_t rm_index = Bit::get_bits<3, 5>(opcode);

    state.reg(rd_index) = Endian::swap32(state.reg(rm_index));
}

void REV16(State::ARMState& state, const uint32_t opcode)
{
    const uint32_t rd_index = Bit::get_bits<0, 2>(opcode);
    const uint32_t rm_value = state.reg(Bit::get_bits<3, 5>(opcode));

    const uint32_t rm_low  = Endian::swap16(static_cast<uint16_t>(Bit::get_bits<0, 15>(rm_value)));
    const uint32_t rm_high = Endian::swap16(static_cast<uint16_t>(Bit::get_bits<16, 31>(rm_value)));

    state.reg(rd_index) = (rm_high << 16) | rm_low;
}

void REVSH(State::ARMState& state, const uint32_t opcode)
{
    const uint32_t rd_index = Bit::get_bits<0, 2>(opcode);
    const uint32_t rm_value = state.reg(Bit::get_bits<3, 5>(opcode));

    const uint32_t rm_low  = Bit::get_bits<0, 7>(rm_value);
    const uint32_t rm_high = Bit::get_bits<8, 15>(rm_value);

    state.reg(rd_index) = rm_high | (Bit::sign_extend<8>(rm_low) << 8);
}

} // namespace Thumb1
} // namespace ForeARM

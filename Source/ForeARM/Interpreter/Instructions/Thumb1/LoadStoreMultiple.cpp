#include <bitset>
#include "ForeARM/Interpreter/Instructions/Thumb1.hpp"
#include "ForeARM/Interpreter/Instructions/Helper/Branch.hpp"
#include "ForeARM/Interpreter/State/ARMState.hpp"
#include "ForeARM/Utils/Bits.hpp"

namespace
{

// The first 8 bits of a Thumb-1 mode LDM or STM instruction indicate
// a list of bits that signify which registers to load data into.
uint32_t num_register_list_bits_set(const uint32_t value) noexcept
{
    return static_cast<uint32_t>(std::bitset<8>(value).count());
}

// read/store_to_memory only read the initial register list field
// in order to remain usable with LDMIA and STMIA until memory alignment
// checking is implemented.
uint32_t read_memory_to_registers(ForeARM::State::ARMState& state, const uint32_t opcode, uint32_t start_address)
{
    for (size_t i = 0; i <= 7; i++)
    {
        if (Bit::is_not_set(opcode, i))
            continue;

        state.reg(i) = state.memory_interface().read32(start_address, state);
        start_address += 4;
    }

    return start_address;
}

uint32_t store_registers_to_memory(ForeARM::State::ARMState& state, const uint32_t opcode, uint32_t start_address)
{
    for (size_t i = 0; i <= 7; i++)
    {
        if (Bit::is_not_set(opcode, i))
            continue;

        state.memory_interface().store32(start_address, state.reg(i), state);
        start_address += 4;
    }

    return start_address;
}

} // Anonymous namespace

namespace ForeARM
{
namespace Thumb1
{

// AKA LDMFD
void LDMIA(State::ARMState& state, const uint32_t opcode)
{
    const uint32_t rn_index = Bit::get_bits<8, 10>(opcode);
    const uint32_t address  = state.reg(rn_index);

    read_memory_to_registers(state, opcode, address);

    // Do writeback only if the register list entry for rn isn't set.
    if (Bit::is_not_set(opcode, rn_index))
    {
        const uint32_t count = num_register_list_bits_set(opcode);
        state.reg(rn_index) += 4 * count;
    }
}

void POP(State::ARMState& state, const uint32_t opcode)
{
    const uint32_t sp_value = state.reg(13);
    const uint32_t end_address = read_memory_to_registers(state, opcode, sp_value);

    // Signifies the PC in Thumb-1
    if (Bit::is_set<8>(opcode))
    {
        const uint32_t branch_address = state.memory_interface().read32(end_address, state);
        Branch::bx_write_pc(state, branch_address);
    }

    const uint32_t pc_bit   = Bit::get_bit<8>(opcode);
    const uint32_t num_regs = num_register_list_bits_set(opcode) + pc_bit;

    state.reg(13) += 4 * num_regs;
}

void PUSH(State::ARMState& state, const uint32_t opcode)
{
    const uint32_t link_reg_bit  = Bit::get_bit<8>(opcode);
    const uint32_t num_regs_set  = num_register_list_bits_set(opcode) + link_reg_bit;
    const uint32_t subtracted_sp = state.reg(13) - 4 * num_regs_set;

    const uint32_t end_address = store_registers_to_memory(state, opcode, subtracted_sp);

    // Signifies the link register in Thumb-1
    if (Bit::is_set<8>(opcode))
        state.memory_interface().store32(end_address, state.reg(14), state);

    state.reg(13) = subtracted_sp;
}

// AKA STMEA
void STMIA(State::ARMState& state, const uint32_t opcode)
{
    const uint32_t rn_index = Bit::get_bits<8, 10>(opcode);
    const uint32_t address  = state.reg(rn_index);

    store_registers_to_memory(state, opcode, address);

    const uint32_t count = num_register_list_bits_set(opcode);
    state.reg(rn_index) += 4 * count;
}

} // namespace Thumb1
} // namespace ForeARM

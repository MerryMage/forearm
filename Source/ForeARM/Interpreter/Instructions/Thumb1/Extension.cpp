#include "ForeARM/Interpreter/Instructions/Thumb1.hpp"
#include "ForeARM/Interpreter/State/ARMState.hpp"
#include "ForeARM/Utils/Bits.hpp"

namespace ForeARM
{
namespace Thumb1
{

void SXTB(State::ARMState& state, const uint32_t opcode)
{
    const uint32_t rd_index = Bit::get_bits<0, 2>(opcode);
    const uint32_t rm_value = state.reg(Bit::get_bits<3, 5>(opcode));

    state.reg(rd_index) = Bit::sign_extend<8>(rm_value & 0xFF);
}

void SXTH(State::ARMState& state, const uint32_t opcode)
{
    const uint32_t rd_index = Bit::get_bits<0, 2>(opcode);
    const uint32_t rm_value = state.reg(Bit::get_bits<3, 5>(opcode));

    state.reg(rd_index) = Bit::sign_extend<16>(rm_value & 0xFFFF);
}

void UXTB(State::ARMState& state, const uint32_t opcode)
{
    const uint32_t rd_index = Bit::get_bits<0, 2>(opcode);
    const uint32_t rm_value = state.reg(Bit::get_bits<3, 5>(opcode));

    state.reg(rd_index) = rm_value & 0xFF;
}

void UXTH(State::ARMState& state, const uint32_t opcode)
{
    const uint32_t rd_index = Bit::get_bits<0, 2>(opcode);
    const uint32_t rm_value = state.reg(Bit::get_bits<3, 5>(opcode));

    state.reg(rd_index) = rm_value & 0xFFFF;
}

} // namespace Thumb1
} // namespace ForeARM

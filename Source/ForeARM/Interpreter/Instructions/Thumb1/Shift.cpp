#include "ForeARM/Interpreter/Instructions/Thumb1.hpp"
#include "ForeARM/Interpreter/Instructions/Helper/Branch.hpp"
#include "ForeARM/Interpreter/Instructions/Helper/Shift.hpp"
#include "ForeARM/Interpreter/State/ARMState.hpp"
#include "ForeARM/Utils/Bits.hpp"

namespace
{
using namespace ForeARM::Shift;
using namespace ForeARM::State;

// TODO: Check IT bits
void set_cpsr_if_s_bit_set(ARMState& state, const ShiftWithCarryResult& result)
{
    if (in_it_block(state))
        return;

    state.cpsr().n(result.value & 0x80000000);
    state.cpsr().z(result.value == 0);
    state.cpsr().c(result.carry_out);
}

void shift_by_immediate(ARMState& state, const uint32_t opcode, const ShiftType shift_type)
{
    const uint32_t rd_index = Bit::get_bits<0, 2>(opcode);
    const uint32_t rm_index = Bit::get_bits<3, 5>(opcode);
    const uint32_t imm5     = Bit::get_bits<6, 10>(opcode);

    const uint32_t shift_value = decode_imm_shift(static_cast<uint32_t>(shift_type), imm5).amount;
    const uint32_t rm_value = get_register_value_and_offset_if_pc(state, rm_index);

    const auto result = shift_with_carry(rm_value, shift_type, shift_value, state.cpsr().c());

    state.reg(rd_index) = result.value;
    set_cpsr_if_s_bit_set(state, result);
}

void shift_by_register(ARMState& state, const uint32_t opcode, const ShiftType shift_type)
{
    const uint32_t rdn_index = Bit::get_bits<0, 2>(opcode);
    const uint32_t rdn_value = state.reg(rdn_index);
    const uint32_t rm_value  = state.reg(Bit::get_bits<3, 5>(opcode));

    const auto result = shift_with_carry(rdn_value, shift_type, rm_value & 0xFF, state.cpsr().c());
    state.reg(rdn_index) = result.value;
    set_cpsr_if_s_bit_set(state, result);
}

} // Anonymous namespace

namespace ForeARM
{
namespace Thumb1
{

void ASR_imm(State::ARMState& state, const uint32_t opcode)
{
    shift_by_immediate(state, opcode, Shift::ShiftType::ASR);
}

void ASR_reg(State::ARMState& state, const uint32_t opcode)
{
    shift_by_register(state, opcode, Shift::ShiftType::ASR);
}

void LSL_imm(State::ARMState& state, const uint32_t opcode)
{
    shift_by_immediate(state, opcode, Shift::ShiftType::LSL);
}

void LSL_reg(State::ARMState& state, const uint32_t opcode)
{
    shift_by_register(state, opcode, Shift::ShiftType::LSL);
}

void LSR_imm(State::ARMState& state, const uint32_t opcode)
{
    shift_by_immediate(state, opcode, Shift::ShiftType::LSR);
}

void LSR_reg(State::ARMState& state, const uint32_t opcode)
{
    shift_by_register(state, opcode, Shift::ShiftType::LSR);
}

void ROR_reg(State::ARMState& state, const uint32_t opcode)
{
    shift_by_register(state, opcode, Shift::ShiftType::ROR);
}

} // namespace Thumb1
} // namespace ForeARM

#pragma once

#include <cstdint>

namespace ForeARM
{
namespace State { class ARMState; }
namespace Thumb1
{

// Address Generation
void ADR(State::ARMState&, uint32_t);
void ADD_sp_plus_imm(State::ARMState&, uint32_t);
void SUB_sp_min_imm(State::ARMState&, uint32_t);

// Branch
void B_imm8(State::ARMState&, uint32_t);
void B_imm11(State::ARMState&, uint32_t);
void BLX_reg(State::ARMState&, uint32_t);
void BX(State::ARMState&, uint32_t);
void CB(State::ARMState&, uint32_t);

// Data Processing
void ADC_reg(State::ARMState&, uint32_t);
void ADD_imm3(State::ARMState&, uint32_t);
void ADD_imm8(State::ARMState&, uint32_t);
void ADD_reg_t1(State::ARMState&, uint32_t);
void ADD_reg_t2(State::ARMState&, uint32_t);
void AND_reg(State::ARMState&, uint32_t);
void BIC_reg(State::ARMState&, uint32_t);
void CMN_reg(State::ARMState&, uint32_t);
void CMP_imm(State::ARMState&, uint32_t);
void CMP_reg(State::ARMState&, uint32_t);
void EOR_reg(State::ARMState&, uint32_t);
void MOV_imm(State::ARMState&, uint32_t);
void MOV_reg_t1(State::ARMState&, uint32_t);
void MOV_reg_t2(State::ARMState&, uint32_t);
void MUL(State::ARMState&, uint32_t);
void MVN_reg(State::ARMState&, uint32_t);
void ORR_reg(State::ARMState&, uint32_t);
void RSB_imm(State::ARMState&, uint32_t);
void SBC_reg(State::ARMState&, uint32_t);
void SUB_imm3(State::ARMState&, uint32_t);
void SUB_imm8(State::ARMState&, uint32_t);
void SUB_reg(State::ARMState&, uint32_t);
void TST_reg(State::ARMState&, uint32_t);

// Exception Generating
void SVC(State::ARMState&, uint32_t);

// Extension
void SXTB(State::ARMState&, uint32_t);
void SXTH(State::ARMState&, uint32_t);
void UXTB(State::ARMState&, uint32_t);
void UXTH(State::ARMState&, uint32_t);

// Load/Store Single
void LDR_imm(State::ARMState&, uint32_t);
void LDR_imm_sp_rel(State::ARMState&, uint32_t);
void LDR_lit(State::ARMState&, uint32_t);
void LDR_reg(State::ARMState&, uint32_t);
void LDRB_imm(State::ARMState&, uint32_t);
void LDRB_reg(State::ARMState&, uint32_t);
void LDRH_imm(State::ARMState&, uint32_t);
void LDRH_reg(State::ARMState&, uint32_t);
void LDRSB_reg(State::ARMState&, uint32_t);
void LDRSH_reg(State::ARMState&, uint32_t);
void STR_imm(State::ARMState&, uint32_t);
void STR_imm_sp_rel(State::ARMState&, uint32_t);
void STR_reg(State::ARMState&, uint32_t);
void STRB_imm(State::ARMState&, uint32_t);
void STRB_reg(State::ARMState&, uint32_t);
void STRH_imm(State::ARMState&, uint32_t);
void STRH_reg(State::ARMState&, uint32_t);

// Load/Store Multiple
void LDMIA(State::ARMState&, uint32_t);
void POP(State::ARMState&, uint32_t);
void PUSH(State::ARMState&, uint32_t);
void STMIA(State::ARMState&, uint32_t);

// Reversal
void REV(State::ARMState&, uint32_t);
void REV16(State::ARMState&, uint32_t);
void REVSH(State::ARMState&, uint32_t);

// Shift
void ASR_imm(State::ARMState&, uint32_t);
void ASR_reg(State::ARMState&, uint32_t);
void LSL_imm(State::ARMState&, uint32_t);
void LSL_reg(State::ARMState&, uint32_t);
void LSR_imm(State::ARMState&, uint32_t);
void LSR_reg(State::ARMState&, uint32_t);
void ROR_reg(State::ARMState&, uint32_t);

// Status Register Access
void CPS(State::ARMState&, uint32_t);
void IT(State::ARMState&, uint32_t);
void SETEND(State::ARMState&, uint32_t);

} // namespace Thumb1
} // namespace ForeARM

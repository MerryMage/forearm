#include "ForeARM/Interpreter/State/ARMState.hpp"
#include "ForeARM/Interpreter/State/PSR.hpp"
#include "ForeARM/Utils/Bits.hpp"

namespace
{

enum class ConditionCode
{
    EQ = 0,  // Equal
    NE = 1,  // Not Equal
    CS = 2,  // Unsigned higher or same (carry set)
    CC = 3,  // Unsigned lower (or carry clear)
    MI = 4,  // Negative
    PL = 5,  // Zero or Positive
    VS = 6,  // Signed overflow (V set)
    VC = 7,  // No signed overflow (V cleared)
    HI = 8,  // Unsigned higher
    LS = 9,  // Unsigned lower or same
    GE = 10, // Signed greater-than or equal
    LT = 11, // Signed less-than
    GT = 12, // Signed greater-than
    LE = 13, // Signed less-than or equal
    AL = 14, // Always executed
    NV = 15, // Never executed (or unconditional)
};

} // Anonymous namespace

namespace ForeARM
{
namespace State
{

// Retrieves the current condition code. Depending on certain instructions
// or execution states, where the condition field is located can differ.
//
// For example:
//   - For conditional ARM instructions, this would be bits 31:28 of the opcode.
//   - For Thumb-1 conditional B instructions, this would be bits 11:8 of the opcode.
//   - For Thumb-2 conditional B instructions, this would be bits 25:22 of the opcode.
//   - If an IT block is active, then this would be bits 7:4 of the IT state field.
static ConditionCode current_condition(const ARMState& state, const uint32_t opcode) noexcept
{
    if (state.current_instruction_set() == InstructionSet::ARM)
        return static_cast<ConditionCode>(Bit::get_bits<28, 31>(opcode));

    constexpr uint32_t thumb1_b_mask     = 0xFFFFF000;
    constexpr uint32_t thumb1_b_expected = 0x0000D000;
    if ((opcode & thumb1_b_mask) == thumb1_b_expected)
        return static_cast<ConditionCode>(Bit::get_bits<8, 11>(opcode));

    constexpr uint32_t thumb2_b_mask     = 0xF800D000;
    constexpr uint32_t thumb2_b_expected = 0xF0008000;
    if ((opcode & thumb2_b_mask) == thumb2_b_expected)
        return static_cast<ConditionCode>(Bit::get_bits<22, 25>(opcode));

    const uint32_t it_bits = state.cpsr().it();
    if (it_bits == 0)
        return ConditionCode::AL;

    const uint32_t it_mask = it_bits & 0b1111;
    if (it_mask != 0)
        return static_cast<ConditionCode>(Bit::get_bits<4, 7>(it_bits));

    return ConditionCode::AL;
}

bool predication_condition_passed(const ARMState& state, const uint32_t opcode) noexcept
{
    const ConditionCode condition_bits = current_condition(state, opcode);

    const bool n_flag = state.cpsr().n() != 0;
    const bool z_flag = state.cpsr().z() != 0;
    const bool c_flag = state.cpsr().c() != 0;
    const bool v_flag = state.cpsr().v() != 0;

    switch (condition_bits)
    {
    case ConditionCode::EQ:
        return z_flag;
    case ConditionCode::NE:
        return !z_flag;
    case ConditionCode::CS:
        return c_flag;
    case ConditionCode::CC:
        return !c_flag;
    case ConditionCode::MI:
        return n_flag;
    case ConditionCode::PL:
        return !n_flag;
    case ConditionCode::VS:
        return v_flag;
    case ConditionCode::VC:
        return !v_flag;
    case ConditionCode::HI:
        return c_flag && !z_flag;
    case ConditionCode::LS:
        return !c_flag || z_flag;
    case ConditionCode::GE:
        return n_flag == v_flag;
    case ConditionCode::LT:
        return n_flag != v_flag;
    case ConditionCode::GT:
        return !z_flag && (n_flag == v_flag);
    case ConditionCode::LE:
        return z_flag || (n_flag != v_flag);
    case ConditionCode::AL:
    case ConditionCode::NV:
        break;
    }

    return true;
}

bool is_valid_mode(const uint32_t value) noexcept
{
    const auto mode = static_cast<PSR::Mode>(value);

    switch (mode)
    {
    case PSR::Mode::User:
    case PSR::Mode::FIQ:
    case PSR::Mode::IRQ:
    case PSR::Mode::Supervisor:
    case PSR::Mode::Monitor:
    case PSR::Mode::Abort:
    case PSR::Mode::Hypervisor:
    case PSR::Mode::Undefined:
    case PSR::Mode::System:
        return true;
    }

    return false;
}

bool current_mode_is_user(const ARMState& state) noexcept
{
    return state.cpsr().mode() == PSR::Mode::User;
}

bool current_mode_is_user_or_system(const ARMState& state) noexcept
{
    return current_mode_is_user(state) ||
           state.cpsr().mode() == PSR::Mode::System;
}

bool current_mode_is_privileged(const ARMState& state) noexcept
{
    return !current_mode_is_user(state);
}

void write_cpsr_from_instruction(ARMState& state, const uint32_t value, const uint32_t mask, const bool is_exception_return) noexcept
{
    const bool privileged = current_mode_is_privileged(state);
    uint32_t new_value = state.cpsr().value();

    if (Bit::is_set<3>(mask))
    {
        // NZCVQ flags
        new_value = (new_value & ~0xF8000000U) | (value & 0xF8000000U);

        // IT<1:0> and J bits
        if (is_exception_return)
            new_value = (new_value & ~0x07000000U) | (value & 0x07000000U);
    }

    // GE bits
    if (Bit::is_set<2>(mask))
        new_value = (new_value & ~0x000F0000U) | (value & 0x000F0000U);

    if (Bit::is_set<1>(mask))
    {
        // IT<7:2> bits
        if (is_exception_return)
            new_value = (new_value & ~0x0000FC00U) | (value & 0x0000FC00U);

        // E bit
        new_value = (new_value & ~0x00000200U) | (value & 0x00000200U);

        // A interrupt bit
        // TODO: Add additional checks here as specified in the
        //       ARMv7 reference manual when the facilities
        //       to check for them get implemented.
        if (privileged)
            new_value = (new_value & ~0x00000100U) | (value & 0x00000100U);
    }

    if (Bit::is_set<0>(mask))
    {
        if (privileged)
        {
            // I interrupt bit
            new_value = (new_value & ~0x00000080U) | (value & 0x00000080U);

            // F interrupt bit
            // TODO: Add additional checks here as specified in the
            //       ARMv7 reference manual when the facilities
            //       to check for them get implemented.
            new_value = (new_value & ~0x00000040U) | (value & 0x00000040U);

            // Mode bits
            // TODO: Ditto about the above TODO.
            new_value = (new_value & ~0x0000001FU) | (value & 0x0000001FU);
        }

        // T bit
        if (is_exception_return)
            new_value = (new_value & ~0x00000020U) | (value & 0x00000020U);
    }

    state.cpsr() = new_value;
}

void write_spsr_from_instruction(ARMState& state, const uint32_t value, const uint32_t mask) noexcept
{
    uint32_t new_value = state.spsr().value();

    if (Bit::is_set<3>(mask))
        new_value = (new_value & ~0xFF000000U) | (value & 0xFF000000U);

    if (Bit::is_set<2>(mask))
        new_value = (new_value & ~0x000F0000U) | (value & 0x000F0000U);

    if (Bit::is_set<1>(mask))
        new_value = (new_value & ~0x0000FF00U) | (value & 0x0000FF00U);

    if (Bit::is_set<0>(mask))
    {
        new_value = (new_value & ~0x000000E0U) | (value & 0x000000E0U);

        if (is_valid_mode(new_value & 0x1FU))
            new_value = (new_value & ~0x1FU) | (value & 0x1FU);
    }

    state.spsr() = new_value;
}

void it_advance(ARMState& state) noexcept
{
    const uint32_t it_bits = state.cpsr().it();
    const uint32_t first_three_bits = it_bits & 0b111;

    if (first_three_bits == 0)
    {
        state.cpsr().it(0);
    }
    else
    {
        const uint32_t mask_bits    = it_bits & 0x1F;
        const uint32_t shifted_mask = (mask_bits << 1) & 0x1F;
        const uint32_t modified_it  = Bit::clear_and_set_bits<0, 4>(it_bits, shifted_mask);

        state.cpsr().it(modified_it);
    }
}

bool in_it_block(const ARMState& state) noexcept
{
    const uint32_t bits = Bit::get_bits<0, 3>(state.cpsr().it());

    return bits != 0;
}

bool not_in_it_block(const ARMState& state) noexcept
{
    return !in_it_block(state);
}

bool last_in_it_block(const ARMState& state) noexcept
{
    const uint32_t bits = Bit::get_bits<0, 3>(state.cpsr().it());

    return bits == 0b1000;
}

} // namespace State
} // namespace ForeARM

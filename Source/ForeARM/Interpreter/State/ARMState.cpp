#include <cassert>
#include <cstdint>
#include "ForeARM/Interpreter/State/ARMState.hpp"
#include "ForeARM/Interpreter/State/MemoryInterface/DummyMemoryInterface.hpp"
#include "ForeARM/Interpreter/State/MemoryInterface/MemoryInterface.hpp"

namespace ForeARM
{
namespace State
{

ARMState::ARMState()
    : m_memory_interface{std::make_unique<DummyMemoryInterface>()}
{
}

ARMState::ARMState(std::unique_ptr<MemoryInterface> memory_interface)
    : m_memory_interface{std::move(memory_interface)}
{
}

uint32_t& ARMState::reg(const size_t index)
{
    return m_registers[index];
}

const uint32_t& ARMState::reg(const size_t index) const
{
    return m_registers[index];
}

PSR& ARMState::cpsr() noexcept
{
    return m_cpsr;
}

const PSR& ARMState::cpsr() const noexcept
{
    return m_cpsr;
}

PSR& ARMState::spsr() noexcept
{
    return m_spsr;
}

const PSR& ARMState::spsr() const noexcept
{
    return m_spsr;
}

CoprocessorManager& ARMState::coprocessor_manager() noexcept
{
    return m_coprocessor_manager;
}

const CoprocessorManager& ARMState::coprocessor_manager() const noexcept
{
    return m_coprocessor_manager;
}

MemoryInterfaceProxy& ARMState::memory_interface() noexcept
{
    return m_memory_interface;
}

const MemoryInterfaceProxy& ARMState::memory_interface() const noexcept
{
    return m_memory_interface;
}

InstructionSet ARMState::current_instruction_set() const noexcept
{
    // Represents bits 5 and 24 of the CPSR.
    constexpr uint32_t mask = 0x1000020;

    switch (m_cpsr.value() & mask)
    {
    case 0x00000000:
        return InstructionSet::ARM;
    case 0x00000020:
        return InstructionSet::Thumb;
    case 0x01000000:
        return InstructionSet::Jazelle;
    case 0x01000020:
        return InstructionSet::ThumbEE;
    }

    // Should never happen.
    assert(false);
    return InstructionSet::ARM;
}

void ARMState::set_memory_interface(std::unique_ptr<MemoryInterface> memory_interface)
{
    m_memory_interface = MemoryInterfaceProxy{std::move(memory_interface)};
}

void ARMState::select_instruction_set(const InstructionSet instruction_set) noexcept
{
    switch (instruction_set)
    {
    case InstructionSet::ARM:
        m_cpsr.j(0);
        m_cpsr.t(0);
        break;
    case InstructionSet::Jazelle:
        m_cpsr.j(1);
        m_cpsr.t(0);
        break;
    case InstructionSet::Thumb:
        m_cpsr.j(0);
        m_cpsr.t(1);
        break;
    case InstructionSet::ThumbEE:
        m_cpsr.j(1);
        m_cpsr.t(1);
        break;
    }
}

void ARMState::select_processor_mode(const PSR::Mode target_mode) noexcept
{
    const auto current_mode = m_cpsr.mode();

    // Don't bother with anything if we're switching into the same mode.
    if (target_mode == current_mode)
        return;

    enum : uint32_t
    {
        BANK_HYPERVISOR = 0,
        BANK_SUPERVISOR = 1,
        BANK_ABORT      = 2,
        BANK_UNDEFINED  = 3,
        BANK_MONITOR    = 4,
        BANK_IRQ        = 5,
        BANK_FIQ        = 6
    };

    // Save registers
    switch (current_mode)
    {
    // System mode shares registers with user mode.
    case PSR::Mode::System:
    case PSR::Mode::User:
        std::copy(m_registers.begin() + 13, m_registers.end() - 1, m_banked_user.begin());
        break;
    case PSR::Mode::Hypervisor:
        m_banked_hypervisor_sp = m_registers[13];
        m_banked_spsrs[BANK_HYPERVISOR] = m_spsr;
        break;
    case PSR::Mode::Supervisor:
        std::copy(m_registers.begin() + 13, m_registers.end() - 1, m_banked_supervisor.begin());
        m_banked_spsrs[BANK_SUPERVISOR] = m_spsr;
        break;
    case PSR::Mode::Abort:
        std::copy(m_registers.begin() + 13, m_registers.end() - 1, m_banked_abort.begin());
        m_banked_spsrs[BANK_ABORT] = m_spsr;
        break;
    case PSR::Mode::Undefined:
        std::copy(m_registers.begin() + 13, m_registers.end() - 1, m_banked_undefined.begin());
        m_banked_spsrs[BANK_UNDEFINED] = m_spsr;
        break;
    case PSR::Mode::Monitor:
        std::copy(m_registers.begin() + 13, m_registers.end() - 1, m_banked_monitor.begin());
        m_banked_spsrs[BANK_MONITOR] = m_spsr;
        break;
    case PSR::Mode::IRQ:
        std::copy(m_registers.begin() + 13, m_registers.end() - 1, m_banked_irq.begin());
        m_banked_spsrs[BANK_IRQ] = m_spsr;
        break;
    case PSR::Mode::FIQ:
        std::copy(m_registers.begin() + 8, m_registers.end() - 1, m_banked_fiq.begin());
        m_banked_spsrs[BANK_FIQ] = m_spsr;
        break;
    }

    // Load target mode registers
    switch (target_mode)
    {
    case PSR::Mode::System:
    case PSR::Mode::User:
        std::copy(m_banked_user.begin(), m_banked_user.end(), m_registers.begin() + 13);
        break;
    case PSR::Mode::Hypervisor:
        m_registers[13] = m_banked_hypervisor_sp;
        m_spsr = m_banked_spsrs[BANK_HYPERVISOR];
        break;
    case PSR::Mode::Supervisor:
        std::copy(m_banked_supervisor.begin(), m_banked_supervisor.end(), m_registers.begin() + 13);
        m_spsr = m_banked_spsrs[BANK_SUPERVISOR];
        break;
    case PSR::Mode::Abort:
        std::copy(m_banked_abort.begin(), m_banked_abort.end(), m_registers.begin() + 13);
        m_spsr = m_banked_spsrs[BANK_ABORT];
        break;
    case PSR::Mode::Undefined:
        std::copy(m_banked_undefined.begin(), m_banked_undefined.end(), m_registers.begin() + 13);
        m_spsr = m_banked_spsrs[BANK_UNDEFINED];
        break;
    case PSR::Mode::Monitor:
        std::copy(m_banked_monitor.begin(), m_banked_monitor.end(), m_registers.begin() + 13);
        m_spsr = m_banked_spsrs[BANK_MONITOR];
        break;
    case PSR::Mode::IRQ:
        std::copy(m_banked_irq.begin(), m_banked_irq.end(), m_registers.begin() + 13);
        m_spsr = m_banked_spsrs[BANK_IRQ];
        break;
    case PSR::Mode::FIQ:
        std::copy(m_banked_fiq.begin(), m_banked_fiq.end(), m_registers.begin() + 8);
        m_spsr = m_banked_spsrs[BANK_FIQ];
        break;
    }

    // Set the mode bits
    m_cpsr.mode(static_cast<uint32_t>(target_mode));
}

void ARMState::increase_program_counter(const uint32_t opcode) noexcept
{
    // Thumb-2 instruction bits 11 to 15 must be one of
    // the following to be identified as a Thumb-2 instruction:
    // 0b11101, 0b11110, 0b11111.
    const bool is_arm_or_thumb2_instruction = m_cpsr.t() == 0 || (opcode & 0xF8000000) >= 0xE8000000;

    if (is_arm_or_thumb2_instruction)
        m_registers[15] += 4;
    else // Thumb-1
        m_registers[15] += 2;
}

uint32_t program_counter_prefetch_offset(const ARMState& state) noexcept
{
    return state.cpsr().t() ? 4 : 8;
}

uint32_t get_program_counter(const ARMState& state) noexcept
{
    return state.reg(15) + program_counter_prefetch_offset(state);
}

uint32_t get_word_aligned_program_counter(const ARMState& state) noexcept
{
    const uint32_t pc = get_program_counter(state);

    if (state.current_instruction_set() == InstructionSet::ARM)
        return pc & 0xFFFFFFFC;

    // Thumb
    return pc & 0xFFFFFFFE;
}

uint32_t get_register_value_and_offset_if_pc(const ARMState& state, const uint32_t register_index) noexcept
{
    if (register_index == 15)
        return get_program_counter(state);

    return state.reg(register_index);
}

} // namespace State
} // namespace ForeARM

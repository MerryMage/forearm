#pragma once

#include <cstdint>
#include "ForeARM/Utils/Bits.hpp"

namespace ForeARM
{
namespace State
{

class ARMState;

/**
 * Program Status Register
 *
 * | Bit(s)  | Description                                   |
 * |:-------:|:----------------------------------------------|
 * | N       | Negative                                      |
 * | Z       | Zero                                          |
 * | C       | Carry                                         |
 * | V       | Overflow                                      |
 * | Q       | Sticky overflow for DSP-oriented instructions |
 * | IT[1:0] | Lower two bits of the If-Then execution state |
 * | J       | Jazelle                                       |
 * | GE      | Greater-than or Equal                         |
 * | IT[7:2] | Upper six bits of the If-Then execution state |
 * | E       | Endian (0 is little endian, 1 is big endian)  |
 * | A       | Imprecise data abort (disables them when set) |
 * | I       | IRQ interrupts (disables them when set)       |
 * | F       | FIQ interrupts (disables them when set)       |
 * | T       | Thumb bit                                     |
 * | Mode    | Current processor mode                        |
 */
class PSR final
{
public:
    /// Possible modes a PSR can indicate.
    enum class Mode : uint32_t
    {
        /// Normal execution mode. This is the only unprivileged mode.
        User       = 0b10000,

        /// High-speed data transfer/channel processing.
        /// @note An IRQ can be interrupted by a FIQ,
        /// but a FIQ cannot be interrupted by an IRQ.
        FIQ        = 0b10001,

        /// Used for general-purpose interrupt handling.
        IRQ        = 0b10010,

        /// Protected mode for an operating system
        /// and software interrupts.
        Supervisor = 0b10011,

        /// Mode in which a secure monitor call exception is taken.
        Monitor    = 0b10110,

        /// Virtual memory and/or memory protection.
        /// Entered whenever the memory system signals
        /// an about (either prefetch aborts, or data aborts).
        Abort      = 0b10111,

        /// Non-secure PL2 mode implemented as part of
        /// the virtualization extensions
        Hypervisor = 0b11010,

        /// Entered whenever an undefined instruction is encountered.
        /// Normally ARM will wait a little bit upon encountering a
        /// undefined instruction and see if a coprocessor can handle
        /// it. If no coprocessors can handle it, then this mode is entered.
        Undefined  = 0b11011,

        /// Offers the privileges of Supervisor mode, but shares
        /// registers with User mode.
        System     = 0b11111,
    };

    constexpr PSR() = default;
    constexpr explicit PSR(const uint32_t value) noexcept : m_value{value & mask} {}

    PSR& operator=(const uint32_t value) noexcept
    {
        m_value = value & mask;
        return *this;
    }

    constexpr uint32_t value() const noexcept { return m_value; }

    constexpr uint32_t n()    const noexcept { return Bit::get_bit<31>(m_value); }
    constexpr uint32_t z()    const noexcept { return Bit::get_bit<30>(m_value); }
    constexpr uint32_t c()    const noexcept { return Bit::get_bit<29>(m_value); }
    constexpr uint32_t v()    const noexcept { return Bit::get_bit<28>(m_value); }
    constexpr uint32_t q()    const noexcept { return Bit::get_bit<27>(m_value); }
    constexpr uint32_t j()    const noexcept { return Bit::get_bit<24>(m_value); }
    constexpr uint32_t ge()   const noexcept { return Bit::get_bits<16, 19>(m_value); }
    constexpr uint32_t e()    const noexcept { return Bit::get_bit<9>(m_value); }
    constexpr uint32_t a()    const noexcept { return Bit::get_bit<8>(m_value); }
    constexpr uint32_t i()    const noexcept { return Bit::get_bit<7>(m_value); }
    constexpr uint32_t f()    const noexcept { return Bit::get_bit<6>(m_value); }
    constexpr uint32_t t()    const noexcept { return Bit::get_bit<5>(m_value); }
    constexpr Mode     mode() const noexcept { return static_cast<Mode>(Bit::get_bits<0, 4>(m_value)); }
    constexpr uint32_t it()   const noexcept
    {
        return Bit::get_bits<10, 15>(m_value) << 2 | Bit::get_bits<25, 26>(m_value);
    }

    void n(const uint32_t value)    noexcept { m_value = Bit::clear_and_set_bit<31>(m_value, value); }
    void z(const uint32_t value)    noexcept { m_value = Bit::clear_and_set_bit<30>(m_value, value); }
    void c(const uint32_t value)    noexcept { m_value = Bit::clear_and_set_bit<29>(m_value, value); }
    void v(const uint32_t value)    noexcept { m_value = Bit::clear_and_set_bit<28>(m_value, value); }
    void q(const uint32_t value)    noexcept { m_value = Bit::clear_and_set_bit<27>(m_value, value); }
    void j(const uint32_t value)    noexcept { m_value = Bit::clear_and_set_bit<24>(m_value, value); }
    void ge(const uint32_t value)   noexcept { m_value = Bit::clear_and_set_bits<16, 19>(m_value, value); }
    void e(const uint32_t value)    noexcept { m_value = Bit::clear_and_set_bit<9>(m_value, value); }
    void a(const uint32_t value)    noexcept { m_value = Bit::clear_and_set_bit<8>(m_value, value); }
    void i(const uint32_t value)    noexcept { m_value = Bit::clear_and_set_bit<7>(m_value, value); }
    void f(const uint32_t value)    noexcept { m_value = Bit::clear_and_set_bit<6>(m_value, value); }
    void t(const uint32_t value)    noexcept { m_value = Bit::clear_and_set_bit<5>(m_value, value); }
    void mode(const uint32_t value) noexcept { m_value = Bit::clear_and_set_bits<0, 4>(m_value, value); }
    void it(const uint32_t value)   noexcept
    {
        const uint32_t first_two_bits = Bit::get_bits<0, 1>(value);
        const uint32_t last_five_bits = Bit::get_bits<2, 7>(value);

        m_value = Bit::clear_and_set_bits<25, 26>(m_value, first_two_bits);
        m_value = Bit::clear_and_set_bits<10, 15>(m_value, last_five_bits);
    }

private:
    // Bits 20 - 23 are reserved.
    static constexpr uint32_t mask = 0xFF0FFFFF;
    uint32_t m_value = 0;
};

// Indicates if an opcode's condition field passes based upon the given ARMState's configuration
bool predication_condition_passed(const ARMState& state, uint32_t opcode) noexcept;

// Indicates if the given value corresponds to an valid CPU mode in the PSR.
bool is_valid_mode(uint32_t value) noexcept;

// Whether or not the current CPU mode is user.
bool current_mode_is_user(const ARMState& state) noexcept;

// Whether or not the current CPU mode is user or system.
bool current_mode_is_user_or_system(const ARMState& state) noexcept;

// Whether or not the current CPU mode is privileged.
// If the current mode of the CPU is not user, then it is privileged.
bool current_mode_is_privileged(const ARMState& state) noexcept;

// Contains the behavior for a CPSR write from an ARM instruction (e.g. MSR)
void write_cpsr_from_instruction(ARMState& state, uint32_t value, uint32_t mask, bool is_exception_return) noexcept;

// Contains the behavior for a SPSR write from an ARM instruction (e.g. MSR)
void write_spsr_from_instruction(ARMState& state, uint32_t value, uint32_t mask) noexcept;

// Advances the IT state bits after an instruction within an IT block is executed.
// This shifts bits 0-4 of the IT state bits left by one: moving the next bit into
// place to form the least significant bit of the next instruction's condition code.
void it_advance(ARMState& state) noexcept;

// Indicates whether or not the current instruction is within an IT block.
bool in_it_block(const ARMState& state) noexcept;

// Indicates whether or not the current instruction is not within an IT block.
bool not_in_it_block(const ARMState& state) noexcept;

// Indicates whether or not the current instruction is the last instruction within an IT block.
bool last_in_it_block(const ARMState& state) noexcept;
	
} // namespace State
} // namespace ForeARM

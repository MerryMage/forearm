#pragma once

#include <cstdint>
#include "ForeARM/Interpreter/State/MemoryInterface/MemoryInterface.hpp"

namespace ForeARM
{
namespace State
{

/**
 * MemoryInterface implementation that provides stubbed load/store
 * functionality. Useful for unit-testing and using arithmetic
 * instructions only.
 */
class DummyMemoryInterface final : public MemoryInterface
{
public:
    uint8_t  read8(uint32_t)  const noexcept override;
    uint16_t read16(uint32_t) const noexcept override;
    uint32_t read32(uint32_t) const noexcept override;

    void store8(uint32_t, uint8_t)   noexcept override;
    void store16(uint32_t, uint16_t) noexcept override;
    void store32(uint32_t, uint32_t) noexcept override;
};

} // namespace State
} // namespace ForeARM

#pragma once

#include <cstdint>

namespace ForeARM
{
namespace State
{

/**
 * Interface that can be used to provide a custom
 * means of interacting with memory that should be
 * available to the emulated ARM CPU.
 */
class MemoryInterface
{
public:
    virtual ~MemoryInterface();

    // Prevent copies, as it doesn't particularly make sense to allow.
    MemoryInterface(const MemoryInterface&) = delete;
    MemoryInterface& operator=(const MemoryInterface&) = delete;

    // Reads a value from memory at the given address.
    virtual uint8_t  read8(uint32_t)  const = 0;
    virtual uint16_t read16(uint32_t) const = 0;
    virtual uint32_t read32(uint32_t) const = 0;

    // Stores a value to memory at the given address.
    virtual void store8(uint32_t, uint8_t) = 0;
    virtual void store16(uint32_t, uint16_t) = 0;
    virtual void store32(uint32_t, uint32_t) = 0;

protected:
    MemoryInterface();
};

} // namespace State
} // namespace ForeARM

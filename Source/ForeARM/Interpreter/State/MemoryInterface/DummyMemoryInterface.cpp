#include <cstdint>
#include "ForeARM/Interpreter/State/MemoryInterface/DummyMemoryInterface.hpp"

namespace ForeARM
{
namespace State
{

uint8_t DummyMemoryInterface::read8(uint32_t) const noexcept
{
    return 0;
}

uint16_t DummyMemoryInterface::read16(uint32_t) const noexcept
{
    return 0;
}

uint32_t DummyMemoryInterface::read32(uint32_t) const noexcept
{
    return 0;
}

void DummyMemoryInterface::store8(uint32_t, uint8_t) noexcept
{
}

void DummyMemoryInterface::store16(uint32_t, uint16_t) noexcept
{
}

void DummyMemoryInterface::store32(uint32_t, uint32_t) noexcept
{
}

} // namespace State
} // namespace ForeARM

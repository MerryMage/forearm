#pragma once

#include <cstdint>
#include <memory>

namespace ForeARM
{
namespace State
{

class ARMState;
class MemoryInterface;

/**
 * Proxy class that mimics a MemoryInterface but provides
 * additional functionality, such as endianness swapping.
 *
 * The reasoning for not including this in the MemoryInterface
 * is because endianness swapping is a functionality inherent
 * to the ARM CPU itself - it's an internal emulated behavior.
 *
 * Exposing the requirement for the interfacing code to handle
 * this behavior would be an unnecessary and unwanted addition
 * to the public interface. All it's there for is to bridge
 * necessary memory accesses to the CPU, that's it. Nothing more.
 */
class MemoryInterfaceProxy final
{
public:
    MemoryInterfaceProxy();
    MemoryInterfaceProxy(MemoryInterfaceProxy&) = delete;
    MemoryInterfaceProxy(MemoryInterfaceProxy&&) = default;
    explicit MemoryInterfaceProxy(std::unique_ptr<MemoryInterface> memory_interface);
    ~MemoryInterfaceProxy();

    MemoryInterfaceProxy& operator=(MemoryInterfaceProxy&) = delete;
    MemoryInterfaceProxy& operator=(MemoryInterfaceProxy&&) = default;

    // Reads a value from memory at the given address.
    uint8_t  read8(uint32_t  address) const;
    uint16_t read16(uint32_t address, const ARMState& state) const;
    uint32_t read32(uint32_t address, const ARMState& state) const;

    // Stores a value to memory at the given address.
    void store8(uint32_t  address, uint8_t  data);
    void store16(uint32_t address, uint16_t data, const ARMState& state);
    void store32(uint32_t address, uint32_t data, const ARMState& state);

private:
    std::unique_ptr<MemoryInterface> m_memory_interface;
};

} // namespace State
} // namespace ForeARM

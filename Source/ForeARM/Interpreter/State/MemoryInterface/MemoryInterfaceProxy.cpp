#include <cstdint>
#include <memory>

#include "ForeARM/Interpreter/State/MemoryInterface/MemoryInterface.hpp"
#include "ForeARM/Interpreter/State/MemoryInterface/MemoryInterfaceProxy.hpp"
#include "ForeARM/Interpreter/State/ARMState.hpp"
#include "ForeARM/Utils/Endian.hpp"

namespace ForeARM
{
namespace State
{

MemoryInterfaceProxy::MemoryInterfaceProxy()
{
}

MemoryInterfaceProxy::MemoryInterfaceProxy(std::unique_ptr<MemoryInterface> memory_interface)
    : m_memory_interface{std::move(memory_interface)}
{
}

MemoryInterfaceProxy::~MemoryInterfaceProxy()
{
}

uint8_t MemoryInterfaceProxy::read8(const uint32_t address) const
{
    return m_memory_interface->read8(address);
}

uint16_t MemoryInterfaceProxy::read16(const uint32_t address, const ARMState& state) const
{
    uint16_t data = m_memory_interface->read16(address);

    if (state.cpsr().e() == 1)
        data = Endian::swap16(data);

    return data;
}

uint32_t MemoryInterfaceProxy::read32(const uint32_t address, const ARMState& state) const
{
    uint32_t data = m_memory_interface->read32(address);

    if (state.cpsr().e() == 1)
        data = Endian::swap32(data);

    return data;
}

void MemoryInterfaceProxy::store8(const uint32_t address, const uint8_t data)
{
    m_memory_interface->store8(address, data);
}

void MemoryInterfaceProxy::store16(const uint32_t address, uint16_t data, const ARMState& state)
{
    if (state.cpsr().e() == 1)
        data = Endian::swap16(data);

    m_memory_interface->store16(address, data);
}

void MemoryInterfaceProxy::store32(const uint32_t address, uint32_t data, const ARMState& state)
{
    if (state.cpsr().e() == 1)
        data = Endian::swap32(data);

    m_memory_interface->store32(address, data);
}

} // namespace State
} // namespace ForeARM

#pragma once

#include <array>
#include <cstdint>
#include <memory>

#include "ForeARM/Interpreter/State/MemoryInterface/MemoryInterfaceProxy.hpp"
#include "ForeARM/Interpreter/State/PSR.hpp"
#include "ForeARM/Interpreter/State/Coprocessor/CoprocessorManager.hpp"

namespace ForeARM
{
namespace State
{

class MemoryInterface;

/// Possible instruction sets the emulated CPU may use.
enum class InstructionSet
{
    ARM,
    Jazelle,
    Thumb,
    ThumbEE
};

/// Contains the operational state of the emulated ARM CPU.
class ARMState final
{
public:
    /**
     * Constructor.
     *
     * @note This constructor initializes the underlying memory interface as
     *       a DummyMemoryInterface instance.
     */
    ARMState();

    ARMState(ARMState&&) = default;
    ARMState& operator=(ARMState&&) = default;

    ARMState(const ARMState&) = delete;
    ARMState& operator=(const ARMState&) = delete;

    /**
     * Constructor
     *
     * @param memory_interface Bridge to memory used with the interpreter
     *                         this will be used by load/store instructions
     *                         and by the interpreter for reading instruction
     *                         data in general.
     */
    ARMState(std::unique_ptr<MemoryInterface> memory_interface);

    uint32_t& reg(size_t index);
    const uint32_t& reg(size_t index) const;

    PSR& cpsr() noexcept;
    const PSR& cpsr() const noexcept;

    PSR& spsr() noexcept;
    const PSR& spsr() const noexcept;

    CoprocessorManager& coprocessor_manager() noexcept;
    const CoprocessorManager& coprocessor_manager() const noexcept;

    MemoryInterfaceProxy& memory_interface() noexcept;
    const MemoryInterfaceProxy& memory_interface() const noexcept;

    // Gets the current instruction set that this state is in.
    InstructionSet current_instruction_set() const noexcept;

    // Changes the underlying memory interface to use the supplied instance.
    void set_memory_interface(std::unique_ptr<MemoryInterface> memory_interface);

    // Changes the current emulated processor mode.
    void select_processor_mode(PSR::Mode mode) noexcept;

    // Changes the current executing instruction set.
    void select_instruction_set(InstructionSet instruction_set) noexcept;

    // Increases the program counter based on the current instruction set mode.
    void increase_program_counter(uint32_t opcode) noexcept;

private:
    // Current mode general purpose registers.
    std::array<uint32_t, 16> m_registers{};

    // Banked registers
    std::array<uint32_t, 2>  m_banked_abort{};
    std::array<uint32_t, 7>  m_banked_fiq{};
    std::array<uint32_t, 2>  m_banked_irq{};
    std::array<uint32_t, 2>  m_banked_monitor{};
    std::array<uint32_t, 2>  m_banked_supervisor{};
    std::array<uint32_t, 2>  m_banked_undefined{};
    std::array<uint32_t, 2>  m_banked_user{};
    uint32_t                 m_banked_hypervisor_sp{};

    // Current program status register.
    PSR m_cpsr;

    // Currently active saved program status register.
    PSR m_spsr;

    // Banked saved program status registers.
    std::array<PSR, 7> m_banked_spsrs;

    // Access handler for coprocessors.
    CoprocessorManager m_coprocessor_manager{this};

    // Bridges accesses to external memory.
    MemoryInterfaceProxy m_memory_interface;

    // TODO: Expand this
};

/**
 * The amount of bytes ahead the program counter is with regards to prefetching.
 *
 * @param state The @ref ARMState to use as a context
 *
 * @return
 *         - If the CPU is in ARM mode, this returns 8.
 *         - If the CPU is in Thumb mode, this returns 4.
 */
uint32_t program_counter_prefetch_offset(const ARMState& state) noexcept;

/**
 * Gets the program counter with its prefetch offset added to it.
 *
 * @param state An @ref ARMState to use as a context.
 *
 * @return The program counter value with the prefetch offset added.
 */
uint32_t get_program_counter(const ARMState& state) noexcept;

/**
 * Gets the program counter aligned on a word boundary.
 *
 * @param state An @ref ARMState instance to use as a context.
 *
 * @return Word-aligned value within the program counter.
 *
 * @note This also adds the prefetch offset to the returned value.
 */
uint32_t get_word_aligned_program_counter(const ARMState& state) noexcept;

/**
 * Accesses a register and applies a prefetch offset to it
 * if the provided index signifies the program counter.
 *
 * @param state          An @ref ARMState instance to use for register access.
 * @param register_index The register to access
 *
 * @return
 *         - If `register_index` is within 0-14:
 *            - The value stored in the register.
 *         - If `register_index` is 15:
 *            - The value stored in the program counter plus 8 (if in ARM mode)
 *              or plus 4 (if in Thumb mode).
 *
 * @note This is used for instructions that allow the program counter to be specified as an operand.
 */
uint32_t get_register_value_and_offset_if_pc(const ARMState& state, uint32_t register_index) noexcept;

} // namespace State
} // namespace ForeARM

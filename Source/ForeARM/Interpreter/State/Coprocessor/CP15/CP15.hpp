#pragma once

#include <cstdint>
#include <functional>
#include <memory>
#include <string>
#include <unordered_map>

#include "ForeARM/Interpreter/State/Coprocessor/Coprocessor.hpp"
#include "ForeARM/Interpreter/State/Coprocessor/CP15/CP15Register.hpp"

namespace ForeARM
{
namespace State
{

class ARMState;

/**
 * Represents the CP15 System Control Coprocessor
 */
class CP15 final : public Coprocessor
{
public:
    /**
     * Constructor
     *
     * @note This constructor does not set an ARMState
     * instance to observe. This is useful when doing
     * two-part construction.
     */
    CP15();

    /**
     * Constructor
     *
     * @param state_context ARMState instance that this CP15 instance acts with/upon.
     */
    explicit CP15(ARMState* state_context);

    bool can_handle_instruction(uint32_t opcode) const override;

    uint32_t read32(uint32_t opcode) const override;
    uint64_t read64(uint32_t opcode) const override;

    void write32(uint32_t opcode, uint32_t value) override;
    void write64(uint32_t opcode, uint64_t value) override;

    void perform_operation(uint32_t opcode) override;

    void reset() noexcept override;

    /**
     * Inserts a CP15Register32 instance into this CP15 instance.
     *
     * @param crn           Coprocessor register - first operand
     * @param opcode1       Coprocessor-specific opcode in the range 0 to 7.
     * @param crm           Coprocessor register - second operand
     * @param opcode2       Coprocessor-specific opcode in the range 0 to 7.
     * @param cp15_register The register to insert.
     */
    void insert_register(uint32_t crn, uint32_t opcode1, uint32_t crm, uint32_t opcode2, std::unique_ptr<CP15Register32> cp15_register);

    /**
     * Inserts a CP15Register64 instance into this CP15 instance.
     *
     * @param opcode1       Coprocessor-specific opcode in the range 0 to 15.
     * @param crm           Coprocessor register.
     * @param cp15_register The register to insert.
     */
    void insert_register(uint32_t opcode1, uint32_t crm, std::unique_ptr<CP15Register64> cp15_register);

private:
    /**
     * Indicates whether or not the given information constitutes a valid 32-bit coprocessor register access.
     *
     * @param crn     Coprocessor register that contains the first operand.
     * @param opcode1 Coprocessor-specific opcode in the range 0 to 7.
     * @param crm     Additional source or destination coprocessor register.
     * @param opcode2 Coprocessor-specific opcode in the range 0 to 7.
     *
     * @return true if the register is valid, false otherwise.
     */
    bool is_valid_32bit_register(uint32_t crn, uint32_t opcode1, uint32_t crm, uint32_t opcode2) const;

    /**
     * Indicates whether or not the given information constitutes a valid 64-bit coprocessor register access.
     *
     * @param opcode1 Coprocessor-specific opcode in the range 0-15
     * @param crm     The coprocessor register to access.
     */
    bool is_valid_64bit_register(uint32_t opcode1, uint32_t crm);

    // Coprocessor registers and opcodes.
    using CRm     = uint32_t;
    using CRn     = uint32_t;
    using Opcode1 = uint32_t;
    using Opcode2 = uint32_t;

    // Defines the map used for storing all 32-bit registers.
    using CP15Map32 = std::unordered_map<CRn,
                      std::unordered_map<Opcode1,
                      std::unordered_map<CRm,
                      std::unordered_map<Opcode2,
                      std::unique_ptr<CP15Register32>>>>>;

    // Defines the map used for storing all 64-bit registers.
    using CP15Map64 = std::unordered_map<Opcode1,
                      std::unordered_map<CRm,
                      std::unique_ptr<CP15Register64>>>;

    CP15Map32 m_32bit_registers;
    CP15Map64 m_64bit_registers;
    ARMState* m_state_context = nullptr;
};

} // namespace State
} // namespace ForeARM

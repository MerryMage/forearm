#pragma once

#include <cstdint>
#include <string>

namespace ForeARM
{
namespace State
{

class ARMState;

/**
 * Represents a single register within the CP15 coprocessor.
 */
template <typename T>
class CP15Register
{
public:
    /// Indicates the access type for this register.
    enum class AccessType
    {
        ReadOnly,
        WriteOnly,
        ReadWrite
    };

    /// Indicates the privilege level required to access this register.
    enum class PrivilegeLevel
    {
        PL0, ///< Unprivileged
        PL1, ///< Privileged
        PL2  ///< Hypervisor
    };

    /// Indicates if this register contains bits that can be
    /// banked/shadowed depending on the current CPU mode.
    enum class Banked
    {
        No,
        Yes
    };

    /// Default constructor
    CP15Register() = default;

    /**
     * Constructor
     *
     * @param name            Name of this register.
     * @param access_type     Describes the allowed operations on this register.
     * @param privilege_level Privilege level required to perform operation on this register.
     * @param banked          Whether or not this register is banked.
     * @param reset_value     The reset value of this register.
     */
    CP15Register(std::string name,
                 AccessType access_type,
                 PrivilegeLevel privilege_level,
                 Banked banked,
                 T reset_value) noexcept
        : m_name{std::move(name)}
        , m_value{reset_value}
        , m_access_type{access_type}
        , m_privilege_level{privilege_level}
        , m_banked{banked}
        , m_reset_value{reset_value}
    {
    }

    // Necessary to be defined explictly as implicit generation of
    // these in C++11 and newer standards is considered deprecated
    // if a user-defined destructor is present.
    CP15Register(const CP15Register&) = default;
    CP15Register& operator=(const CP15Register&) = default;

    CP15Register(CP15Register&&) = default;
    CP15Register& operator=(CP15Register&&) = default;

    /// Virtual destructor
    virtual ~CP15Register() = default;

    /**
     * Called when a store is performed on this register.
     *
     * This allows for handling write-only register behaviors.
     *
     * @note The ARMState context passed in is the context the
     *       CP15 coprocessor is currently using. This can be
     *       accessed to perform any necessary modifications to CPU
     *       state as a result of a store to this register.
     */
    virtual void perform_action(ARMState&)
    {
    }

    /**
     * Assigns a new value to this register.
     *
     * @param value The new value to store.
     *
     * @note This can be used by inheriting classes to
     *       enforce bits that should be read-only.
     */
    virtual void value(T value) noexcept
    {
        m_value = value;
    }

    /// Resets this register to its defined reset value
    void reset() noexcept
    {
        m_value = m_reset_value;
    }

    const std::string& name()        const noexcept { return m_name;            }
    AccessType access_type()         const noexcept { return m_access_type;     }
    PrivilegeLevel privilege_level() const noexcept { return m_privilege_level; }
    Banked banked()                  const noexcept { return m_banked;          }
    T value()                        const noexcept { return m_value;           }

protected:
    T m_value{};

private:
    std::string    m_name;
    AccessType     m_access_type     = AccessType::ReadWrite;
    PrivilegeLevel m_privilege_level = PrivilegeLevel::PL0;
    Banked         m_banked          = Banked::No;
    T              m_reset_value{};
};

/// 32-bit CP15 register.
using CP15Register32 = CP15Register<uint32_t>;
/// 64-bit CP15 register.
using CP15Register64 = CP15Register<uint64_t>;

} // namespace State
} // namespace ForeARM

#include <cstdint>
#include <string>
#include "ForeARM/Interpreter/State/ARMState.hpp"
#include "ForeARM/Interpreter/State/Coprocessor/CP15/CP15.hpp"
#include "ForeARM/Interpreter/State/Coprocessor/CP15/CP15Register.hpp"
#include "ForeARM/Utils/Bits.hpp"

namespace
{
using namespace ForeARM::State;

//
// TODO: Invalid reads and writes should signal an undefined instruction exception
//       Currently they are simply stubbed, as I haven't implemented the
//       exception vector handling yet.
//

template <typename T>
bool can_read_register(const CP15Register<T>& cp15_register) noexcept
{
    return cp15_register.access_type() == CP15Register<T>::AccessType::ReadOnly ||
    cp15_register.access_type() == CP15Register<T>::AccessType::ReadWrite;
}

template <typename T>
bool can_write_register(const CP15Register<T>& cp15_register) noexcept
{
    return cp15_register.access_type() == CP15Register<T>::AccessType::WriteOnly ||
    cp15_register.access_type() == CP15Register<T>::AccessType::ReadWrite;
}

} // Anonymous namespace

namespace ForeARM
{
namespace State
{

CP15::CP15()
{
}

CP15::CP15(ARMState* state_context)
    : Coprocessor{state_context}
{
}

bool CP15::can_handle_instruction(const uint32_t opcode) const
{
    const bool is_read  = Bit::is_set<20>(opcode);
    const bool is_32bit = Bit::is_set<25>(opcode);

    const auto is_valid_read_or_write = [is_read](const auto& reg) {
        return (is_read && can_read_register(reg)) ||
               (!is_read && can_write_register(reg));
    };

    if (is_32bit)
    {
        const uint32_t crm  = Bit::get_bits<0, 3>(opcode);
        const uint32_t crn  = Bit::get_bits<16, 19>(opcode);
        const uint32_t opc1 = Bit::get_bits<21, 23>(opcode);
        const uint32_t opc2 = Bit::get_bits<5, 7>(opcode);

        if (!is_valid_32bit_register(crn, opc1, crm, opc2))
            return false;

        const CP15Register32& reg = *m_32bit_registers.at(crn).at(opc1).at(crm).at(opc2);
        if (is_valid_read_or_write(reg))
            return true;
    }
    else // 64-bit
    {
        const uint32_t crm  = Bit::get_bits<0, 3>(opcode);
        const uint32_t opc1 = Bit::get_bits<4, 7>(opcode);

        const CP15Register64& reg = *m_64bit_registers.at(opc1).at(crm);
        if (is_valid_read_or_write(reg))
            return true;
    }

    return false;
}

uint32_t CP15::read32(const uint32_t opcode) const
{
    const uint32_t crm  = Bit::get_bits<0, 3>(opcode);
    const uint32_t crn  = Bit::get_bits<16, 19>(opcode);
    const uint32_t opc1 = Bit::get_bits<21, 23>(opcode);
    const uint32_t opc2 = Bit::get_bits<5, 7>(opcode);

    return m_32bit_registers.at(crn).at(opc1).at(crm).at(opc2)->value();
}

uint64_t CP15::read64(const uint32_t opcode) const
{
    const uint32_t crm  = Bit::get_bits<0, 3>(opcode);
    const uint32_t opc1 = Bit::get_bits<4, 7>(opcode);

    return m_64bit_registers.at(opc1).at(crm)->value();
}

void CP15::write32(const uint32_t opcode, const uint32_t value)
{
    const uint32_t crm      = Bit::get_bits<0, 3>(opcode);
    const uint32_t crn      = Bit::get_bits<16, 19>(opcode);
    const uint32_t opc1     = Bit::get_bits<21, 23>(opcode);
    const uint32_t opc2     = Bit::get_bits<5, 7>(opcode);

    CP15Register32* const reg = m_32bit_registers[crn][opc1][crm][opc2].get();

    reg->value(value);
    reg->perform_action(*m_state_context);
}

void CP15::write64(const uint32_t opcode, const uint64_t value)
{
    const uint32_t crm  = Bit::get_bits<0, 3>(opcode);
    const uint32_t opc1 = Bit::get_bits<4, 7>(opcode);

    CP15Register64* const reg = m_64bit_registers[opc1][crm].get();

    reg->value(value);
    reg->perform_action(*m_state_context);
}

void CP15::perform_operation(uint32_t)
{
    // TODO: This should trigger an undefined instruction
    //       exception as the CP15 doesn't support using
    //       CDP. This is here as a reminder to do this
    //       whenver I implement exception vectors.
}

void CP15::reset() noexcept
{
    for (auto& crn_entry : m_32bit_registers)
    {
        for (auto& opcode1_entry : crn_entry.second)
        {
            for (auto& crm_entry : opcode1_entry.second)
            {
                for (auto& opcode2_entry : crm_entry.second)
                    opcode2_entry.second.reset();
            }
        }
    }

    for (auto& opcode1_entry : m_64bit_registers)
    {
        for (auto& crm_entry : opcode1_entry.second)
        {
            crm_entry.second.reset();
        }
    }
}

void CP15::insert_register(const uint32_t crn, const uint32_t opcode1, const uint32_t crm, const uint32_t opcode2, std::unique_ptr<CP15Register32> cp15_register)
{
    m_32bit_registers[crn][opcode1][crm][opcode2] = std::move(cp15_register);
}

void CP15::insert_register(const uint32_t opcode1, const uint32_t crm, std::unique_ptr<CP15Register64> cp15_register)
{
    m_64bit_registers[opcode1][crm] = std::move(cp15_register);
}

bool CP15::is_valid_32bit_register(const uint32_t crn, const uint32_t opcode1, const uint32_t crm, const uint32_t opcode2) const
{
    const auto crn_iter = m_32bit_registers.find(crn);
    if (crn_iter == m_32bit_registers.end())
        return false;

    const auto opcode1_iter = crn_iter->second.find(opcode1);
    if (opcode1_iter == crn_iter->second.end())
        return false;

    const auto crm_iter = opcode1_iter->second.find(crm);
    if (crm_iter == opcode1_iter->second.end())
        return false;

    const auto opcode2_iter = crm_iter->second.find(opcode2);
    return opcode2_iter != crm_iter->second.end();
}

bool CP15::is_valid_64bit_register(uint32_t opcode1, uint32_t crm)
{
    const auto opcode1_iter = m_64bit_registers.find(opcode1);
    if (opcode1_iter == m_64bit_registers.end())
        return false;

    const auto crm_iter = opcode1_iter->second.find(crm);
    return crm_iter != opcode1_iter->second.end();
}

} // namespace State
} // namespace ForeARM

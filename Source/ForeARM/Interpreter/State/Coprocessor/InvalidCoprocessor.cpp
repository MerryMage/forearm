#include <cstdint>

#include "ForeARM/Interpreter/State/Coprocessor/InvalidCoprocessor.hpp"

namespace ForeARM
{
namespace State
{

InvalidCoprocessor::InvalidCoprocessor() noexcept
{
}

InvalidCoprocessor::InvalidCoprocessor(ARMState* state) noexcept
    : Coprocessor{state}
{
}

bool InvalidCoprocessor::can_handle_instruction(uint32_t) const noexcept
{
    return false;
}

uint32_t InvalidCoprocessor::read32(uint32_t) const
{
    return 0;
}

uint64_t InvalidCoprocessor::read64(uint32_t) const
{
    return 0;
}

void InvalidCoprocessor::write32(uint32_t, uint32_t)
{
}

void InvalidCoprocessor::write64(uint32_t, uint64_t)
{
}

void InvalidCoprocessor::perform_operation(uint32_t)
{
}

void InvalidCoprocessor::reset() noexcept
{
}

} // namespace State
} // namespace ForeARM

#include <algorithm>
#include <cstdint>
#include <memory>

#include "ForeARM/Interpreter/State/ARMState.hpp"
#include "ForeARM/Interpreter/State/Coprocessor/Coprocessor.hpp"
#include "ForeARM/Interpreter/State/Coprocessor/CoprocessorManager.hpp"
#include "ForeARM/Interpreter/State/Coprocessor/InvalidCoprocessor.hpp"

namespace ForeARM
{
namespace State
{

CoprocessorManager::CoprocessorManager(ARMState* state)
    : m_state_context{state}
{
    // Initially treat all coprocessers as unimplemented.
    std::generate(m_coprocessors.begin(), m_coprocessors.end(), [&state]() {
        return std::make_unique<InvalidCoprocessor>(state);
    });
}

CoprocessorManager::~CoprocessorManager()
{
}

bool CoprocessorManager::can_handle_instruction(const uint32_t coprocessor_index, const uint32_t opcode) const
{
    return m_coprocessors.at(coprocessor_index)->can_handle_instruction(opcode);
}

uint32_t CoprocessorManager::read32(const uint32_t coprocessor_index, const uint32_t opcode) const
{
    return m_coprocessors.at(coprocessor_index)->read32(opcode);
}

uint64_t CoprocessorManager::read64(const uint32_t coprocessor_index, const uint32_t opcode) const
{
    return m_coprocessors.at(coprocessor_index)->read64(opcode);
}

void CoprocessorManager::write32(const uint32_t coprocessor_index, const uint32_t opcode, const uint32_t value)
{
    m_coprocessors.at(coprocessor_index)->write32(opcode, value);
}

void CoprocessorManager::write64(const uint32_t coprocessor_index, const uint32_t opcode, const uint64_t value)
{
    m_coprocessors.at(coprocessor_index)->write64(opcode, value);
}

void CoprocessorManager::perform_operation(const uint32_t coprocessor_index, const uint32_t opcode)
{
    m_coprocessors.at(coprocessor_index)->perform_operation(opcode);
}

bool CoprocessorManager::done_loading(const uint32_t coprocessor_index, const uint32_t opcode) const
{
    return m_coprocessors.at(coprocessor_index)->done_loading(opcode);
}

bool CoprocessorManager::done_storing(const uint32_t coprocessor_index, const uint32_t opcode) const
{
    return m_coprocessors.at(coprocessor_index)->done_storing(opcode);
}

void CoprocessorManager::reset() noexcept
{
    for (auto& coprocessor : m_coprocessors)
        coprocessor->reset();
}

void CoprocessorManager::change_state_context(ARMState* state) noexcept
{
    for (auto& coprocessor : m_coprocessors)
        coprocessor->change_state_context(state);
}

void CoprocessorManager::attach_coprocessor(const uint32_t coprocessor_index, std::unique_ptr<Coprocessor> coprocessor)
{
    if (coprocessor == nullptr)
        coprocessor = std::make_unique<InvalidCoprocessor>();

    coprocessor->change_state_context(m_state_context);

    m_coprocessors.at(coprocessor_index) = std::move(coprocessor);
}

std::unique_ptr<Coprocessor> CoprocessorManager::detach_coprocessor(const uint32_t coprocessor_index)
{
    std::unique_ptr<Coprocessor> coprocessor = std::make_unique<InvalidCoprocessor>(m_state_context);
    m_coprocessors.at(coprocessor_index).swap(coprocessor);
    coprocessor->change_state_context(nullptr);
    return coprocessor;
}

} // namespace State
} // namespace ForeARM

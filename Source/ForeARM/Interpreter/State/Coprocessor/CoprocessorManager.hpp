#pragma once

#include <array>
#include <cstdint>
#include <memory>
#include "ForeARM/Interpreter/State/Coprocessor/Coprocessor.hpp"

namespace ForeARM
{
namespace State
{

class ARMState;

/**
 * A manager that abstracts away handling code for any
 * coprocessors that an emulated ARM system supports.
 *
 * The initial state of all coprocessors on instantation of this class,
 * as far as the interpreter sees, is considered to be 'unimplemented'.
 * Any attempt to read or write to the coprocessors in this state
 * will trigger an undefined instruction exception.
 *
 * To define a coprocessor, one would need to subclass the
 * @ref Coprocessor class and implement the relevant virtual
 * member functions. Then, it can simply be attached to the
 * manager by using @ref attach_coprocessor().
 *
 * @todo Handle the undefined exception triggering.
 *       Currently this is unimplemented, as exception vectors
 *       need to be implemented. However, that can only be done
 *       once the coprocessor interface is set up, as it requires
 *       use of coprocessor 15 to determine whether or not the
 *       exceptions trap to a high or low address.
 *
 * @note Any mention of the word 'opcode' in further documentation
 *       of the manager refers to any potentially valid
 *       CDP, LDC, MCR, MRC, MCRR, MRRC, or STC instruction opcode.
 */
class CoprocessorManager final
{
public:
    /**
     * Constructor that sets a state context for this
     * manager to observe and act on.
     *
     * @param[in] state The ARMState instance to use.
     */
    explicit CoprocessorManager(ARMState* state);
    ~CoprocessorManager();

    CoprocessorManager(CoprocessorManager&&) = default;
    CoprocessorManager& operator=(CoprocessorManager&&) = default;

    CoprocessorManager(const CoprocessorManager&) = delete;
    CoprocessorManager& operator=(CoprocessorManager&) = delete;

    /**
     * Checks whether or not the indicated @ref Coprocessor is capable
     * of handling the given instruction opcode.
     *
     * @param coprocessor_index The coprocessor to check.
     * @param opcode            The opcode to check.
     *
     * @pre The coprocessor index must be within 0-15.
     *
     * @return True if the coprocessor can handle the instruction,
     *         false otherwise.
     */
    bool can_handle_instruction(uint32_t coprocessor_index, uint32_t opcode) const;

    /**
     * Reads a 32-bit word from a @ref Coprocessor indicated by an index
     * provided it can successfully execute the given opcode.
     *
     * @param coprocessor_index The coprocessor to try and access.
     * @param opcode            Instruction opcode for the coprocessor to decode.
     *
     * @pre The coprocessor index must be within 0-15.
     *
     * @return The read 32-bit value from the coprocessor.
     */
    uint32_t read32(uint32_t coprocessor_index, uint32_t opcode) const;

    /**
     * Reads a 64-bit word from a @ref Coprocessor indicated by an index,
     * provided it can successfully execute the given opcode.
     *
     * @param coprocessor_index The coprocessor to try and access.
     * @param opcode            Instruction opcode for the coprocessor to decode.
     *
     * @pre The coprocessor index must be within 0-15.
     *
     * @return The read 64-bit value from the coprocessor.
     */
    uint64_t read64(uint32_t coprocessor_index, uint32_t opcode) const;

    /**
     * Writes a 32-bit word from a @ref Coprocessor indicated by an index
     * provided it can successfully execute the given opcode.
     *
     * @param coprocessor_index The coprocessor to try and access.
     * @param opcode            Instruction opcode for the coprocessor to decode.
     * @param value             The value to store to the coprocessor.
     *
     * @pre The coprocessor index must be within 0-15.
     */
    void write32(uint32_t coprocessor_index, uint32_t opcode, uint32_t value);

    /**
     * Writes a 64-bit word from a @ref Coprocessor indicated by an index
     * provided there's a coprocessor available that can decode
     * the given opcode.
     *
     * @param coprocessor_index The coprocessor to try and access.
     * @param opcode            Instruction opcode for the coprocessor to decode.
     * @param value             The value to store to the coprocesosr.
     *
     * @pre The coprocessor index must be within 0-15.
     */
    void write64(uint32_t coprocessor_index, uint32_t opcode, uint64_t value);

    /**
     * Signals that a @ref Coprocessor should perform some sort of operation
     * indicated by the opcode passed to it.
     *
     * @param coprocessor_index The coprocessor to perform the operation
     * @param opcode            The opcode that indicates the operation to perform.
     *
     * @pre The coprocessor index must be within 0-15.
     *
     * @note This is used to facilitate the operation of CDP instructions.
     */
    void perform_operation(uint32_t coprocessor_index, uint32_t opcode);

    /**
     * Checks whether or not a @ref Coprocessor has finished executing an
     * LDC instruction's loading routine.
     *
     * @param coprocessor_index The coprocessor to check.
     * @param opcode            The LDC opcode associated with the loading routine.
     *
     * @pre The coprocessor index must be within 0-15.
     *
     * @return True if the loading routine has finished, false otherwise.
     */
    bool done_loading(uint32_t coprocessor_index, uint32_t opcode) const;

    /**
     * Checks whether or not a @ref Coprocessor has finished executing an
     * STC instruction's storage routine.
     *
     * @param coprocessor_index The coprocessor to check.
     * @param opcode            The STC opcode associated with the storage routine.
     *
     * @pre The coprocessor index must be within 0-15.
     *
     * @return True if the storage routine has finished, false otherwise.
     */
    bool done_storing(uint32_t coprocessor_index, uint32_t opcode) const;

    /**
     * Resets all contained coprocessors to their initial state.
     */
    void reset() noexcept;

    /**
     * Changes the ARMState context for all coprocessors.
     *
     * @param[in] state The new state context to use.
     */
    void change_state_context(ARMState* state) noexcept;

    /**
     * Sets an entry in this coprocessor manager to handle the given @ref Coprocessor.
     *
     * @param coprocessor_index The index to store the coprocessor at.
     * @param coprocessor       The coprocessor instance to handle.
     *
     * @pre The coprocessor index must be within 0-15.
     *
     * @note Passing in nullptr will be functionally equivalent to allocating
     *       an InvalidCoprocessor instance.
     */
    void attach_coprocessor(uint32_t coprocessor_index, std::unique_ptr<Coprocessor> coprocessor);

    /**
     * Releases ownership of a @ref Coprocessor at the given index.
     *
     * @param coprocessor_index The index of the coprocessor to release ownership of.
     *
     * @pre The coprocessor index must be within 0-15.
     *
     * @returns A unique_ptr containing the formerly managed coprocessor instance.
     *
     * @note When a coprocessor is detached, it is replaced with an @ref InvalidCoprocessor instance internally.
     * @note A detached coprocessor has no valid underlying state.
     *       Ensure it is reattached before perfoming any operations
     *       that rely on the existence of a valid state instance.
     */
    std::unique_ptr<Coprocessor> detach_coprocessor(uint32_t coprocessor_index);

private:
    // Tracked state context that is passed to attached coprocessors.
    // Note that this is not an owning pointer.
    ARMState* m_state_context = nullptr;

    // Note that coprocessors 8 to 15 are reserved by ARM.
    // Coprocessor 10 and 11 support floating-point operations together (VFP and Advanced SIMD).
    // Coprocessor 14 supports debug registers, ThumbEE, and Jazelle.
    // Coprocessor 15 provides system control functionality.
    std::array<std::unique_ptr<Coprocessor>, 16> m_coprocessors{};
};

} // namespace State
} // namespace ForeARM

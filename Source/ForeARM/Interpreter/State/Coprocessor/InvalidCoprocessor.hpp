#pragma once

#include <cstdint>
#include "ForeARM/Interpreter/State/Coprocessor/Coprocessor.hpp"

namespace ForeARM
{
namespace State
{

class ARMState;

/**
 * Defines an invalid coprocessor.
 *
 * This can be used to indicate that a given coprocessor
 * entry is supported within the ARM CPU, but has no valid
 * coprocessor residing at said entry.
 *
 * For example, it's perfectly valid for an MRC/MCR, MCRR/MRRC, LDC/STC,
 * or CDP instruction to access coprocessor 0, even if the implementation
 * doesn't include a corresponding coprocessor in hardware. It'll
 * simply result in an undefined instruction exception.
 *
 * This class can be used to model such behavior.
 *
 * @todo This class should cause undefined instruction exceptions
 *       upon any load or store attempt. This will need to be done
 *       after exception vector support is implemented
 *       (which relies on basic coprocessor support first,
 *       as configuration bits for exception vectors lay
 *       within coprocessor 15).
 */
class InvalidCoprocessor final : public Coprocessor
{
public:
    InvalidCoprocessor() noexcept;
    explicit InvalidCoprocessor(ARMState* state) noexcept;

    bool can_handle_instruction(uint32_t opcode) const noexcept override;

    uint32_t read32(uint32_t opcode) const override;
    uint64_t read64(uint32_t opcode) const override;

    void write32(uint32_t opcode, uint32_t value) override;
    void write64(uint32_t opcode, uint64_t value) override;

    void perform_operation(uint32_t opcode) override;

    void reset() noexcept override;
};

} // namespace State
} // namespace ForeARM

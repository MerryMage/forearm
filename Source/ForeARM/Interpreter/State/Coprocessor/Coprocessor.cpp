#include "ForeARM/Interpreter/State/ARMState.hpp"
#include "ForeARM/Interpreter/State/Coprocessor/Coprocessor.hpp"

namespace ForeARM
{
namespace State
{

Coprocessor::Coprocessor() noexcept
{
}

Coprocessor::Coprocessor(ARMState* state) noexcept
    : m_state_context{state}
{
}

Coprocessor::~Coprocessor() noexcept
{
}

void Coprocessor::change_state_context(ARMState* state) noexcept
{
    m_state_context = state;
}

bool Coprocessor::done_loading(uint32_t) const noexcept
{
    return true;
}

bool Coprocessor::done_storing(uint32_t) const noexcept
{
    return true;
}

} // namespace State
} // namespace ForeARM

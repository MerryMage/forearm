#pragma once

#include <cstdint>

namespace ForeARM
{
namespace State
{

class ARMState;

/**
 * Interface that defines what is necessary to implement
 * a coprocessor that can be used with the ARM core.
 */
class Coprocessor
{
public:
    /// Virtual destructor.
    virtual ~Coprocessor() noexcept;

    /**
     * Changes the ARMState context for this coprocessor.
     *
     * @param[in] state The new ARMState instance to use as a context.
     */
    void change_state_context(ARMState* state) noexcept;

    /**
     * Determines whether or not this coprocessor can
     * handle a given opcode.
     *
     * @param opcode The opcode to check against the coprocessor.
     * @return       True if the opcode can be handled, false otherwise.
     */
    virtual bool can_handle_instruction(uint32_t opcode) const = 0;

    /**
     * Attempts to read a 32-bit value, indicated by the opcode,
     * from this coprocessor.
     *
     * @param  opcode The opcode for this coprocessor to attempt to decode.
     * @return The read coprocessor value.
     */
    virtual uint32_t read32(uint32_t opcode) const = 0;

    /**
     * Attempts to read a 64-bit value, indicated by the opcode,
     * from this coprocessor.
     *
     * @param  opcode The opcode for this coprocessor to attempt to decode.
     * @return The read coprocessor value.
     */
    virtual uint64_t read64(uint32_t opcode) const = 0;

    /**
     * Attempts to store a 32-bit value, indicated by the opcode,
     * to this coprocessor from a state-managed variable.
     *
     * @param opcode The opcode for this coprocessor to attempt to decode.
     * @param value  The value to store to this coprocessor.
     */
    virtual void write32(uint32_t opcode, uint32_t value) = 0;

    /**
     * Attempts to store a 64-bit value, indicated by the opcode,
     * to this coprocessor from a state-managed variable.
     *
     * @param opcode The opcode for this coprocessor to attempt to decode.
     * @param value  The value to write to this coprocessor.
     */
    virtual void write64(uint32_t opcode, uint64_t value) = 0;

    /**
     * Performs a given internal operation that is determined
     * through the use of the provided opcode.
     *
     * @param opcode An instruction opcode that signifies some form of
     *               operation for the coprocessor to perform.
     *
     * @note This is used to facilitate the operation of CDP instructions.
     */
    virtual void perform_operation(uint32_t opcode) = 0;

    /**
     * Resets all registers within the coprocessor to their
     * defined reset value.
     */
    virtual void reset() noexcept = 0;

    /**
     * Checks whether or not an LDC instruction has finished
     * loading all required data for an operation.
     *
     * @note This must provide valid output if a coprocessor is able
     *       to handle LDC instructions, as this will act as the
     *       terminator for the loading routine, since the main CPU
     *       has no way of knowing how large the loading operation
     *       should be.
     */
    virtual bool done_loading(uint32_t opcode) const noexcept;

    /**
     * Checks whether or not an STC instruction has finished
     * storing all required data for an operation.
     *
     * @note This must provide valid output if a coprocessor is able
     *       to handle STC instructions, as this will act as the
     *       terminator for the storing routine since the main CPU
     *       has no way of knowing how large the store operation
     *       should be.
     */
    virtual bool done_storing(uint32_t opcode) const noexcept;

protected:
    /// Default constructor
    Coprocessor() noexcept;

    /**
     * Constructor that initializes a coprocessor,
     * and uses the given state as a context handle.
     *
     * @param[in] state The ARMState instance to use as a context.
     */
    explicit Coprocessor(ARMState* state) noexcept;

    /**
     * The tracked state context for this coprocessor.
     *
     * A coprocessor may utilize this to query configuration
     * bits or inspect state in order to determine if a specific
     * behavior must be performed. State may also be modified if
     * necessary.
     *
     * @note This is not an owning pointer.
     */
    ARMState* m_state_context = nullptr;
};

} // namespace State
} // namespace ForeARM

#include <algorithm>
#include <array>
#include <cstdint>

#include "ForeARM/Interpreter/Decoding/Decode.hpp"
#include "ForeARM/Interpreter/Instructions/ARM.hpp"
#include "ForeARM/Interpreter/Instructions/Thumb1.hpp"
#include "ForeARM/Interpreter/State/ARMState.hpp"

namespace
{
using namespace ForeARM;

// Internal representation of instructions
struct Instruction final
{
    const char* name;
    const uint32_t bit_mask;
    const uint32_t expected_value;
    const DecodedInstruction::InstructionFunction function;
};

template <typename T, size_t N>
const Instruction* decode_implementation(const std::array<T, N>& instruction_table, const uint32_t opcode)
{
    return std::find_if(instruction_table.cbegin(), instruction_table.cend(), [opcode](const auto& instruction) {
        return (opcode & instruction.bit_mask) == instruction.expected_value;
    });
}

//
// A table that consists of structs with a name (for debugging purposes),
// a mask, an expected value, and the function to execute if there is a match.
//
// An opcode is given to the decoding function and then it
// goes down the instruction list bitwise ANDing the mask onto
// the opcode value until the result of the ANDing matches
// the expected value.
//
// Some instructions have multiple variants and are indicated as such:
//    - A1     : ARM encoding 1
//    - A2     : ARM encoding 2
//    - banked : Banked Registers
//    - exc ret: Exception Return
//    - imm    : Immediate
//    - lit    : Literal
//    - reg    : Register
//    - rsr    : Register-shifted Register
//    - system : Privileged variant
//    - usr reg: User Registers
//
constexpr std::array<Instruction, 259> arm_instruction_table{{
    // Barrier instructions
    {"DSB",                 0xFFFFFFF0, 0xF57FF040, &ARM::DSB       },
    {"DMB",                 0xFFFFFFF0, 0xF57FF050, &ARM::DMB       },
    {"ISB",                 0xFFFFFFF0, 0xF57FF060, &ARM::ISB       },

    // Status Register Access instructions (the ones that had to be separated to decode properly)
    {"CPS",                 0xFFF1FE20, 0xF1000000, &ARM::CPS       },

    // Bit Field instructions
    {"BFC",                 0x0FE0007F, 0x07C0001F, &ARM::BFC       },
    {"BFI",                 0x0FE00070, 0x07C00010, &ARM::BFI       },
    {"SBFX",                0x0FE00070, 0x07A00050, &ARM::SBFX      },
    {"UBFX",                0x0FE00070, 0x07E00050, &ARM::UBFX      },

    // Branch instructions
    {"BLX (immediate)",     0xFE000000, 0xFA000000, &ARM::BLX_imm   },
    {"BLX (register)",      0x0FFFFFF0, 0x012FFF30, &ARM::BLX_reg   },
    {"B",                   0x0F000000, 0x0A000000, &ARM::B         },
    {"BL",                  0x0F000000, 0x0B000000, &ARM::BLX_imm   },
    {"BX",                  0x0FFFFFF0, 0x012FFF10, &ARM::BX        },
    {"BXJ",                 0x0FFFFFF0, 0x012FFF20, &ARM::BXJ       },

    // Coprocessor instructions
    {"CDP2",                0xFF000010, 0xFE000000, &ARM::CDP       },
    {"CDP",                 0x0F000010, 0x0E000000, &ARM::CDP       },
    {"MCR2",                0xFF100010, 0xFE000010, &ARM::MCR       },
    {"MCR",                 0x0F100010, 0x0E000010, &ARM::MCR       },
    {"MCRR2",               0xFFF00000, 0xFC400000, &ARM::MCRR      },
    {"MCRR",                0x0FF00000, 0x0C400000, &ARM::MCRR      },
    {"MRC2",                0xFF100010, 0xFE100010, &ARM::MRC       },
    {"MRC",                 0x0F100010, 0x0E100010, &ARM::MRC       },
    {"MRRC2",               0xFFF00000, 0xFC500000, &ARM::MRRC      },
    {"MRRC",                0x0FF00000, 0x0C500000, &ARM::MRRC      },
    {"LDC2",                0xFE100000, 0xFC100000, &ARM::LDC       },
    {"LDC",                 0x0E100000, 0x0C100000, &ARM::LDC       },
    {"STC2",                0xFE100000, 0xFC000000, &ARM::STC       },
    {"STC",                 0x0E100000, 0x0C000000, &ARM::STC       },

    // Data Processing instructions
    {"ADC (imm)",           0x0FE00000, 0x02A00000, &ARM::ADC_imm   },
    {"ADC (reg)",           0x0FE00010, 0x00A00000, &ARM::ADC_reg   },
    {"ADC (rsr)",           0x0FE00090, 0x00A00010, &ARM::ADC_rsr   },
    {"ADD (imm)",           0x0FE00000, 0x02800000, &ARM::ADD_imm   },
    {"ADD (reg)",           0x0FE00010, 0x00800000, &ARM::ADD_reg   },
    {"ADD (rsr)",           0x0FE00090, 0x00800010, &ARM::ADD_rsr   },
    {"AND (imm)",           0x0FE00000, 0x02000000, &ARM::AND_imm   },
    {"AND (reg)",           0x0FE00010, 0x00000000, &ARM::AND_reg   },
    {"AND (rsr)",           0x0FE00090, 0x00000010, &ARM::AND_rsr   },
    {"BIC (imm)",           0x0FE00000, 0x03C00000, &ARM::BIC_imm   },
    {"BIC (reg)",           0x0FE00010, 0x01C00000, &ARM::BIC_reg   },
    {"BIC (rsr)",           0x0FE00090, 0x01C00010, &ARM::BIC_rsr   },
    {"CMN (imm)",           0x0FF0F000, 0x03700000, &ARM::CMN_imm   },
    {"CMN (reg)",           0x0FF0F010, 0x01700000, &ARM::CMN_reg   },
    {"CMN (rsr)",           0x0FF0F090, 0x01700010, &ARM::CMN_rsr   },
    {"CMP (imm)",           0x0FF0F000, 0x03500000, &ARM::CMP_imm   },
    {"CMP (reg)",           0x0FF0F010, 0x01500000, &ARM::CMP_reg   },
    {"CMP (rsr)",           0x0FF0F090, 0x01500010, &ARM::CMP_rsr   },
    {"EOR (imm)",           0x0FE00000, 0x02200000, &ARM::EOR_imm   },
    {"EOR (reg)",           0x0FE00010, 0x00200000, &ARM::EOR_reg   },
    {"EOR (rsr)",           0x0FE00090, 0x00200010, &ARM::EOR_rsr   },
    {"MOV (imm)",           0x0FEF0000, 0x03A00000, &ARM::MOV_imm   },
    {"MOV (reg)",           0x0FEF0FF0, 0x01A00000, &ARM::MOV_reg   },
    {"MOVT",                0x0FF00000, 0x03400000, &ARM::MOVT      },
    {"MOVW",                0x0FF00000, 0x03000000, &ARM::MOVW      },
    {"MVN (imm)",           0x0FEF0000, 0x03E00000, &ARM::MVN_imm   },
    {"MVN (reg)",           0x0FEF0010, 0x01E00000, &ARM::MVN_reg   },
    {"MVN (rsr)",           0x0FEF0090, 0x01E00010, &ARM::MVN_rsr   },
    {"ORR (imm)",           0x0FE00000, 0x03800000, &ARM::ORR_imm   },
    {"ORR (reg)",           0x0FE00010, 0x01800000, &ARM::ORR_reg   },
    {"ORR (rsr)",           0x0FE00090, 0x01800010, &ARM::ORR_rsr   },
    {"RSB (imm)",           0x0FE00000, 0x02600000, &ARM::RSB_imm   },
    {"RSB (reg)",           0x0FE00010, 0x00600000, &ARM::RSB_reg   },
    {"RSB (rsr)",           0x0FE00090, 0x00600010, &ARM::RSB_rsr   },
    {"RSC (imm)",           0x0FE00000, 0x02E00000, &ARM::RSC_imm   },
    {"RSC (reg)",           0x0FE00010, 0x00E00000, &ARM::RSC_reg   },
    {"RSC (rsr)",           0x0FE00090, 0x00E00010, &ARM::RSC_rsr   },
    {"SBC (imm)",           0x0FE00000, 0x02C00000, &ARM::SBC_imm   },
    {"SBC (reg)",           0x0FE00010, 0x00C00000, &ARM::SBC_reg   },
    {"SBC (rsr)",           0x0FE00090, 0x00C00010, &ARM::SBC_rsr   },
    {"SUB (imm)",           0x0FE00000, 0x02400000, &ARM::SUB_imm   },
    {"SUB (reg)",           0x0FE00010, 0x00400000, &ARM::SUB_reg   },
    {"SUB (rsr)",           0x0FE00090, 0x00400010, &ARM::SUB_rsr   },
    {"TEQ (imm)",           0x0FF0F000, 0x03300000, &ARM::TEQ_imm   },
    {"TEQ (reg)",           0x0FF0F010, 0x01300000, &ARM::TEQ_reg   },
    {"TEQ (rsr)",           0x0FF0F090, 0x01300010, &ARM::TEQ_rsr   },
    {"TST (imm)",           0x0FF0F000, 0x03100000, &ARM::TST_imm   },
    {"TST (reg)",           0x0FF0F010, 0x01100000, &ARM::TST_reg   },
    {"TST (rsr)",           0x0FF0F090, 0x01100010, &ARM::TST_rsr   },

    // Divide instructions
    {"SDIV",                0x0FF0F0F0, 0x0710F010, &ARM::SDIV      },
    {"UDIV",                0x0FF0F0F0, 0x0730F010, &ARM::UDIV      },

    // Exception Generating instructions
    {"BKPT",                0x0FF000F0, 0x01200070, &ARM::BKPT      },
    {"HVC",                 0x0FF000F0, 0x01400070, &ARM::HVC       },
    {"SMC",                 0x0FFFFFF0, 0x01600070, &ARM::SMC       },
    {"SVC",                 0x0F000000, 0x0F000000, &ARM::SVC       },
    {"UDF",                 0xFFF000F0, 0xE7F000F0, &ARM::UDF       },

    // Extension instructions
    {"SXTB",                0x0FFF03F0, 0x06AF0070, &ARM::SXTB      },
    {"SXTB16",              0x0FFF03F0, 0x068F0070, &ARM::SXTB16    },
    {"SXTH",                0x0FFF03F0, 0x06BF0070, &ARM::SXTH      },
    {"SXTAB",               0x0FF003F0, 0x06A00070, &ARM::SXTAB     },
    {"SXTAB16",             0x0FF003F0, 0x06800070, &ARM::SXTAB16   },
    {"SXTAH",               0x0FF003F0, 0x06B00070, &ARM::SXTAH     },
    {"UXTB",                0x0FFF03F0, 0x06EF0070, &ARM::UXTB      },
    {"UXTB16",              0x0FFF03F0, 0x06CF0070, &ARM::UXTB16    },
    {"UXTH",                0x0FFF03F0, 0x06FF0070, &ARM::UXTH      },
    {"UXTAB",               0x0FF003F0, 0x06E00070, &ARM::UXTAB     },
    {"UXTAB16",             0x0FF003F0, 0x06C00070, &ARM::UXTAB16   },
    {"UXTAH",               0x0FF003F0, 0x06F00070, &ARM::UXTAH     },

    // Hint instructions
    {"DBG",                 0x0FFFFFF0, 0x0320F0F0, &ARM::DBG       },
    {"PLD (imm)",           0xFF70F000, 0xF550F000, &ARM::PLD       },
    {"PLD (lit)",           0xFFFFF000, 0xF55FF000, &ARM::PLD       },
    {"PLD (reg)",           0xFF70F010, 0xF710F000, &ARM::PLD       },
    {"PLDW (imm)",          0xFF70F000, 0xF510F000, &ARM::PLD       },
    {"PLDW (reg)",          0xFF70F010, 0xF750F000, &ARM::PLD       },
    {"PLI (imm lit)",       0xFF70F000, 0xF450F000, &ARM::PLI       },
    {"PLI (reg)",           0xFF70F010, 0xF650F000, &ARM::PLI       },
    {"SEV",                 0x0FFFFFFF, 0x0320F004, &ARM::SEV       },
    {"WFE",                 0x0FFFFFFF, 0x0320F002, &ARM::WFE       },
    {"WFI",                 0x0FFFFFFF, 0x0320F003, &ARM::WFI       },
    {"YIELD",               0x0FFFFFFF, 0x0320F001, &ARM::YIELD     },

    // Synchronization Primitive instructions
    {"CLREX",               0xFFFFFFFF, 0xF57FF01F, &ARM::CLREX     },
    {"LDREX",               0x0FF00FFF, 0x01900F9F, &ARM::LDREX     },
    {"LDREXB",              0x0FF00FFF, 0x01D00F9F, &ARM::LDREXB    },
    {"LDREXD",              0x0FF00FFF, 0x01B00F9F, &ARM::LDREXD    },
    {"LDREXH",              0x0FF00FFF, 0x01F00F9F, &ARM::LDREXH    },
    {"STREX",               0x0FF00FF0, 0x01800F90, &ARM::STREX     },
    {"STREXB",              0x0FF00FF0, 0x01C00F90, &ARM::STREXB    },
    {"STREXD",              0x0FF00FF0, 0x01A00F90, &ARM::STREXD    },
    {"STREXH",              0x0FF00FF0, 0x01E00F90, &ARM::STREXH    },
    {"SWP",                 0x0FB00FF0, 0x01000090, &ARM::SWP       },

    // Load/Store instructions
    {"LDRT (A1)",           0x0F700000, 0x04300000, &ARM::LDRT_imm  },
    {"LDRT (A2)",           0x0F700010, 0x06300000, &ARM::LDRT_reg  },
    {"LDR (imm)",           0x0E500000, 0x04100000, &ARM::LDR_imm   },
    {"LDR (reg)",           0x0E500010, 0x06100000, &ARM::LDR_reg   },
    {"LDRBT (A1)",          0x0F700000, 0x04700000, &ARM::LDRBT_imm },
    {"LDRBT (A2)",          0x0F700010, 0x06700000, &ARM::LDRBT_reg },
    {"LDRB (imm)",          0x0E500000, 0x04500000, &ARM::LDRB_imm  },
    {"LDRB (reg)",          0x0E500010, 0x06500000, &ARM::LDRB_reg  },
    {"LDRD (imm)",          0x0E5000F0, 0x004000D0, &ARM::LDRD_imm  },
    {"LDRD (reg)",          0x0E500FF0, 0x000000D0, &ARM::LDRD_reg  },
    {"LDRHT (A1)",          0x0F7000F0, 0x007000B0, &ARM::LDRHT_imm },
    {"LDRHT (A2)",          0x0F700FF0, 0x003000B0, &ARM::LDRHT_reg },
    {"LDRH (imm)",          0x0E5000F0, 0x005000B0, &ARM::LDRH_imm  },
    {"LDRH (reg)",          0x0E500FF0, 0x001000B0, &ARM::LDRH_reg  },
    {"LDRSBT (A1)",         0x0F7000F0, 0x007000D0, &ARM::LDRSBT_imm},
    {"LDRSBT (A2)",         0x0F700FF0, 0x003000D0, &ARM::LDRSBT_reg},
    {"LDRSB (imm)",         0x0E5000F0, 0x005000D0, &ARM::LDRSB_imm },
    {"LDRSB (reg)",         0x0E500FF0, 0x001000D0, &ARM::LDRSB_reg },
    {"LDRSHT (A1)",         0x0F7000F0, 0x007000F0, &ARM::LDRSHT_imm},
    {"LDRSHT (A2)",         0x0F700FF0, 0x003000F0, &ARM::LDRSHT_reg},
    {"LDRSH (imm)",         0x0E5000F0, 0x005000F0, &ARM::LDRSH_imm },
    {"LDRSH (reg)",         0x0E500FF0, 0x001000F0, &ARM::LDRSH_reg },
    {"STRT (A1)",           0x0F700000, 0x04200000, &ARM::STRT_imm  },
    {"STRT (A2)",           0x0F700010, 0x06200000, &ARM::STRT_reg  },
    {"STR (imm)",           0x0E500000, 0x04000000, &ARM::STR_imm   },
    {"STR (reg)",           0x0E500010, 0x06000000, &ARM::STR_reg   },
    {"STRBT (A1)",          0x0F700000, 0x04600000, &ARM::STRBT_imm },
    {"STRBT (A2)",          0x0F700010, 0x06600000, &ARM::STRBT_reg },
    {"STRB (imm)",          0x0E500000, 0x04400000, &ARM::STRB_imm  },
    {"STRB (reg)",          0x0E500010, 0x06400000, &ARM::STRB_reg  },
    {"STRD (imm)",          0x0E5000F0, 0x004000F0, &ARM::STRD_imm  },
    {"STRD (reg)",          0x0E500FF0, 0x000000F0, &ARM::STRD_reg  },
    {"STRHT (A1)",          0x0F7000F0, 0x006000B0, &ARM::STRHT_imm },
    {"STRHT (A2)",          0x0F700FF0, 0x002000B0, &ARM::STRHT_reg },
    {"STRH (imm)",          0x0E5000F0, 0x004000B0, &ARM::STRH_imm  },
    {"STRH (reg)",          0x0E500FF0, 0x000000B0, &ARM::STRH_reg  },

    // Load/Store Multiple instructions
    {"LDMIA/LDMFD",         0x0FD00000, 0x08900000, &ARM::LDMIA     },
    {"LDMDA/LDMFA",         0x0FD00000, 0x08100000, &ARM::LDMDA     },
    {"LDMDB/LDMEA",         0x0FD00000, 0x09100000, &ARM::LDMDB     },
    {"LDMIB/LDMED",         0x0FD00000, 0x09900000, &ARM::LDMIB     },
    {"LDM (exc ret)",       0x0E508000, 0x08508000, &ARM::LDM_stub  },
    {"LDM (usr reg)",       0x0E508000, 0x08500000, &ARM::LDM_stub  },
    {"STMIA/STMEA",         0x0FD00000, 0x08800000, &ARM::STMIA     },
    {"STMDA/STMED",         0x0FD00000, 0x08000000, &ARM::STMDA     },
    {"STMDB/STMFD",         0x0FD00000, 0x09000000, &ARM::STMDB     },
    {"STMIB/STMFA",         0x0FD00000, 0x09800000, &ARM::STMIB     },
    {"STMIB (usr reg)",     0x0E700000, 0x08400000, &ARM::STM_stub  },

    // Miscellaneous instructions
    {"CLZ",                 0x0FFF0FF0, 0x016F0F10, &ARM::CLZ       },
    {"NOP",                 0x0FFFFFFF, 0x0320F000, &ARM::NOP       },
    {"SEL",                 0x0FF00FF0, 0x06800FB0, &ARM::SEL       },

    // Unsigned Sum of Absolute Differences instructions
    {"USAD8",               0x0FF0F0F0, 0x0780F010, &ARM::USAD8     },
    {"USADA8",              0x0FF000F0, 0x07800010, &ARM::USADA8    },

    // Packing instructions
    {"PKH",                 0x0FF00030, 0x06800010, &ARM::PKH       },

    // Reversal instructions
    {"RBIT",                0x0FFF0FF0, 0x06FF0F30, &ARM::RBIT      },
    {"REV",                 0x0FFF0FF0, 0x06BF0F30, &ARM::REV       },
    {"REV16",               0x0FFF0FF0, 0x06BF0FB0, &ARM::REV16     },
    {"REVSH",               0x0FFF0FF0, 0x06FF0FB0, &ARM::REVSH     },

    // Saturation instructions
    {"SSAT",                0x0FE00030, 0x06A00010, &ARM::SSAT      },
    {"SSAT16",              0x0FF00FF0, 0x06A00F30, &ARM::SSAT16    },
    {"USAT",                0x0FE00030, 0x06E00010, &ARM::USAT      },
    {"USAT16",              0x0FF00FF0, 0x06E00F30, &ARM::USAT16    },

    // Multiply (Normal) instructions
    {"MLA",                 0x0FE000F0, 0x00200090, &ARM::MLA       },
    {"MLS",                 0x0FF000F0, 0x00600090, &ARM::MLS       },
    {"MUL",                 0x0FE0F0F0, 0x00000090, &ARM::MUL       },

    // Multiply (Long) instructions
    {"SMLAL",               0x0FE000F0, 0x00E00090, &ARM::SMLAL     },
    {"SMULL",               0x0FE000F0, 0x00C00090, &ARM::SMULL     },
    {"UMAAL",               0x0FF000F0, 0x00400090, &ARM::UMAAL     },
    {"UMLAL",               0x0FE000F0, 0x00A00090, &ARM::UMLAL     },
    {"UMULL",               0x0FE000F0, 0x00800090, &ARM::UMULL     },

    // Multiply (Halfword) instructions
    {"SMLALXY",             0x0FF00090, 0x01400080, &ARM::SMLALXY   },
    {"SMLAXY",              0x0FF00090, 0x01000080, &ARM::SMLAXY    },
    {"SMULXY",              0x0FF0F090, 0x01600080, &ARM::SMULXY    },

    // Multiply (Word by Halfword) instructions
    {"SMLAWY",              0x0FF000B0, 0x01200080, &ARM::SMLAWY    },
    {"SMULWY",              0x0FF0F0B0, 0x012000A0, &ARM::SMULWY    },

    // Multiply (Most Significant Word) instructions
    {"SMMUL",               0x0FF0F0D0, 0x0750F010, &ARM::SMMUL     },
    {"SMMLA",               0x0FF000D0, 0x07500010, &ARM::SMMLA     },
    {"SMMLS",               0x0FF000D0, 0x075000D0, &ARM::SMMLS     },

    // Multiply (Dual) instructions
    {"SMLAD",               0x0FF000D0, 0x07000010, &ARM::SMLAD     },
    {"SMLALD",              0x0FF000D0, 0x07400010, &ARM::SMLALD    },
    {"SMLSD",               0x0FF000D0, 0x07000050, &ARM::SMLSD     },
    {"SMLSLD",              0x0FF000D0, 0x07400050, &ARM::SMLSLD    },
    {"SMUAD",               0x0FF0F0D0, 0x0700F010, &ARM::SMUAD     },
    {"SMUSD",               0x0FF0F0D0, 0x0700F050, &ARM::SMUSD     },

    // Parallel Add/Subtract (Modulo) instructions
    {"SADD8",               0x0FF00FF0, 0x06100F90, &ARM::SADD8     },
    {"SADD16",              0x0FF00FF0, 0x06100F10, &ARM::SADD16    },
    {"SASX",                0x0FF00FF0, 0x06100F30, &ARM::SASX      },
    {"SSAX",                0x0FF00FF0, 0x06100F50, &ARM::SSAX      },
    {"SSUB8",               0x0FF00FF0, 0x06100FF0, &ARM::SSUB8     },
    {"SSUB16",              0x0FF00FF0, 0x06100F70, &ARM::SSUB16    },
    {"UADD8",               0x0FF00FF0, 0x06500F90, &ARM::UADD8     },
    {"UADD16",              0x0FF00FF0, 0x06500F10, &ARM::UADD16    },
    {"UASX",                0x0FF00FF0, 0x06500F30, &ARM::UASX      },
    {"USAX",                0x0FF00FF0, 0x06500F50, &ARM::USAX      },
    {"USUB8",               0x0FF00FF0, 0x06500FF0, &ARM::USUB8     },
    {"USUB16",              0x0FF00FF0, 0x06500F70, &ARM::USUB16    },

    // Parallel Add/Subtract (Saturating) instructions
    {"QADD8",               0x0FF00FF0, 0x06200F90, &ARM::QADD8     },
    {"QADD16",              0x0FF00FF0, 0x06200F10, &ARM::QADD16    },
    {"QASX",                0x0FF00FF0, 0x06200F30, &ARM::QASX      },
    {"QSAX",                0x0FF00FF0, 0x06200F50, &ARM::QSAX      },
    {"QSUB8",               0x0FF00FF0, 0x06200FF0, &ARM::QSUB8     },
    {"QSUB16",              0x0FF00FF0, 0x06200F70, &ARM::QSUB16    },
    {"UQADD8",              0x0FF00FF0, 0x06600F90, &ARM::UQADD8    },
    {"UQADD16",             0x0FF00FF0, 0x06600F10, &ARM::UQADD16   },
    {"UQASX",               0x0FF00FF0, 0x06600F30, &ARM::UQASX     },
    {"UQSAX",               0x0FF00FF0, 0x06600F50, &ARM::UQSAX     },
    {"UQSUB8",              0x0FF00FF0, 0x06600FF0, &ARM::UQSUB8    },
    {"UQSUB16",             0x0FF00FF0, 0x06600F70, &ARM::UQSUB16   },

    // Parallel Add/Subtract (Halving) instructions
    {"SHADD8",              0x0FF00FF0, 0x06300F90, &ARM::SHADD8    },
    {"SHADD16",             0x0FF00FF0, 0x06300F10, &ARM::SHADD16   },
    {"SHASX",               0x0FF00FF0, 0x06300F30, &ARM::SHASX     },
    {"SHSAX",               0x0FF00FF0, 0x06300F50, &ARM::SHSAX     },
    {"SHSUB8",              0x0FF00FF0, 0x06300FF0, &ARM::SHSUB8    },
    {"SHSUB16",             0x0FF00FF0, 0x06300F70, &ARM::SHSUB16   },
    {"UHADD8",              0x0FF00FF0, 0x06700F90, &ARM::UHADD8    },
    {"UHADD16",             0x0FF00FF0, 0x06700F10, &ARM::UHADD16   },
    {"UHASX",               0x0FF00FF0, 0x06700F30, &ARM::UHASX     },
    {"UHSAX",               0x0FF00FF0, 0x06700F50, &ARM::UHSAX     },
    {"UHSUB8",              0x0FF00FF0, 0x06700FF0, &ARM::UHSUB8    },
    {"UHSUB16",             0x0FF00FF0, 0x06700F70, &ARM::UHSUB16   },

    // Saturated Add/Subtract instructions
    {"QADD",                0x0FF00FF0, 0x01000050, &ARM::QADD      },
    {"QSUB",                0x0FF00FF0, 0x01200050, &ARM::QSUB      },
    {"QDADD",               0x0FF00FF0, 0x01400050, &ARM::QDADD     },
    {"QDSUB",               0x0FF00FF0, 0x01600050, &ARM::QDSUB     },

    // Shift instructions
    {"ASR (imm)",           0x0FEF0070, 0x01A00040, &ARM::ASR_imm   },
    {"ASR (reg)",           0x0FEF00F0, 0x01A00050, &ARM::ASR_reg   },
    {"LSL (imm)",           0x0FEF0070, 0x01A00000, &ARM::LSL_imm   },
    {"LSL (reg)",           0x0FEF00F0, 0x01A00010, &ARM::LSL_reg   },
    {"LSR (imm)",           0x0FEF0070, 0x01A00020, &ARM::LSR_imm   },
    {"LSR (reg)",           0x0FEF00F0, 0x01A00030, &ARM::LSR_reg   },
    {"ROR (imm)",           0x0FEF0070, 0x01A00060, &ARM::ROR_imm   },
    {"ROR (reg)",           0x0FEF00F0, 0x01A00070, &ARM::ROR_reg   },
    {"RRX",                 0x0FEF0FF0, 0x01A00060, &ARM::RRX       },

    // Status Register Access instructions
    {"ERET",                0x0FFFFFFF, 0x0160006E, &ARM::ERET      },
    {"SETEND",              0xFFFFFDFF, 0xF1010000, &ARM::SETEND    },
    {"MRS",                 0x0FFF0FFF, 0x010F0000, &ARM::MRS       },
    {"MRS (banked)",        0x0FB00EFF, 0x01000200, &ARM::MRS_banked},
    {"MSR (imm)",           0x0FB0F000, 0x0320F000, &ARM::MSR_imm   },
    {"MSR (reg)",           0x0F70FFF0, 0x0120F000, &ARM::MSR_reg   },
    {"MSR (banked)",        0x0FB0FEF0, 0x0120F200, &ARM::MSR_banked},
    {"RFE",                 0xFE50FFFF, 0xF8100A00, &ARM::RFE       },
    {"SRS",                 0xFE5FFFF0, 0x06A00010, &ARM::SRS       },
}};

constexpr std::array<Instruction, 86> thumb_instruction_table{{
    // Shift, add, subtract, move, compare instructions
    {"MOV (reg)"       , 0xFFC0, 0x0000, &Thumb1::MOV_reg_t2     },
    {"LSL (imm)"       , 0xF800, 0x0000, &Thumb1::LSL_imm        },
    {"LSR (imm)"       , 0xF800, 0x0800, &Thumb1::LSR_imm        },
    {"ASR (imm)"       , 0xF800, 0x1000, &Thumb1::ASR_imm        },
    {"ADD (reg)"       , 0xFE00, 0x1800, &Thumb1::ADD_reg_t1     },
    {"SUB (reg)"       , 0xFE00, 0x1A00, &Thumb1::SUB_reg        },
    {"ADD (3-bit imm)" , 0xFE00, 0x1C00, &Thumb1::ADD_imm3       },
    {"SUB (3-bit imm)" , 0xFE00, 0x1E00, &Thumb1::SUB_imm3       },
    {"MOV (imm)"       , 0xF800, 0x2000, &Thumb1::MOV_imm        },
    {"CMP (imm)"       , 0xF800, 0x2800, &Thumb1::CMP_imm        },
    {"ADD (8-bit imm)" , 0xF800, 0x3000, &Thumb1::ADD_imm8       },
    {"SUB (8-bit imm)" , 0xF800, 0x3800, &Thumb1::SUB_imm8       },

    // Data Processing instructions
    {"AND (reg)"       , 0xFFC0, 0x4000, &Thumb1::AND_reg        },
    {"EOR (reg)"       , 0xFFC0, 0x4040, &Thumb1::EOR_reg        },
    {"LSL (reg)"       , 0xFFC0, 0x4080, &Thumb1::LSL_reg        },
    {"LSR (reg)"       , 0xFFC0, 0x40C0, &Thumb1::LSR_reg        },
    {"ASR (reg)"       , 0xFFC0, 0x4100, &Thumb1::ASR_reg        },
    {"ADC (reg)"       , 0xFFC0, 0x4140, &Thumb1::ADC_reg        },
    {"SBC (reg)"       , 0xFFC0, 0x4180, &Thumb1::SBC_reg        },
    {"ROR (reg)"       , 0xFFC0, 0x41C0, &Thumb1::ROR_reg        },
    {"TST (reg)"       , 0xFFC0, 0x4200, &Thumb1::TST_reg        },
    {"RSB (imm)"       , 0xFFC0, 0x4240, &Thumb1::RSB_imm        },
    {"CMP (reg)"       , 0xFFC0, 0x4280, &Thumb1::CMP_reg        },
    {"CMN (reg)"       , 0xFFC0, 0x42C0, &Thumb1::CMN_reg        },
    {"ORR (reg)"       , 0xFFC0, 0x4300, &Thumb1::ORR_reg        },
    {"MUL"             , 0xFFC0, 0x4340, &Thumb1::MUL            },
    {"BIC (reg)"       , 0xFFC0, 0x4380, &Thumb1::BIC_reg        },
    {"MVN (reg)"       , 0xFFC0, 0x43C0, &Thumb1::MVN_reg        },

    // Special Data Instructions and Branch & Exchange instructions
    {"ADD (reg low)"   , 0xFFC0, 0x4400, &Thumb1::ADD_reg_t2     },
    {"ADD (reg high)"  , 0xFFC0, 0x4440, &Thumb1::ADD_reg_t2     },
    {"ADD (reg high)"  , 0xFF80, 0x4480, &Thumb1::ADD_reg_t2     },
    {"CMP (reg high)"  , 0xFF00, 0x4500, &Thumb1::CMP_reg        },
    {"MOV (reg low)"   , 0xFFC0, 0x4600, &Thumb1::MOV_reg_t1     },
    {"MOV (reg high)"  , 0xFFC0, 0x4640, &Thumb1::MOV_reg_t1     },
    {"MOV (reg high)"  , 0xFF80, 0x4680, &Thumb1::MOV_reg_t1     },
    {"BX"              , 0xFF80, 0x4700, &Thumb1::BX             },
    {"BLX (reg)"       , 0xFF80, 0x4780, &Thumb1::BLX_reg        },

    // Load from literal pool
    {"LDR (lit)"       , 0xF800, 0x4800, &Thumb1::LDR_lit        },

    // Load/Store Single Data Item instructions
    {"STR (reg)"       , 0xFE00, 0x5000, &Thumb1::STR_reg        },
    {"STRH (reg)"      , 0xFE00, 0x5200, &Thumb1::STRH_reg       },
    {"STRB (reg)"      , 0xFE00, 0x5400, &Thumb1::STRB_reg       },
    {"LDRSB (reg)"     , 0xFE00, 0x5600, &Thumb1::LDRSB_reg      },
    {"LDR (reg)"       , 0xFE00, 0x5800, &Thumb1::LDR_reg        },
    {"LDRH (reg)"      , 0xFE00, 0x5A00, &Thumb1::LDRH_reg       },
    {"LDRB (reg)"      , 0xFE00, 0x5C00, &Thumb1::LDRB_reg       },
    {"LDRSH (reg)"     , 0xFE00, 0x5E00, &Thumb1::LDRSH_reg      },
    {"STR (imm)"       , 0xF800, 0x6000, &Thumb1::STR_imm        },
    {"LDR (imm)"       , 0xF800, 0x6800, &Thumb1::LDR_imm        },
    {"STRB (imm)"      , 0xF800, 0x7000, &Thumb1::STRB_imm       },
    {"LDRB (imm)"      , 0xF800, 0x7800, &Thumb1::LDRB_imm       },
    {"STRH (imm)"      , 0xF800, 0x8000, &Thumb1::STRH_imm       },
    {"LDRH (imm)"      , 0xF800, 0x8800, &Thumb1::LDRH_imm       },
    {"STR (imm SP rel)", 0xF800, 0x9000, &Thumb1::STR_imm_sp_rel },
    {"LDR (imm SP rel)", 0xF800, 0x9800, &Thumb1::LDR_imm_sp_rel },

    // Relative address generation instructions
    {"ADR"             , 0xF800, 0xA000, &Thumb1::ADR            },
    {"ADD (SP + imm)"  , 0xF800, 0xA800, &Thumb1::ADD_sp_plus_imm},

    // Miscellaneous instructions
    {"ADD (SP + imm)"  , 0xFF80, 0xB000, &Thumb1::ADD_sp_plus_imm},
    {"SUB (SP - imm)"  , 0xFF80, 0xB080, &Thumb1::SUB_sp_min_imm },
    {"CBZ/CBNZ"        , 0xFF00, 0xB100, &Thumb1::CB             },
    {"SXTH"            , 0xFFC0, 0xB200, &Thumb1::SXTH           },
    {"SXTB"            , 0xFFC0, 0xB240, &Thumb1::SXTB           },
    {"UXTH"            , 0xFFC0, 0xB280, &Thumb1::UXTH           },
    {"UXTB"            , 0xFFC0, 0xB2C0, &Thumb1::UXTB           },
    {"CBZ/CBNZ"        , 0xFF00, 0xB300, &Thumb1::CB             },
    {"PUSH"            , 0xFE00, 0xB400, &Thumb1::PUSH           },
    {"SETEND"          , 0xFFE0, 0xB640, &Thumb1::SETEND         },
    {"CPS"             , 0xFFE0, 0xB660, &Thumb1::CPS            },
    {"CBZ/CBNZ"        , 0xFF00, 0xB900, &Thumb1::CB             },
    {"REV"             , 0xFFC0, 0xBA00, &Thumb1::REV            },
    {"REV16"           , 0xFFC0, 0xBA40, &Thumb1::REV16          },
    {"REVSH"           , 0xFFC0, 0xBAC0, &Thumb1::REVSH          },
    {"CBZ/CBNZ"        , 0xFF00, 0xBB00, &Thumb1::CB             },
    {"POP"             , 0xFE00, 0xBC00, &Thumb1::POP            },
    {"BKPT"            , 0xFF00, 0xBE00, &ARM::BKPT              },

    // If-then and Hint instructions
    {"NOP"             , 0xFFFF, 0xBF00, &ARM::NOP               },
    {"YIELD"           , 0xFFFF, 0xBF10, &ARM::YIELD             },
    {"WFE"             , 0xFFFF, 0xBF20, &ARM::WFE               },
    {"WFI"             , 0xFFFF, 0xBF30, &ARM::WFI               },
    {"SEV"             , 0xFFFF, 0xBF40, &ARM::SEV               },
    {"IT"              , 0xFF00, 0xBF00, &Thumb1::IT             },

    // Load/Store Multiple instructions
    {"STM"             , 0xFC00, 0xC000, &Thumb1::STMIA          },
    {"LDM"             , 0xFC00, 0xC800, &Thumb1::LDMIA          },

    // Conditional Branch and Supervisor Call instructions
    {"UDF"             , 0xFF00, 0xDE00, &ARM::UDF               },
    {"SVC"             , 0xFF00, 0xDF00, &Thumb1::SVC            },
    {"B"               , 0xF000, 0xD000, &Thumb1::B_imm8         },

    // Branch instruction
    {"B"               , 0xF800, 0xE000, &Thumb1::B_imm11        }
}};

constexpr std::array<Instruction, 268> thumb2_instruction_table{{
    // Load/Store Multiple
    {"SRS"          , 0xFFD00000, 0xE8000000},
    {"RFE"          , 0xFFD00000, 0xE8100000},
    {"STMIA/STMEA"  , 0xFFD00000, 0xE8800000},
    {"POP"          , 0xFFFF0000, 0xE8BD0000},
    {"LDMIA/LDMFD"  , 0xFFD00000, 0xE8900000},
    {"PUSH"         , 0xFFFF0000, 0xE92D0000},
    {"STMDB/STMFD"  , 0xFFD00000, 0xE9000000},
    {"LDMDB/LDMEA"  , 0xFFD00000, 0xE9100000},
    {"SRS"          , 0xFFD00000, 0xE9800000},
    {"RFE"          , 0xFFD00000, 0xE9900000},

    // Load/Store Dual, Load/Store Exclusive, Table Branch
    {"STREX"        , 0xFFF00000, 0xE8400000},
    {"LDREX"        , 0xFFF00000, 0xE8500000},
    {"STRD (imm)"   , 0xFF700000, 0xE8600000},
    {"STRD (imm)"   , 0xFF500000, 0xE9400000},
    {"LDRD (lit)"   , 0xFF7F0000, 0xE87F0000},
    {"LDRD (lit)"   , 0xFF5F0000, 0xE95F0000},
    {"LDRD (imm)"   , 0xFF700000, 0xE8700000},
    {"LDRD (imm)"   , 0xFF500000, 0xE9500000},
    {"STREXB"       , 0xFFF000F0, 0xE8C00040},
    {"STREXH"       , 0xFFF000F0, 0xE8C00050},
    {"STREXD"       , 0xFFF000F0, 0xE8C00070},
    {"TBB"          , 0xFFF000F0, 0xE8D00000},
    {"TBH"          , 0xFFF000F0, 0xE8D00010},
    {"LDREXB"       , 0xFFF000F0, 0xE8D00040},
    {"LDREXH"       , 0xFFF000F0, 0xE8D00050},
    {"LDREXD"       , 0xFFF000F0, 0xE8D00070},

    // Data Processing (Shifted Register)
    {"TST (reg)"    , 0xFFF00F00, 0xEA100F00},
    {"AND (reg)"    , 0xFFE00000, 0xEA000000},
    {"BIC (reg)"    , 0xFFE00000, 0xEA200000},
    {"MOV (reg)"    , 0xFFEF70F0, 0xEA4F0000},
    {"LSL (imm)"    , 0xFFEF0030, 0xEA4F0000},
    {"LSR (imm)"    , 0xFFEF0030, 0xEA4F0010},
    {"ASR (imm)"    , 0xFFEF0030, 0xEA4F0020},
    {"RRX"          , 0xFFEF70F0, 0xEA4F0030},
    {"ROR (imm)"    , 0xFFEF0030, 0xEA4F0030},
    {"ORR (reg)"    , 0xFFE00000, 0xEA400000},
    {"MVN (reg)"    , 0xFFEF0000, 0xEA6F0000},
    {"ORN (reg)"    , 0xFFE00000, 0xEA600000},
    {"TEQ (reg)"    , 0xFFF00F00, 0xEA900F00},
    {"EOR (reg)"    , 0xFFE00000, 0xEA800000},
    {"PKH"          , 0xFFE00000, 0xEAC00000},
    {"CMN (reg)"    , 0xFFF00F00, 0xEB100F00},
    {"ADD (reg)"    , 0xFFE00000, 0xEB000000},
    {"ADC (reg)"    , 0xFFE00000, 0xEB400000},
    {"SBC (reg)"    , 0xFFE00000, 0xEB600000},
    {"CMP (reg)"    , 0xFFF00F00, 0xEBB00F00},
    {"SUB (reg)"    , 0xFFE00000, 0xEBA00000},
    {"RSB (reg)"    , 0xFFE00000, 0xEBC00000},

    // Data Processing (Modified Immediate)
    {"TST (imm)"    , 0xFBF08F00, 0xF0100F00},
    {"AND (imm)"    , 0xFBE08000, 0xF0000000},
    {"BIC (imm)"    , 0xFBE08000, 0xF0200000},
    {"MOV (imm)"    , 0xFFEF8000, 0xF04F0000},
    {"ORR (imm)"    , 0xFBE08000, 0xF0400000},
    {"MVN (imm)"    , 0xFFEF8000, 0xF06F0000},
    {"ORN (imm)"    , 0xFBE08000, 0xF0600000},
    {"TEQ (imm)"    , 0xFBF08F00, 0xF0900F00},
    {"EOR (imm)"    , 0xFBE08000, 0xF0800000},
    {"CMN (imm)"    , 0xFBF08F00, 0xF1100F00},
    {"ADD (imm)"    , 0xFBE08000, 0xF1000000},
    {"ADC (imm)"    , 0xFBE08000, 0xF1400000},
    {"SBC (imm)"    , 0xFBE08000, 0xF1600000},
    {"CMP (imm)"    , 0xFBF08F00, 0xF1B00F00},
    {"SUB (imm)"    , 0xFBE08000, 0xF1A00000},
    {"RSB (imm)"    , 0xFBE08000, 0xF1C00000},

    // Data Processing (Plain Binary Immediate)
    {"ADR"          , 0xFBFF8000, 0xF20F0000},
    {"ADD (imm)"    , 0xFBF08000, 0xF2000000},
    {"MOVW (imm)"   , 0xFBF08000, 0xF2400000},
    {"ADR"          , 0xFBFF8000, 0xF2AF0000},
    {"SUB (imm)"    , 0xFBF08000, 0xF2AF0000},
    {"MOVT"         , 0xFBF08000, 0xF2C00000},
    {"SSAT"         , 0xFBF08000, 0xF3000000},
    {"SSAT16"       , 0xFBF0F0C0, 0xF3200000},
    {"SSAT"         , 0xFBF08000, 0xF3200000},
    {"SBFX"         , 0xFBF08000, 0xF3400000},
    {"BFC"          , 0xFBFF8000, 0xF36F0000},
    {"BFI"          , 0xFBF08000, 0xF3600000},
    {"USAT"         , 0xFBF08000, 0xF3800000},
    {"USAT16"       , 0xFBF0F0C0, 0xF3A00000},
    {"USAT"         , 0xFBF08000, 0xF3A00000},
    {"UBFX"         , 0xFBF08000, 0xF3C00000},

    // Branches and Miscellaneous Control
    {"MSR (banked)" , 0xFFE0D020, 0xF3808020},
    {"MSR (reg)"    , 0xFFF0D020, 0xF3908000},
    {"MSR (reg)"    , 0xFFF0D320, 0xF3808100},
    {"MSR (reg)"    , 0xFFF0D220, 0xF3808200},
    {"MSR (reg)"    , 0xFFF0D320, 0xF3808000},

    {"NOP"          , 0xFFF0D7FF, 0xF3A08000},
    {"YIELD"        , 0xFFF0D7FF, 0xF3A08001},
    {"WFE"          , 0xFFF0D7FF, 0xF3A08002},
    {"WFI"          , 0xFFF0D7FF, 0xF3A08003},
    {"SEV"          , 0xFFF0D7FF, 0xF3A08004},
    {"DBG"          , 0xFFF0D7F0, 0xF3A080F0},
    {"CPS"          , 0xFFF0D000, 0xF3A08000},

    {"ENTERX"       , 0xFFF0D0F0, 0xF3B08010},
    {"LEAVEX"       , 0xFFF0D0F0, 0xF3B08000},
    {"CLREX"        , 0xFFF0D0F0, 0xF3B08020},
    {"DSB"          , 0xFFF0D0F0, 0xF3B08040},
    {"DMB"          , 0xFFF0D0F0, 0xF3B08050},
    {"ISB"          , 0xFFF0D0F0, 0xF3B08060},

    {"BXJ"          , 0xFFF0FFFF, 0xF3C08F00},
    {"ERET"         , 0xFFFFFFFF, 0xF3DE8F00},
    {"SUBS PC, LR"  , 0xFFFFFF00, 0xF3DE8F00},

    {"MRS (banked)" , 0xFFE0D020, 0xF3E08020},
    {"MRS (reg)"    , 0xFFF0D020, 0xF3F08000},
    {"MRS (reg)"    , 0xFFF0D020, 0xF3E08000},
    {"HVC"          , 0xFFF0F000, 0xF7E08000},
    {"SMC"          , 0xFFF0FFFF, 0xF7F08000},
    {"UDF"          , 0xFFF0F000, 0xF7F0A000},

    {"BL"           , 0xF800D000, 0xF000D000},
    {"BLX"          , 0xF800D000, 0xF000C000},
    {"B"            , 0xF800D000, 0xF0009000},
    {"B (cond)"     , 0xF800D000, 0xF0008000},

    // Store Single Data Item
    {"STRB (imm)"   , 0xFFF00900, 0xF8000900},
    {"STRB (imm)"   , 0xFFF00F00, 0xF8000C00},
    {"STRB (imm)"   , 0xFFF00000, 0xF8800000},
    {"STRBT"        , 0xFFF00F00, 0xF8000E00},
    {"STRB (reg)"   , 0xFFF00FC0, 0xF8000000},
    {"STRH (imm)"   , 0xFFF00900, 0xF8200900},
    {"STRH (imm)"   , 0xFFF00F00, 0xF8200C00},
    {"STRH (imm)"   , 0xFFF00000, 0xF8A00000},
    {"STRHT"        , 0xFFF00F00, 0xF8200E00},
    {"STRH (reg)"   , 0xFFF00FC0, 0xF8200000},
    {"STR (imm)"    , 0xFFF00900, 0xF8400900},
    {"STR (imm)"    , 0xFFF00F00, 0xF8400C00},
    {"STR (imm)"    , 0xFFF00000, 0xF8C00000},
    {"STRT"         , 0xFFF00F00, 0xF8400E00},
    {"STR (reg)"    , 0xFFF00FC0, 0xF8400000},

    // Load Byte and Memory Hints
    {"PLD (lit)"    , 0xFF7FF000, 0xF81FF000},
    {"PLD (reg)"    , 0xFFF0FFC0, 0xF810F000},
    {"PLD (imm8)"   , 0xFFD0FF00, 0xF810FC00},
    {"PLD (imm12)"  , 0xFFF0F000, 0xF890F000},
    {"PLI (lit)"    , 0xFF7FF000, 0xF91FF000},
    {"PLI (reg)"    , 0xFFF0FFC0, 0xF910F000},
    {"PLI (imm8)"   , 0xFFF0FF00, 0xF910FC00},
    {"PLI (imm12)"  , 0xFFF0F000, 0xF990F000},
    {"LDRB (lit)"   , 0xFF7F0000, 0xF81F0000},
    {"LDRB (reg)"   , 0xFFF00FC0, 0xF8100000},
    {"LDRBT"        , 0xFFF00F00, 0xF8100E00},
    {"LDRB (imm8)"  , 0xFFF00800, 0xF8100800},
    {"LDRB (imm12)" , 0xFFF00000, 0xF8900000},
    {"LDRSB (lit)"  , 0xFF7F0000, 0xF91F0000},
    {"LDRSB (reg)"  , 0xFFF00FC0, 0xF9100000},
    {"LDRSBT"       , 0xFFF00F00, 0xF9100E00},
    {"LDRSB (imm8)" , 0xFFF00800, 0xF9100800},
    {"LDRSB (imm12)", 0xFFF00000, 0xF9900000},

    // Load Halfword and Memory Hints
    {"LDRH (lit)"   , 0xFF7F0000, 0xF83F0000},
    {"LDRH (reg)"   , 0xFFF00FC0, 0xF8300000},
    {"LDRHT"        , 0xFFF00F00, 0xF8300E00},
    {"LDRH (imm8)"  , 0xFFF00800, 0xF8300800},
    {"LDRH (imm12)" , 0xFFF00000, 0xF8B00000},
    {"LDRSH (lit)"  , 0xFF7F0000, 0xF93F0000},
    {"LDRSH (reg)"  , 0xFFF00FC0, 0xF9300000},
    {"LDRSHT"       , 0xFFF00F00, 0xF9300E00},
    {"LDRSH (imm8)" , 0xFFF00800, 0xF9300800},
    {"LDRSH (imm12)", 0xFFF00000, 0xF9B00000},
    {"NOP"          , 0xFFF0FFC0, 0xF930F000},
    {"NOP"          , 0xFFF0FF00, 0xF930FC00},
    {"NOP"          , 0xFF7FF000, 0xF93FF000},
    {"NOP"          , 0xFFF0F000, 0xF9B0F000},

    // Load Word
    {"LDR (lit)"    , 0xFF7F0000, 0xF85F0000},
    {"LDRT"         , 0xFFF00F00, 0xF8500E00},
    {"LDR (reg)"    , 0xFFF00FC0, 0xF8500000},
    {"LDR (imm8)"   , 0xFFF00800, 0xF8500800},
    {"LDR (imm12)"  , 0xFFF00000, 0xF8D00000},

    // Undefined
    {"UDF"          , 0xFE700000, 0xF8700000},

    // Data Processing (register)
    {"LSL (reg)"    , 0xFFE0F0F0, 0xFA00F000},
    {"LSR (reg)"    , 0xFFE0F0F0, 0xFA20F000},
    {"ASR (reg)"    , 0xFFE0F0F0, 0xFA40F000},
    {"ROR (reg)"    , 0xFFE0F0F0, 0xFA60F000},
    {"SXTH"         , 0xFFFFF080, 0xFA0FF080},
    {"SXTAH"        , 0xFFF0F080, 0xFA00F080},
    {"UXTH"         , 0xFFFFF080, 0xFA1FF080},
    {"UXTAH"        , 0xFFF0F080, 0xFA10F080},
    {"SXTB16"       , 0xFFFFF080, 0xFA2FF080},
    {"SXTAB16"      , 0xFFF0F080, 0xFA20F080},
    {"UXTB16"       , 0xFFFFF080, 0xFA3FF080},
    {"UXTAB16"      , 0xFFF0F080, 0xFA30F080},
    {"SXTB"         , 0xFFFFF080, 0xFA4FF080},
    {"SXTAB"        , 0xFFF0F080, 0xFA40F080},
    {"UXTB"         , 0xFFFFF080, 0xFA5FF080},
    {"UXTAB"        , 0xFFF0F080, 0xFA50F080},

    // Parallel Addition and Subtraction (signed)
    {"SADD16"       , 0xFFF0F0F0, 0xFA90F000},
    {"SASX"         , 0xFFF0F0F0, 0xFAA0F000},
    {"SSAX"         , 0xFFF0F0F0, 0xFAE0F000},
    {"SSUB16"       , 0xFFF0F0F0, 0xFAD0F000},
    {"SADD8"        , 0xFFF0F0F0, 0xFA80F000},
    {"SSUB8"        , 0xFFF0F0F0, 0xFAC0F000},
    {"QADD16"       , 0xFFF0F0F0, 0xFA90F010},
    {"QASX"         , 0xFFF0F0F0, 0xFAA0F010},
    {"QSAX"         , 0xFFF0F0F0, 0xFAE0F010},
    {"QSUB16"       , 0xFFF0F0F0, 0xFAD0F010},
    {"QADD8"        , 0xFFF0F0F0, 0xFA80F010},
    {"QSUB8"        , 0xFFF0F0F0, 0xFAC0F010},
    {"SHADD16"      , 0xFFF0F0F0, 0xFA90F020},
    {"SHASX"        , 0xFFF0F0F0, 0xFAA0F020},
    {"SHSAX"        , 0xFFF0F0F0, 0xFAE0F020},
    {"SHSUB16"      , 0xFFF0F0F0, 0xFAD0F020},
    {"SHADD8"       , 0xFFF0F0F0, 0xFA80F020},
    {"SHSUB8"       , 0xFFF0F0F0, 0xFAC0F020},

    // Parallel Addition and Subtraction (unsigned)
    {"UADD16"       , 0xFFF0F0F0, 0xFA90F040},
    {"UASX"         , 0xFFF0F0F0, 0xFAA0F040},
    {"USAX"         , 0xFFF0F0F0, 0xFAE0F040},
    {"USUB16"       , 0xFFF0F0F0, 0xFAD0F040},
    {"UADD8"        , 0xFFF0F0F0, 0xFA80F040},
    {"USUB8"        , 0xFFF0F0F0, 0xFAC0F040},
    {"UQADD16"      , 0xFFF0F0F0, 0xFA90F050},
    {"UQASX"        , 0xFFF0F0F0, 0xFAA0F050},
    {"UQSAX"        , 0xFFF0F0F0, 0xFAE0F050},
    {"UQSUB16"      , 0xFFF0F0F0, 0xFAD0F050},
    {"UQADD8"       , 0xFFF0F0F0, 0xFA80F050},
    {"UQSUB8"       , 0xFFF0F0F0, 0xFAC0F050},
    {"UHADD16"      , 0xFFF0F0F0, 0xFA90F060},
    {"UHASX"        , 0xFFF0F0F0, 0xFAA0F060},
    {"UHSAX"        , 0xFFF0F0F0, 0xFAE0F060},
    {"UHSUB16"      , 0xFFF0F0F0, 0xFAD0F060},
    {"UHADD8"       , 0xFFF0F0F0, 0xFA80F060},
    {"UHSUB8"       , 0xFFF0F0F0, 0xFAC0F060},

    // Miscellaneous Operations
    {"QADD"         , 0xFFF0F0F0, 0xFA80F080},
    {"QDADD"        , 0xFFF0F0F0, 0xFA80F090},
    {"QSUB"         , 0xFFF0F0F0, 0xFA80F0A0},
    {"QDSUB"        , 0xFFF0F0F0, 0xFA80F0B0},
    {"REV"          , 0xFFF0F0F0, 0xFA90F080},
    {"REV16"        , 0xFFF0F0F0, 0xFA90F090},
    {"RBIT"         , 0xFFF0F0F0, 0xFA90F0A0},
    {"REVSH"        , 0xFFF0F0F0, 0xFA90F0B0},
    {"SEL"          , 0xFFF0F0F0, 0xFAA0F080},
    {"CLZ"          , 0xFFF0F0F0, 0xFAB0F080},

    // Multiply, Multiply Accumulate, and Absolute Difference
    {"MUL"          , 0xFFF0F0F0, 0xFB00F000},
    {"MLA"          , 0xFFF000F0, 0xFB000000},
    {"MLS"          , 0xFFF000F0, 0xFB000010},
    {"SMULXY"       , 0xFFF0F0C0, 0xFB10F000},
    {"SMLAXY"       , 0xFFF000C0, 0xFB100000},
    {"SMUAD"        , 0xFFF0F0E0, 0xFB20F000},
    {"SMLAD"        , 0xFFF000E0, 0xFB200000},
    {"SMULWY"       , 0xFFF0F0E0, 0xFB30F000},
    {"SMLAWY"       , 0xFFF000E0, 0xFB300000},
    {"SMUSD"        , 0xFFF0F0E0, 0xFB40F000},
    {"SMLSD"        , 0xFFF000E0, 0xFB400000},
    {"SMMUL"        , 0xFFF0F0E0, 0xFB50F000},
    {"SMMLA"        , 0xFFF000E0, 0xFB500000},
    {"SMMLS"        , 0xFFF000E0, 0xFB600000},
    {"USAD8"        , 0xFFF0F0F0, 0xFB70F000},
    {"USADA8"       , 0xFFF000F0, 0xFB700000},

    // Long Multiply, Long Multiply Accumulate, and Divide
    {"SMULL"        , 0xFFF000F0, 0xFB800000},
    {"SDIV"         , 0xFFF000F0, 0xFB9000F0},
    {"UMULL"        , 0xFFF000F0, 0xFBA00000},
    {"UDIV"         , 0xFFF000F0, 0xFBB000F0},
    {"SMLAL"        , 0xFFF000F0, 0xFBC00000},
    {"SMLALXY"      , 0xFFF000C0, 0xFBC00080},
    {"SMLALD"       , 0xFFF000E0, 0xFBC000C0},
    {"SMLSLD"       , 0xFFF000E0, 0xFBD000C0},
    {"UMLAL"        , 0xFFF000F0, 0xFBE00000},
    {"UMAAL"        , 0xFFF000F0, 0xFBE00060},

    // Coprocessor
    {"MCRR2"        , 0xFFF00000, 0xFC400000},
    {"MCRR"         , 0xFFF00000, 0xEC400000},
    {"STC2"         , 0xFE100000, 0xFC000000},
    {"STC"          , 0xFE100000, 0xEC000000},
    {"MRRC2"        , 0xFFF00000, 0xFC500000},
    {"MRRC"         , 0xFFF00000, 0xEC500000},
    {"LDC2 (lit)"   , 0xFE1F0000, 0xFC1F0000},
    {"LDC (lit)"    , 0xFE1F0000, 0xEC1F0000},
    {"LDC2 (imm)"   , 0xFE100000, 0xFC100000},
    {"LDC (imm)"    , 0xFE100000, 0xEC100000},
    {"CDP2"         , 0xFF000010, 0xFE000000},
    {"CDP"          , 0xFF000010, 0xEE000000},
    {"MCR2"         , 0xFF100010, 0xFE000010},
    {"MCR"          , 0xFF100010, 0xEE000010},
    {"MRC2"         , 0xFF100010, 0xFE100010},
    {"MRC"          , 0xFF100010, 0xEE100010},
}};

constexpr std::array<Instruction, 9> thumbee_instruction_table{{
    {"HBP"               , 0xFF00, 0xC000},
    {"UDF"               , 0xFF00, 0xC100},
    {"HB/HBL"            , 0xFE00, 0xC200},
    {"HBLP"              , 0xFC00, 0xC400},
    {"LDR (imm frame)"   , 0xFE00, 0xC800},
    {"CHKA"              , 0xFF00, 0xCA00},
    {"LDR (imm lit pool)", 0xFF00, 0xCB00},
    {"LDR (imm array)"   , 0xFE00, 0xCC00},
    {"STR (imm frame)"   , 0xFE00, 0xCE00}
}};

DecodedInstruction decode_arm_instruction(const uint32_t opcode)
{
    const auto inst = decode_implementation(arm_instruction_table, opcode);
    if (inst != arm_instruction_table.end())
        return {inst->function, opcode};

    return {nullptr, opcode};
}

DecodedInstruction decode_thumb16_instruction(const uint32_t opcode, const State::InstructionSet instruction_set)
{
    if (instruction_set == State::InstructionSet::ThumbEE)
    {
        const auto inst = decode_implementation(thumbee_instruction_table, opcode);
        if (inst != thumbee_instruction_table.end())
            return {inst->function, opcode};
    }

    const auto inst = decode_implementation(thumb_instruction_table, opcode);
    if (inst != thumb_instruction_table.end())
        return {inst->function, opcode};

    return {nullptr, opcode};
}

DecodedInstruction decode_thumb32_instruction(const uint32_t opcode)
{
    const auto inst = decode_implementation(thumb2_instruction_table, opcode);
    if (inst != thumb2_instruction_table.end())
        return {inst->function, opcode};

    return {nullptr, opcode};
}

DecodedInstruction decode_thumb_instruction(const uint32_t opcode, const State::InstructionSet instruction_set)
{
    const bool is_thumb_2_instruction = (opcode & 0xF8000000) >= 0xE8000000;

    if (is_thumb_2_instruction)
        return decode_thumb32_instruction(opcode);

    return decode_thumb16_instruction(opcode, instruction_set);
}

} // Anonymous namespace

namespace ForeARM
{

DecodedInstruction decode_instruction(const uint32_t opcode, const State::InstructionSet instruction_set)
{
    if (instruction_set == State::InstructionSet::ARM)
        return decode_arm_instruction(opcode);

    return decode_thumb_instruction(opcode, instruction_set);
}

} // namespace ForeARM

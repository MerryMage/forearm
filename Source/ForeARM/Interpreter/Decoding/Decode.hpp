#pragma once

#include <cstdint>

namespace ForeARM
{

namespace State
{
class ARMState;
enum class InstructionSet;
}

/**
 * Represents a potentially successfully decoded instruction.
 *
 * @note This class satisfies the FunctionObject concept.
 */
class DecodedInstruction final
{
public:
    /**
     * Underlying function that implements the functionality of the
     * decoded instruction. The function is passed the current state
     * instance of an arbitrary interpreter instance as well as the
     * opcode that represents the instruction.
     */
    using InstructionFunction = void (*)(State::ARMState&, uint32_t);

    constexpr DecodedInstruction() noexcept = default;

    /**
     * Constructor
     *
     * @param func   The function that implements the functionality of the decoded instruction.
     * @param opcode The opcode for the instruction.
     */
    constexpr DecodedInstruction(InstructionFunction func, uint32_t opcode) noexcept
        : m_func{func}, m_opcode{opcode}
    {
    }

    /**
     * Indicates whether or not this decoded instruction is valid.
     *
     * @return Whether or not this decoded instruction is valid.
     */
    constexpr explicit operator bool() const noexcept
    {
        return m_func != nullptr;
    }

    /**
     * Retrieves the underlying opcode for this decoded instruction.
     *
     * @return The underlying opcode.
     */
    constexpr uint32_t opcode() const noexcept
    {
        return m_opcode;
    }

    /**
     * Executes the decoded instruction.
     *
     * @param state The ARMState context to execute this instruction under.
     *
     * @note This assumes the instruction is in a valid state.
     */
    void operator()(State::ARMState& state) const
    {
        m_func(state, m_opcode);
    }

private:
    InstructionFunction m_func = nullptr;
    uint32_t m_opcode = 0;
};

/**
 * Attempts to decode an instruction.
 *
 * @param opcode          The opcode to attempt to decode.
 * @param instruction_set The instruction set to decode in terms of.
 *
 * @return A potentially successfully decoded instruction.
 */
DecodedInstruction decode_instruction(uint32_t opcode, State::InstructionSet instruction_set);

} // namespace ForeARM

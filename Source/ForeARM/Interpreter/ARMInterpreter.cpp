#include <cmath>
#include <utility>
#include "ForeARM/Interpreter/Decoding/Decode.hpp"
#include "ForeARM/Interpreter/ARMInterpreter.hpp"
#include "ForeARM/Interpreter/State/MemoryInterface/MemoryInterface.hpp"

namespace ForeARM
{

Interpreter::Interpreter() = default;

Interpreter::Interpreter(State::ARMState state)
    : m_state{std::move(state)}
{
}

// TODO: This greatly needs to be improved.
//
// Possibly keep decoding until a branch is hit and then cache?
// However, I'm not sure what a decent 'eviction' method for the cache would be.
//
// Will cross that bridge when I get to it.
//
void Interpreter::interpret(const uint32_t opcode)
{
    const auto current_instruction_set = m_state.current_instruction_set();
    const auto decoded_instruction = decode_instruction(opcode, current_instruction_set);

    // TODO: Signal an undefined instruction exception
    //       here whenever I implement exception vectors.
    if (!decoded_instruction)
        return;

    // TODO/REMEMBER: if bits 11 to 15 of the first halfword of a Thumb instruction
    //                are any of: 0b11101, 0b11110, 0b11111, then it is the first
    //                half of a Thumb-2 instruction. This is here so I remember
    //                to take this into account when shuffling this around
    //                once I get to doing opcode translation.
    execute_instruction(decoded_instruction);
}

State::ARMState& Interpreter::state() noexcept
{
    return m_state;
}

const State::ARMState& Interpreter::state() const noexcept
{
    return m_state;
}

void Interpreter::set_memory_interface(std::unique_ptr<State::MemoryInterface> memory_interface)
{
    m_state.set_memory_interface(std::move(memory_interface));
}

void Interpreter::execute_instruction(const DecodedInstruction instruction)
{
    const uint32_t pc_before_execution = m_state.reg(15);

    if (State::predication_condition_passed(m_state, instruction.opcode()))
        instruction(m_state);

    const uint32_t pc_after_execution = m_state.reg(15);

    // Only increase the program counter if a branch did not occur.
    if (pc_before_execution == pc_after_execution)
        m_state.increase_program_counter(instruction.opcode());
}

} // namespace ForeARM

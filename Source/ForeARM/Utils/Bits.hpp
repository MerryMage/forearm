#pragma once

#include <climits>
#include <cstddef>
#include <cstdint>
#include <limits>
#include <type_traits>

namespace Bit
{

/**
 * Retrieves the size of a type in bytes.
 *
 * @tparam T Type to get the size of.
 *
 * @return the size of the type in bytes.
 */
template <typename T>
constexpr size_t bit_size() noexcept
{
    return sizeof(T) * CHAR_BIT;
}

/**
 * Creates a bitmask of a given size.
 * The bitmask encompasses bits `0` to `size - 1`.
 *
 * @param  size The size of the bitmask.
 * @tparam T    The type of the bitmask.
 *
 * @return The created bitmask.
 */
template <typename T>
constexpr T create_bitmask(const size_t size) noexcept
{
    return static_cast<T>((static_cast<uintmax_t>(1) << size) - 1);
}

/**
 * Creates a bitmask of a given size.
 * The bitmask encompasses bits `0` to `size - 1`.
 *
 * @tparam size The size of the bitmask.
 * @tparam T    The type of the bitmask.
 *
 * @return The created bitmask.
 */
template <size_t size, typename T>
constexpr T create_bitmask() noexcept
{
    static_assert(size < bit_size<T>(), "Cannot make a mask larger than the given type's bit size.");
    static_assert(std::is_unsigned<T>() || size < bit_size<T>() - 1, "Creating a bitmask of that size on a signed type will cause overflow.");

    return create_bitmask<T>(size);
}

/**
 * Creates a bit mask from a given starting bit and ending bit.
 * This can be used for mask generation of bit ranges that
 * don't start from bit zero.
 *
 * @param  begin Beginning bit of the mask range.
 * @param  end   Ending bit of the mask range.
 * @tparam T     The type of the bitmask.
 *
 * @return The created bit mask.
 */
template <typename T>
constexpr T create_bitmask(const size_t begin, const size_t end) noexcept
{
    return static_cast<T>(create_bitmask<T>((end - begin) + 1) << begin);
}

/**
 * Creates a bit mask from a given starting bit and ending bit.
 * This can be used for mask generation of bit ranges that
 * don't start from bit zero.
 *
 * @tparam begin Beginning bit of the mask range.
 * @tparam end   Ending bit of the mask range.
 * @tparam T     The type of the bitmask.
 *
 * @return The created bit mask.
 */
template <size_t begin, size_t end, typename T>
constexpr T create_bitmask() noexcept
{
    static_assert(begin < bit_size<T>(), "Beginning bit cannot be larger than the given type's bit size.");
    static_assert(end   < bit_size<T>(), "Ending bit cannot be larger than the given type's bit size.");
    static_assert(begin < end, "Beginning bit position cannot be larger or equal to the ending bit position.");
    static_assert(std::is_unsigned<T>() || end < bit_size<T>() - 1, "Creating a bitmask of that size of a signed type will cause overflow.");

    return create_bitmask<T>(begin, end);
}

/**
 * Extracts a bit from a value.
 *
 * @param  src The value to extract a bit from.
 * @param  bit The bit to extract.
 * @tparam T   The type of the value.
 *
 * @return The extracted bit.
 */
template <typename T>
constexpr T get_bit(const T src, const size_t bit) noexcept
{
    return (src >> bit) & static_cast<T>(1);
}

/**
 * Extracts a bit from a value.
 *
 * @param  src The value to extract a bit from.
 * @tparam bit The bit to extract.
 * @tparam T   The type of the value.
 *
 * @return The extracted bit.
 */
template <size_t bit, typename T>
constexpr T get_bit(const T src) noexcept
{
    static_assert(bit < bit_size<T>(), "Attempting to retrieve bit outside of a type's bitwidth.");

    return get_bit<T>(src, bit);
}

/**
 * Sets a bit in a given value.
 *
 * @param value The value to set the bit within.
 * @param bit   The bit to set.
 * @tparam T    The type of the value.
 *
 * @return The value with the indicated bit set.
 */
template <typename T>
constexpr T set_bit(const T value, const size_t bit) noexcept
{
    return static_cast<T>(value | (static_cast<T>(1) << bit));
}

/**
 * Sets a bit in a given value.
 *
 * @param value The value to set the bit within.
 * @tparam bit  The bit to set.
 * @tparam T    The type of the value.
 *
 * @return The value with the indicated bit set.
 */
template <size_t bit, typename T>
constexpr T set_bit(const T value) noexcept
{
    static_assert(bit < bit_size<T>(), "Cannot set a bit outside of the given type's bit width.");
    static_assert(std::is_unsigned<T>() || bit < bit_size<T>() - 1, "Setting the highest bit of a signed type will cause overflow.");

    return set_bit(value, bit);
}

/**
 * Clears a bit in a value.
 *
 * @param value The value to clear a bit within.
 * @param bit  The bit to clear.
 *
 * @return The value with the bit cleared.
 */
template <typename T>
constexpr T clear_bit(const T value, const size_t bit)
{
    return static_cast<T>(value & ~(static_cast<uintmax_t>(1) << bit));
}

/**
 * Clears a bit in a value.
 *
 * @param value The value to clear a bit within.
 * @tparam bit  The bit to clear.
 *
 * @return The value with the bit cleared.
 */
template <size_t bit, typename T>
constexpr T clear_bit(const T value) noexcept
{
    static_assert(bit < bit_size<T>(), "Cannot set a bit outside of the given type's bit width.");
    static_assert(std::is_unsigned<T>() || bit < bit_size<T>() - 1, "Clearing the highest bit of a signed type will cause overflow.");

    return clear_bit(value, bit);
}

/**
 * Clears a bit in a value and shifts the given value
 * into its place.
 *
 * All other bits except the first bit (bit 0) will be discarded from
 * the given value before shifting it into place.
 *
 * @param  src   The value to clear and set a bit within.
 * @param  value The other value.
 * @tparam bit   The bit to clear and set.
 * @tparam T     The type of both values.
 *
 * @return The source value with its bit cleared and set.
 */
template <size_t bit, typename T>
constexpr T clear_and_set_bit(const T src, const T value) noexcept
{
    static_assert(bit < bit_size<T>(), "Cannot set a bit outside of the given type's bit width.");
    static_assert(std::is_unsigned<T>() || bit < bit_size<T>() - 1, "Clearing the highest bit of a signed type will cause overflow.");

    return static_cast<T>(clear_bit<bit>(src) | static_cast<T>((!!value) << bit));
}

/**
 * Clears a bit range in a value and shifts another value into
 * the range.
 *
 * The value to be moved into the range is masked to exclude bits outside the specified
 * range before shifting it into place.
 *
 * The masking is as follows:
 *
 * Say you have clear_and_set_bits<1, 2>(src, value). This will do the following:
 *  - Mask away bits 1 and 2 in the source value.
 *  - Mask away everything but bits 0 and 1 in the given replacement value (bitmask with a length of two bits).
 *  - Shifts the masked replacement into the bit range within the source value.
 *
 * @param  src   The value to clear and set a bit within.
 * @param  value The other value.
 * @tparam begin The beginning bit of the bit range.
 * @tparam end   The ending bit of the bit range.
 * @tparam T     The type of both values.
 *
 * @return The source value with its bit range cleared and set.
 */
template <size_t begin, size_t end, typename T>
constexpr T clear_and_set_bits(const T src, const T value) noexcept
{
    static_assert(begin < end, "Beginning bit cannot be larger than the ending bit.");
    static_assert(begin < bit_size<T>(), "Beginning bit cannot be larger than the given type's bit width.");
    static_assert(end   < bit_size<T>(), "Ending bit cannot be larger than the given type's bit width.");
    static_assert(std::is_unsigned<T>() || end < bit_size<T>() - 1, "Clearing the highest bit of a signed type will cause overflow.");

    using num_bits = std::integral_constant<T, end - begin + 1>;

    return static_cast<T>((src & ~create_bitmask<begin, end, T>()) | ((value & create_bitmask<num_bits::value, T>()) << begin));
}

/**
 * Extracts a range of bits from a value.
 *
 * @param  src   The value to extract the bits from.
 * @param  begin The beginning of the bit range.
 * @param  end   The ending of the bit range.
 * @tparam T     The type of the value.
 *
 * @return The extracted bits.
 */
template <typename T>
constexpr T get_bits(const T src, const size_t begin, const size_t end) noexcept
{
    return static_cast<T>(((src << ((bit_size<T>() - 1) - end)) >> (bit_size<T>() - end + begin - 1)));
}

/**
 * Extracts a range of bits from a value.
 *
 * @param  src   The value to extract the bits from.
 * @tparam begin The beginning of the bit range.
 * @tparam end   The ending of the bit range.
 * @tparam T     The type of the value.
 *
 * @return The extracted bits.
 */
template <size_t begin, size_t end, typename T>
constexpr T get_bits(const T src) noexcept
{
    static_assert(begin < end, "Beginning bit must be less than the ending bit.");
    static_assert(begin < bit_size<T>(), "Beginning bit is larger than Type's total bit size.");
    static_assert(end   < bit_size<T>(), "Ending bit is larger than Type's total bit size.");

    return get_bits(src, begin, end);
}

/**
 * Checks whether or not a bit is set.
 *
 * @param  value The value containing the bit to check.
 * @param  bit   The bit to check.
 * @tparam T     The type of the value.
 *
 * @return True if the bit is set, false otherwise.
 */
template <typename T>
constexpr bool is_set(const T value, const size_t bit) noexcept
{
    return get_bit(value, bit) != 0;
}

/**
 * Checks whether or not a bit is set.
 *
 * @param  value The value containing the bit to check.
 * @tparam bit   The bit to check.
 * @tparam T     The type of the value.
 *
 * @return True if the bit is set, false otherwise.
 */
template <size_t bit, typename T>
constexpr bool is_set(const T value) noexcept
{
    return is_set(value, bit);
}

/**
 * Checks whether a bit is *not* set.
 *
 * @param  value The value containing the bit to check.
 * @param  bit   The bit to check.
 * @tparam T     The type of the value.
 *
 * @return True if the bit is not set, false otherwise.
 */
template <typename T>
constexpr bool is_not_set(const T value, const size_t bit) noexcept
{
    return !is_set(value, bit);
}

/**
 * Checks whether a bit is *not* set.
 *
 * @param value The value containing the bit to check.
 * @tparam bit  The bit to check.
 * @tparam T    The type of the value.
 *
 * @return True if the bit is not set, false otherwise.
 */
template <size_t bit, typename T>
constexpr bool is_not_set(const T value) noexcept
{
    return is_not_set(value, bit);
}

/**
 * Sign-extends a given value using the indicated
 * bit as the 'sign'.
 *
 * @param  src  The value to sign-extend.
 * @param  bits The number of bits to sign-extend within the value.
 * @tparam T    The type of the value.
 *
 * @return The sign extended value if the bit indicated
 *         by `bits - 1` is set to 1. Otherwise, the original
 *         value is returned.
 */
template <typename T>
inline T sign_extend(T src, const size_t bits) noexcept
{
    const T mask = create_bitmask<T>(bits);
    src &= mask;

    if (get_bit(src, bits - 1))
        return src | ~mask;

    return src;
}

/**
 * Sign-extends a given value using the indicated
 * bit as the 'sign'.
 *
 * @param  src  The value to sign-extend.
 * @tparam bits The number of bits to sign-extend within the value.
 * @tparam T    The type of the value.
 *
 * @return The sign extended value if the bit indicated
 *         by `bits - 1` is set to 1. Otherwise, the original
 *         value is returned.
 */
template <size_t bits, typename T>
inline T sign_extend(T src) noexcept
{
    return sign_extend(src, bits);
}

/**
 * Rotates a value left (ROL).
 *
 * @param  value The value to rotate.
 * @param  count The number of bits to rotate the value.
 * @tparam T     An unsigned type.
 *
 * @return The rotated value.
 */
template <typename T>
constexpr T rotate_left(const T value, const size_t count) noexcept
{
    static_assert(std::is_unsigned<T>(), "Can only rotate unsigned types left.");

    return count == 0
           ? value
           : static_cast<T>((value << count) | (value >> (bit_size<T>() - 1)));
}

/**
 * Rotates a value right (ROR).
 *
 * @param  value The value to rotate.
 * @param  count The number of bits to rotate the value.
 * @tparam T     An unsigned type.
 *
 * @return The rotated value.
 */
template <typename T>
constexpr T rotate_right(const T value, const size_t count) noexcept
{
    static_assert(std::is_unsigned<T>(), "Can only rotate unsigned types right.");

    return count == 0
           ? value
           : static_cast<T>((value >> count) | (value << (bit_size<T>() - count)));
}

/**
 * Counts the leading zeroes in a 32-bit value.
 *
 * @param value The value to count upon.
 *
 * @return The number of leading zeroes.
 */
inline uint32_t count_leading_zeroes32(const uint32_t value)
{
    if (value == 0)
        return 32;

#if defined(__GNUC__) || defined(__clang__)
    return static_cast<uint32_t>(__builtin_clz(value));
#elif defined(_MSC_VER)
    unsigned long result;
    _BitScanReverse(&result, value);

    return static_cast<uint32_t>(result);
#else
    uint32_t count = 0;
    uint32_t bit_test = 0x80000000U;

    while (count < bit_size<uint32_t>() && (bit_test & value) == 0)
    {
        count++;
        bit_test >>= 1;
    }

    return count;
#endif
}

} // namespace Bit

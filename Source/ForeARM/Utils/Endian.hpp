#pragma once

#include <cstdint>

namespace Endian
{

/**
 * Byteswaps a 16-bit value.
 *
 * @param data The value to byteswap.
 *
 * @return The byteswapped value.
 */
inline uint16_t swap16(const uint16_t data) noexcept
{
#if defined (_MSC_VER)
    return _byteswap_ushort(data);
#else
    return static_cast<uint16_t>((data >> 8) | (data << 8));
#endif
}

/**
 * Byteswaps a 32-bit value.
 *
 * @param data The value to byteswap.
 *
 * @return The byteswapped value.
 */
inline uint32_t swap32(const uint32_t data) noexcept
{
#if defined(__clang__) || defined(__GNUC__)
    return __builtin_bswap32(data);
#elif defined (_MSC_VER)
    return _byteswap_ulong(data);
#else
    return (data & 0x000000FFU) << 24 | (data & 0x0000FF00U) << 8 |
           (data & 0x00FF0000U) >> 8  | (data & 0xFF000000U) >> 24;
#endif
}

/**
 * Byteswaps a 64-bit value.
 *
 * @param data The value to byteswap.
 *
 * @return The byteswapped value.
 */
inline uint64_t swap64(const uint64_t data) noexcept
{
#if defined(__clang__) || defined(__GNUC__)
    return __builtin_bswap64(data);
#elif defined (_MSC_VER)
    return _byteswap_uint64(data);
#else
    return (data & 0x00000000000000FFULL) << 56 | (data & 0x000000000000FF00ULL) << 40 |
           (data & 0x0000000000FF0000ULL) << 24 | (data & 0x00000000FF000000ULL) << 8  |
           (data & 0x000000FF00000000ULL) >> 8  | (data & 0x0000FF0000000000ULL) >> 24 |
           (data & 0x00FF000000000000ULL) >> 40 | (data & 0xFF00000000000000ULL) >> 56;
#endif
}

} // namespace Endian
